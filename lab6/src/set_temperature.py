#-------------------------------------------------------
# set_temperature                
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-14
#-------------------------------------------------------
# Gets functions within the range of [15,35]
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------

def set_temperature():

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

    temp_to_check = 0.0

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

    return_value = 0.0

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

    MIN_TEMP = 15
    MAX_TEMP = 35

#-------------------------------------------------------
# get the variable from the user
#-------------------------------------------------------

    temp_to_check = float(raw_input("Enter an temperature between 15 and 35 celsius: ").strip())
    
#-------------------------------------------------------
# check if it is in the range
#-------------------------------------------------------

    if temp_to_check > MAX_TEMP:
        print "Error: temperature must be less than or equal to 35 celsius."
        return_value = None
    elif temp_to_check < MIN_TEMP:
        print "Error: temperature must be greater than or equal to 15 celsius."
        return_value = None
    else:
        return_value = temp_to_check
    
    
        
    return return_value

