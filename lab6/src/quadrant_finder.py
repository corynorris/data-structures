#-------------------------------------------------------
# quadrant finder
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-14
#-------------------------------------------------------
# finds the quadrant of a given point (or axis)
#-------------------------------------------------------

#-------------------------------------------------------
# Parameters:
#    x,y: coordinates of a point
# Preconditions:
#    x,y: floats
# Postconditions:
#-------------------------------------------------------

def find_quadrant():

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------
    x = 0
    y = 0
    
#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    quadrant = 0
    
#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

    on_axis = False
#-------------------------------------------------------
#    Get the coordinates
#-------------------------------------------------------
    x = int(raw_input("Enter a coordinate for x: ").strip())     
    y = int(raw_input("Enter a coordinate for y: ").strip())

#-------------------------------------------------------
# determine if it's on an axis (note the point (0,0) will trigger both)
#-------------------------------------------------------

    if x == 0 :
        print "(%d,%d) is on the y axis" %(x,y)
        on_axis = True
    if y == 0:
        print "(%d,%d) is on the x axis"%(x,y)
        on_axis = True
        
#-------------------------------------------------------
#    Determine which quadrant it's in
#-------------------------------------------------------
    
    if (not on_axis):
        if x>0:
            if y > 0:
                quadrant = 1
            else:
                quadrant = 4
        else:
            if y > 0:
                quadrant = 2
            else:
                quadrant = 3            
        print "(%d,%d) is in quadrant %d"%(x,y, quadrant)
    
    return
            
#testing

find_quadrant()   
    
        