#-------------------------------------------------------
# leap_year
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-14
#-------------------------------------------------------
# checks if a year is a leap year or not
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    year
# Preconditions:
#    year: integer representing year
# Postconditions:
#    result: boolean - true for leap year otherwise false
#-------------------------------------------------------

def leap(year):

#-------------------------------------------------------
# output
#-------------------------------------------------------

    result = False

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    REGULAR_YEAR_QUOTIENT = 4
    CENTENIAL_YEAR_QUOTIENT = 400
    CHECK_CENTENIAL = 100

#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------
    if year%CHECK_CENTENIAL == 0:#if it is a centenial year
        if year%CENTENIAL_YEAR_QUOTIENT == 0:
            result = True
        else:
            result = False
    elif year%REGULAR_YEAR_QUOTIENT == 0:#otherwise
        result = True
    else:
        result = False
        
    return result

#testing
year = int(raw_input("Enter a year (> 0): ").strip())
result = leap(year)
if (result):
    print "is a leap year"
else:
    print "is not a leap year"

    
