from sorts_array import *
import copy
import random
EQUALS = 0
LESSER = -1
GREATER = 1

#test all 3 arrays through both functions
#global
def sort_test(sort_func, cmp_func, array):
    global cmp_count
    #initialize the trees to test
    cmp_count = 0
    sort_func(copy.deepcopy(array),cmp_func)
    return
#after sort_test is called the number of comparisons is cmp_count
def compare_( v1,v2):
    global cmp_count
    cmp_count += 1
    
    #unpack the tuples 
    value = EQUALS
    if v1 > v2:
        value = GREATER
    elif v1 < v2:
        value = LESSER     

    return value

ordered_array = []
rev_array = []

#set up the 3 arrays to test
for count in range(100):
    ordered_array += [count]
for count in range(100):
    rev_array += [(101-count)]

#TEST ORDERED AND REVERSE SORTS
sort_test(bubble_sort,compare_,ordered_array)
bubble_ordered = cmp_count
sort_test(bubble_sort,compare_,rev_array)
bubble_rev = cmp_count

sort_test(insertion_sort,compare_,ordered_array)
insertion_ordered = cmp_count
sort_test(insertion_sort,compare_,rev_array)
insertion_rev = cmp_count

sort_test(merge_sort,compare_,ordered_array)
merge_ordered = cmp_count
sort_test(merge_sort,compare_,rev_array)
merge_rev = cmp_count
#
sort_test(quick_sort,compare_,ordered_array)
quick_ordered = cmp_count
sort_test(quick_sort,compare_,rev_array)
quick_rev = cmp_count

sort_test(heap_sort,compare_,ordered_array)
heap_ordered = cmp_count
sort_test(heap_sort,compare_,rev_array)
heap_rev = cmp_count

sort_test(shell_sort,compare_,ordered_array)
shell_ordered = cmp_count
sort_test(shell_sort,compare_,rev_array)
shell_rev = cmp_count

sort_test(comb_sort,compare_,ordered_array)
comb_ordered = cmp_count
sort_test(comb_sort,compare_,rev_array)
comb_rev = cmp_count

sort_test(cocktail_sort,compare_,ordered_array)
cocktail_ordered = cmp_count
sort_test(cocktail_sort,compare_,rev_array)
cocktail_rev = cmp_count

sort_test(binary_insert_sort,compare_,ordered_array)
binary_insert_ordered = cmp_count
sort_test(binary_insert_sort,compare_,rev_array)
binary_insert_rev = cmp_count

sort_test(m3_quick_sort,compare_,ordered_array)
m3_quick_ordered = cmp_count
sort_test(m3_quick_sort,compare_,rev_array)
m3_quick_rev = cmp_count

sort_test(selection_sort,compare_,ordered_array)
selection_ordered = cmp_count
sort_test(selection_sort,compare_,rev_array)
selection_rev = cmp_count


#initialize variables for sorts
bubble_rand = 0
merge_rand = 0
insertion_rand = 0  
selection_rand = 0
heap_rand = 0
shell_rand = 0
quick_rand = 0
cocktail_rand = 0
comb_rand = 0
binary_insert_rand = 0
m3_quick_rand = 0 


#for the random.    
for count in range(1000):
    cur_array = []
    for num in range(100):
        cur_array += [random.randint(0,1000)]
    
    sort_test(bubble_sort,compare_, cur_array)
    bubble_rand += cmp_count
    sort_test(merge_sort,compare_, cur_array)
    merge_rand += cmp_count
    sort_test(insertion_sort,compare_, cur_array)
    insertion_rand += cmp_count    
    sort_test(selection_sort,compare_, cur_array)
    selection_rand += cmp_count
    sort_test(quick_sort,compare_, cur_array)
    quick_rand += cmp_count
    sort_test(heap_sort,compare_, cur_array)
    heap_rand += cmp_count
    sort_test(shell_sort,compare_, cur_array)
    shell_rand += cmp_count
    sort_test(comb_sort,compare_, cur_array)
    comb_rand += cmp_count
    sort_test(cocktail_sort,compare_, cur_array)
    cocktail_rand += cmp_count
    sort_test(binary_insert_sort,compare_, cur_array)
    binary_insert_rand += cmp_count
    sort_test(m3_quick_sort,compare_, cur_array)
    m3_quick_rand += cmp_count



print '\n                     Comparisons'
print 'Algorithm       In Order  Reversed   Random'
print '---------       --------  --------   ------'
print 'bubble Sort         %4d      %4d     %4d '%(bubble_ordered,bubble_rev,(bubble_rand/1000))
print 'insertion Sort      %4d      %4d     %4d '%(insertion_ordered,insertion_rev,(insertion_rand/1000))
print 'merge Sort          %4d      %4d     %4d '%(merge_ordered,merge_rev,(merge_rand/1000))
print 'selection Sort      %4d      %4d     %4d '%(selection_ordered,selection_rev,(selection_rand/1000))
print 'quick Sort          %4d      %4d     %4d '%(quick_ordered,quick_rev,(quick_rand/1000))
print 'heap Sort           %4d      %4d     %4d '%(heap_ordered,heap_rev,(heap_rand/1000))
print 'shell Sort          %4d      %4d     %4d '%(shell_ordered,shell_rev,(shell_rand/1000))
print 'comb Sort           %4d      %4d     %4d '%(comb_ordered,comb_rev,(comb_rand/1000))
print 'cocktail Sort       %4d      %4d     %4d '%(cocktail_ordered,cocktail_rev,(cocktail_rand/1000))
print 'binary_insert Sort  %4d      %4d     %4d '%(binary_insert_ordered,binary_insert_rev,(binary_insert_rand/1000))
print 'm3_quick Sort       %4d      %4d     %4d '%(m3_quick_ordered,m3_quick_rev,(m3_quick_rand/1000))

    
    
