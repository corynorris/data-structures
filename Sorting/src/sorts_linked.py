from list_linked import *
"""
bubble_sort sorts the contents of a list using the bubble sort algorithm
 Preconditions:  a exists, 0 <= n <= length( a )
 Postconditions: the elements of a are in sorted order
"""
def bubble_sort( a, cmp_func ):
    done = False
    last = list_size(a) -1
    
    while not done:
        # initially, assume done is true
        done = True
        node = a['front']
        next = node['next']
        
        for count in range (last):
            if cmp_func( node['data'], next['data']) > 0:
                # the pair (a[i], a[i+1]) is out of order
                # exchange a[i] and a[i + 1] to put them in sorted order
                swap( node, next )
                # if you swapped you need another pass
                done = False
                last = count
                
            node = next
            next = next['next']
    
        
        # decreases 'last' because the element with the largest value in the unsorted
        # part of the list is now at the right side of the list.       
    # done == False iff no pair of keys was  swapped on the last pass
    return

"""
selection_sort
Sorts a list of numbers.

   1. Find the minimum value in the list
   2. Swap it with the value in the first position
   3. Repeat the steps above for remainder of the list (starting at the second position)
"""
def selection_sort( a, cmp_func ):
    cur = a['front']
    last = list_size(a)-1
# Find the smallest element and put it into it's appropriate location
    for count in range (last):    
        search = cur        
        min = cur
        #search from the known correct value onward to find the next smallest
        while search != None:
            if (cmp_func(search['data'], min['data']) < 0):
                min = search
            search = search['next']
        
        swap(min, cur)
        cur = cur['next']
    
    
    return
"""
insertion_sort sorts the contents of a list using the insertion sort algorithm
 Preconditions:  a exists, 0 <= n <= length( a )
 Postconditions: the elements of a are in sorted order
"""

#insertion sort takes the next value, and puts it where it belongs in the sorted segment
def insertion_sort( a, cmp_func ):
    #go through each element, and put it where it belongs in the list
    print 'insertion sort'
    print a
    
    current = a['front']
    front = a['front']
    
    length = list_size(a) - 1
    
    
    for i in range(length):
        #take the i'th element
        search_cur = front
        for t in range(i):
            #find where in the sorted data it goes
            if (cmp_func(search_cur['data'],current['data']) >= 0):
                #move the # here
                
                temp ={'data':current['data'], 'next':search_cur['next']}            
                search_cur['next'] = temp
                break

            else:    
                search_cur = search_cur['next']
                
        current = current['next']
        
   
    print a    
    
    return

"""
Swaps two elements in an array.
"""
def swap( node,next ):
    temp = node['data']
    node['data'] = copy.deepcopy(next['data'])
    next['data'] = copy.deepcopy(temp)
    return

"""
David Brown
dbrown@wlu.ca
2007-03-12

quicksort  sorts the contents of a list using the quicksort algorithm
    Preconditions:  a exists, 0 <= i <= j < length( a )
    Postconditions: the elements of a are in sorted order
"""
def quick_sort( a, cmp_func ):
    node = a['front']
    size = list_size(a) -1
        
    quick_sort_aux( a, 0, len( a ) - 1, cmp_func )
    return

def quick_sort_aux( a, first, last, cmp_func ):
    # to sort the subarray a[p:q] of array a into ascending order
    if first < last:
        # partitions a[first:last] into a[first:pivot] and a[pivot+1:last]
        i = partition( a, first, last, cmp_func )
        quick_sort_aux( a, first, i - 1, cmp_func )
        quick_sort_aux( a, i + 1, last, cmp_func )
    return