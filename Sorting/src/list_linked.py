#-------------------------------------------------------
# list_linked.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Linked version of the List ADT.
#-------------------------------------------------------
# Constant Definitions
#    Comparison value names.
#-------------------------------------------------------
LESSER = - 1
EQUALS = 0
GREATER = 1

import copy

#---------------------------------------------------------
# Returns an empty list.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
#    cmp_func - data comparison function
# Preconditions:
#    print_func - valid function reference
#    cmp_func - valid function reference
# Postconditions:
#    Returns an empty list.
#-------------------------------------------------------
def list_init( print_func, cmp_func ):
    list = { 'size':0, 'front':None, 'print_func':print_func, 'cmp_func':cmp_func }
    return list

#---------------------------------------------------------
# Returns the size of list.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid linked list dictionary
# Postconditions:
#    Returns the size of list.
#-------------------------------------------------------
def list_size( list ):
    return list['size']

#---------------------------------------------------------
# Determines if list is empty.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid linked list dictionary
# Postconditions:
#    Returns True if list is empty, False otherwise.
#-------------------------------------------------------
def list_empty( list ):
    return list['size'] == 0

#---------------------------------------------------------
# Inserts value into list.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    value - a data element
# Preconditions:
#    list - valid linked list dictionary
#    value - none
# Postconditions:
#    value is added to list.
#-------------------------------------------------------
def list_insert( list, value ):
    node = { 'data':copy.deepcopy( value ), 'next':list['front'] }
    list['front'] = node
    list['size'] += 1
    return

#---------------------------------------------------------
# Removes value from list.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    key - a data element
# Preconditions:
#    list - valid linked list dictionary
#    key - a data element comparable by cmp_func
# Postconditions:
#    Returns the first value matching key - the value is
#    removed from list. Returns None if list is empty.
#-------------------------------------------------------
def list_remove( list, key ):
    # Find the proper value in the list.
    cmp_func = list['cmp_func']
    current = list['front']
    previous = None
    
    while current is not None and cmp_func( key, current['data'] ) != EQUALS:
        previous = current
        current = current['next']
        
    if current is None:
        value = None
    else:
        # Remove the value.
        value = current['data']
        list['size'] -= 1
        
        if previous is None:
            list['front'] = current['next']
        else:
            previous['next'] = current['next']
    return value

#---------------------------------------------------------
# Finds a value in list.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    key - a data element
# Preconditions:
#    list - valid linked list dictionary
#    key - a data element comparable by cmp_func
# Postconditions:
#    Returns a copy of the value matching key, otherwise None.
#    (Uses an iterative algorithm)
#-------------------------------------------------------
def list_find( list, key ):
    # Find the proper value in the list.
    cmp_func = list['cmp_func']
    current = list['front']
    
    while current is not None and cmp_func( key, current['data'] ) != 0:
        current = current['next']
        
    if current is None:
        value = None
    else:
        value = copy.deepcopy( current['data'] )
    return value

#---------------------------------------------------------
# Finds a value in list.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    key - a data element
# Preconditions:
#    list - valid linked list dictionary
#    key - a data element usable by cmp_func
# Postconditions:
#    Returns a copy of the value matching key, otherwise None.
#    (Uses a recursive algorithm)
#-------------------------------------------------------
def list_find( list, key ):
    return list_find_aux( list['front'], key, list['cmp_func'] )

def list_find_aux( current, key, cmp_func ):
    if current == None:
        value = None
    elif cmp_func( key, current['data'] ) == EQUALS:
        value = copy.deepcopy( current['data'] )
    else:
        value = list_find_aux( current['next'], key, cmp_func )
    return value

#---------------------------------------------------------
# Utility function for retrieving the data print function.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid array list dictionary
# Postconditions:
#    Returns list's print function.
#-------------------------------------------------------
def list_get_print_func( list ):
    return list['print_func']

#---------------------------------------------------------
# Utility function for changing the data print function.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    print_func - data print function
# Preconditions:
#    list - valid array list dictionary
#    print_func - valid function reference
# Postconditions:
#    Changes list's print function to print_func.
#-------------------------------------------------------
def list_set_print_func( list, print_func ):
    list['print_func'] = print_func
    return

#---------------------------------------------------------
# Utility function for retrieving the data comparison function.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid array list dictionary
# Postconditions:
#    Returns list's comparison function.
#-------------------------------------------------------
def list_get_cmp_func( list ):
    return list['print_func']

#---------------------------------------------------------
# Utility function for changing the data comparison function.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    cmp_func - data print function
# Preconditions:
#    list - valid array list dictionary
#    cmp_func - valid function reference
# Postconditions:
#    Changes list's comparison function to cmp_func.
#-------------------------------------------------------
def list_set_cmp_func( list, cmp_func ):
    list['cmp_func'] = cmp_func
    return
