#No idea what this one is supposed to do.
from sorts_linked import *
import copy
import random
from list_linked import *

EQUALS = 0
LESSER = -1
GREATER = 1


def sort_test(sort_func, cmp_func, linked):
    global cmp_count
    #initialize the trees to test
    cmp_count = 0
    sort_func(copy.deepcopy(linked),cmp_func)
    
   

def compare_( v1,v2):
    global cmp_count
    cmp_count += 1
    
    #unpack the tuples 
    value = EQUALS
    if v1 > v2:
        value = GREATER
    elif v1 < v2:
        value = LESSER    

    return value




#now fill some linked lists to test
ordered_linked = list_init(None, compare_)
rev_linked = list_init(None, compare_)

for count in range(100):
    list_insert(ordered_linked, count)
for count in range(100):
    list_insert(rev_linked, 100-count)

#test the two lists here with list_sort
'''
sort_test(bubble_sort,compare_,ordered_linked)
bubble_ordered = cmp_count
sort_test(bubble_sort,compare_,rev_linked)
bubble_rev = cmp_count


sort_test(selection_sort,compare_,ordered_linked)
selection_ordered = cmp_count
sort_test(selection_sort,compare_,rev_linked)
selection_rev = cmp_count

sort_test(quick_sort,compare_,ordered_linked)
quick_ordered = cmp_count
sort_test(quick_sort,compare_,rev_linked)
quick_rev = cmp_count


'''
sort_test(insertion_sort,compare_,ordered_linked)
insertion_ordered = cmp_count
sort_test(insertion_sort,compare_,rev_linked)
insertion_rev = cmp_count
#variables to record data
bubble_rand = 0
insertion_rand = 0  
selection_rand = 0
quick_rand = 0
#for the random. 

'''   
for count in range(1000):
    cur_linked = list_init(None, compare_)
    for num in range(100):
        list_insert(cur_linked, random.randint(0,1000))
    
    #test our 4 linked sorts

    sort_test(bubble_sort,compare_, cur_linked)
    bubble_rand += cmp_count
    sort_test(selection_sort,compare_, cur_linked)
    selection_rand += cmp_count   
  
    sort_test(insertion_sort,compare_, cur_linked)
    insertion_rand += cmp_count 

         
    sort_test(quick_sort,compare_, cur_linked)
    quick_rand += cmp_count


print '\n                     Comparisons'
print 'Algorithm       In Order  Reversed   Random'
print '---------       --------  --------   ------'
print 'bubble Sort         %4d      %4d     %4d '%(bubble_ordered,bubble_rev,(bubble_rand/1000))
print 'insertion Sort      %4d      %4d     %4d '%(insertion_ordered,insertion_rev,(insertion_rand/1000))
print 'quick Sort          %4d      %4d     %4d '%(quick_ordered,quick_rev,(quick_rand/1000))
print 'selection Sort      %4d      %4d     %4d '%(selection_ordered,selection_rev,(selection_rand/1000))
'''
