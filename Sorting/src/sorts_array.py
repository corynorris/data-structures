"""
David Brown
dbrown@wlu.ca
2007-03-12

bubble_sort sorts the contents of a list using the bubble sort algorithm
 Preconditions:  a exists, 0 <= n <= length( a )
 Postconditions: the elements of a are in sorted order
"""
def bubble_sort( a, cmp_func ):
    done = False
    last = len( a ) - 1
    
    while not done:
        # initially, assume done is true
        done = True
        
        for i in range( last ):
            if cmp_func( a[i], a[i + 1] ) > 0:
                # the pair (a[i], a[i+1]) is out of order
                # exchange a[i] and a[i + 1] to put them in sorted order
                swap( a, i, i + 1 )
                # if you swapped you need another pass
                done = False
                last = i
        # decreases 'last' because the element with the largest value in the unsorted
        # part of the list is now at the right side of the list.       
    # done == False iff no pair of keys was  swapped on the last pass
    return

#--------------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-03-12

insertion_sort sorts the contents of a list using the insertion sort algorithm
 Preconditions:  a exists, 0 <= n <= length( a )
 Postconditions: the elements of a are in sorted order
"""

def insertion_sort( a, cmp_func ):
    n = len( a )

    for i in range( 1, n ):
        # Move each key bigger than save in a[1:n-1] one space to the right.
        save = a[i]
        j = i

        while j > 0 and cmp_func( a[j - 1], save ) > 0:
            a[j] = a[j - 1]
            j -= 1
        # Move save into hole opened up by moving previous keys to the right.    
        a[j] = save
    return

#--------------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-03-12

merge_sort sorts the contents of a list using the merge sort algorithm
 Preconditions:  a exists, 0 <= n <= length( a )
 Postconditions: the elements of a are in sorted order
"""
def merge_sort( a, cmp_func ):
    merge_sort_aux( a, 0, len( a ) - 1, cmp_func )
    return
    
def merge_sort_aux( a, first, last, cmp_func ):
    if first < last:
        middle = ( first + last ) / 2
        merge_sort_aux( a, first, middle, cmp_func )
        merge_sort_aux( a, middle + 1, last, cmp_func )
        merge( a, first, middle, last, cmp_func )
    return

# Merges two lists together
def merge( a, first, middle, last, cmp_func ):
    temp = []
    i = first
    j = middle + 1
    
    while i <= middle and j <= last:
        
        if cmp_func( a[i], a[j] ) <= 0:
            # put leftmost element of left array into temp array
            temp.append( a[i] )
            i += 1
        else:
            # put leftmost element of right array into temp array
            temp.append( a[j] )
            j += 1

    # copy any remaining elements from the left half
    while i <= middle:
        temp.append( a[i] )
        i += 1
        
    # copy any remaining elements from the right half
    while j <= last:
        temp.append( a[j] )
        j += 1

    # copy the temporary array back to the passed array
    for i in range( len( temp ) ):
        a[first + i] = temp[i]

    return

#------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-03-12

quicksort  sorts the contents of a list using the quicksort algorithm
    Preconditions:  a exists, 0 <= i <= j < length( a )
    Postconditions: the elements of a are in sorted order
"""
def quick_sort( a, cmp_func ):
    quick_sort_aux( a, 0, len( a ) - 1, cmp_func )
    return

def quick_sort_aux( a, first, last, cmp_func ):
    # to sort the subarray a[p:q] of array a into ascending order
    if first < last:
        # partitions a[first:last] into a[first:pivot] and a[pivot+1:last]
        i = partition( a, first, last, cmp_func )
        quick_sort_aux( a, first, i - 1, cmp_func )
        quick_sort_aux( a, i + 1, last, cmp_func )
    return

"""
 Partition  divides a list into two parts.
    Preconditions:  a exists, 0 <= i <= j < length( a )
    Postconditions: j < i; keys with index <= j are smaller than
                         keys with index >= i
"""
def partition( a, start, end, cmp_func ):
    pivotIndex = ( start + end ) / 2
    low = start
    high = end - 1  # After we remove pivot it will be one smaller
    pivotValue = a[pivotIndex]
    a[pivotIndex] = a[end]
    
    while True:
        while low <= high and cmp_func( a[low], pivotValue ) < 0:
            low = low + 1
        while low <= high and cmp_func( a[high], pivotValue ) >= 0:
            high = high - 1
        if low > high:
            break
        swap( a, low, high )
#        a[low], a[high] = a[high], a[low]
        
    a[end] = a[low]
    a[low] = pivotValue
    return low

#------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-01-02

selection_sort
Sorts a list of numbers.
"""
def selection_sort( a, cmp_func ):
    n = len( a )
# Find the samllest element and put it into it's appropriate location

    for i in range( n ):
        min = i
        
        for j in range( i + 1, n ):

            if cmp_func( a[j], a[min] ) < 0:
                min = j
        # swap the elements
        swap( a, i, min )
    return

#------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-03-12

heap_sort  sorts the contents of a list using the heap sort algorithm
    Preconditions:  a exists, 0 <= i <= j < length( a )
    Postconditions: the elements of a are in sorted order
"""
def heap_sort( a, cmp_func ):
    first = 0
    last = len( a ) - 1
    build_heap( a, first, last, cmp_func )

    for i in range( last, first, - 1 ):
        swap( a, i, first )
        reheap ( a, first, i - 1, cmp_func )
    return

# create heap (used by heap_sort)
def build_heap( a, first, last, cmp_func ):
    i = last / 2

    while i >= first:
        reheap( a, i, last, cmp_func )
        i -= 1
    return
 
# establish heap property (used by build_heap)
def reheap( a, first, last, cmp_func ):
    done = False
    
    while 2 * first + 1 <= last and not done:
        k = 2 * first + 1

        if k < last and cmp_func( a[k], a[k + 1] ) < 0:
            k += 1

        if cmp_func( a[first], a[k] ) >= 0:
            done = True
        else:
            swap( a, first, k )
            first = k
    return

#------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-03-12

shell_sort  sorts the contents of a list using the shell sort algorithm
    Preconditions:  a exists, 0 <= i <= j < length( a )
    Postconditions: the elements of a are in sorted order
"""
def shell_sort( a, cmp_func ):
    n = len( a )
    incrmnt = n / 2
    
    while incrmnt > 0:
        for i in range( incrmnt, n ):
            j = i
            temp = a[i]
            
            while j >= incrmnt and cmp_func( a[j - incrmnt], temp ) > 0:
                a[j] = a[j - incrmnt]
                j = j - incrmnt
            a[j] = temp

        if incrmnt == 2:
           incrmnt = 1
        else:
           incrmnt = ( int )( incrmnt / 2.2 )
    return

#------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-03-12

comb_sort  sorts the contents of a list using the comb sort algorithm
    Preconditions:  a exists, 0 <= i <= j < length( a )
    Postconditions: the elements of a are in sorted order
"""
def comb_sort( a, cmp_func ):
    n = len( a )
    gap = n
    swapped = True
    
    while gap > 1 or swapped:
        gap = new_gap( gap )
        swapped = False
        i = 0
        
        for j in range( gap + i, n ):            
            if cmp_func( a[i], a[j] ) > 0:
                swap( a, i, j )
                swapped = True
            i += 1
    return

def new_gap( gap ):
    gap = gap * 10 / 13
    
    if gap == 9 or gap == 10:
        gap = 11
    elif gap < 1:
        gap = 1
    return gap

#------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-03-12

cocktail_sort  sorts the contents of a list using the cocktail sort algorithm
    Preconditions:  a exists, 0 <= i <= j < length( a )
    Postconditions: the elements of a are in sorted order
"""
def cocktail_sort( a, cmp_func ):
    n = len( a )
    done = False
    bottomLast = 0
    topLast = n - 1
    
    while not done:
        # if no elements have been swapped, then the list is sorted
        done = True
        
        for i in range( bottomLast, topLast ):
            if cmp_func( a[i], a[i + 1] ) > 0:
                # test whether the two elements are in the correct order
                swap( a, i, i + 1 )
                done = False
                topLast = i
                
        if not done:
            done = True
            # decreases `top` because the element with the largest value in the unsorted
            # part of the list is now on the position top                
            for i in range( topLast, bottomLast, - 1 ):
                if cmp_func( a[i], a[i - 1] ) < 0:
                    # test whether the two elements are in the correct order
                    swap( a, i, i - 1 )
                    done = False
                    bottomLast = i
        
            # increases `bottom` because the element with the smallest value in the unsorted 
            # part of the list is now on the position bottom 
        
    return

#--------------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-03-12

binary_insert_sort sorts the contents of a list using the insertion sort algorithm
 Preconditions:  a exists, 0 <= n <= length( a )
 Postconditions: the elements of a are in sorted order
"""

def binary_insert_sort( a, cmp_func ):

    for i in range( 1, len( a ) ):
        ins = bin_srch_i( a, 0, i, a[i], cmp_func )
        # Move each key bigger than save in a[1:n-1] one space to the right.
        if ins < i:
            save = a[i];
            for j in range( i - 1, ins - 1, - 1 ):
                a[j + 1] = a[j];
            a[ins] = save
    return

def bin_srch_r( a, low, high, value, cmp_func ):
    if low == high:
        mid = low
    else:
        mid = low + ( ( high - low ) / 2 )
        result = cmp_func( value, a[mid] ) 
    
        if result > 0:
            mid = bin_srch_r ( a, mid + 1, high, value, cmp_func )
        elif result < 0:
            mid = bin_srch_r ( a, low, mid, value, cmp_func )
    return mid

def bin_srch_i( a, low, high, value, cmp_func ):
    found = False
    mid = ( low + high ) / 2
    
    while low < high and not found:
        # Find the middle of the current subarray.
        result = cmp_func( value, a[mid] ) 
        
        if result > 0:
            # Search the right subarray.
            low = mid + 1
            mid = ( low + high ) / 2
        elif result < 0:
            # Search the left subarray.
            high = mid
            mid = ( low + high ) / 2
        else:
            found = True
    return mid

#------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-03-12

m3_quick_sort  (Median of 3 QuickSort) sorts the contents of a list using the quicksort algorithm
    Preconditions:  a exists, 0 <= i <= j < length( a )
    Postconditions: the elements of a are in sorted order
"""
def m3_quick_sort( a, cmp_func ):
    m3_quick_sort_aux( a, 0, len( a ) - 1, cmp_func )
    return

def m3_quick_sort_aux( a, first, last, cmp_func ):
    # to sort the subarray a[p:q] of array a into ascending order
    if first < last:
        # partitions a[first:last] into a[first:pivot] and a[pivot+1:last]
        i = m3_partition( a, first, last, cmp_func )
        m3_quick_sort_aux( a, first, i - 1, cmp_func )
        m3_quick_sort_aux( a, i + 1, last, cmp_func )
    return

"""
 m3_partition  divides a list into two parts.
    Preconditions:  a exists, 0 <= i <= j < length( a )
    Postconditions: j < i; keys with index <= j are smaller than
                         keys with index >= i
"""
def m3_partition( a, start, end, cmp_func ):
    # Find the pivot index
    pivotIndex = ( start + end ) / 2
    # Find the median index
    medianIndex = median( a, start, end, cmp_func )
    # Swap the pivot value and the median value
    swap( a, pivotIndex, medianIndex )
    low = start
    high = end - 1  # After we remove pivot it will be one smaller
    pivotValue = a[pivotIndex]
    a[pivotIndex] = a[end]
    
    while True:
        while low <= high and cmp_func( a[low], pivotValue ) < 0:
            low = low + 1
        while low <= high and cmp_func( a[high], pivotValue ) >= 0:
            high = high - 1
        if low > high:
            break
        swap( a, low, high )
#        a[low], a[high] = a[high], a[low]
        
    a[end] = a[low]
    a[low] = pivotValue
    return low

def median( a, i, j, cmp_func ):
    m = ( i + j ) / 2
    r1 = cmp_func( a[i], a[m] )
    r2 = cmp_func( a[i], a[j] )
    r3 = cmp_func( a[m], a[j] )
    
    if ( r1 > 0 and r2 < 0 ) or ( r2 > 0 and r1 < 0 ):
        med = i
    elif ( r2 < 0 and r3 < 0 ) or ( r3 < 0 and r2 > 0 ):
        med = j
    else:
        med = m        
    return med

#------------------------------------------------------------------
"""
David Brown
dbrown@wlu.ca
2007-01-02

Swaps two elements in an array.
"""
def swap( a, i, j ):
    temp = a[i]
    a[i] = a[j]
    a[j] = temp
    return

#------------------------------------------------------------------
# Determines whether an array is sorted or not.
def sort_test( a, cmp_func ):
    sorted = True
    n = len( a )
    i = 0

    while i < n - 1 and sorted:

        if cmp_func( a[i], a[i + 1] ) != 1:
            i += 1
        else:
            sorted = False
    return sorted
  