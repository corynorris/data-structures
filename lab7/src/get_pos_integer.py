#-------------------------------------------------------
#  get_pos_integer
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-
#-------------------------------------------------------
# returns the value within a range
#-------------------------------------------------------



#-------------------------------------------------------
# Parameters:
#    prompt, low, high
# Preconditions:
#    prompt: string
#    low, high: integers > 0
# Postconditions:
#    return_value: value within range, integer > 0
#-------------------------------------------------------

def get_pos_integer(prompt, low, high):

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------
    
    value = 0

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

    return_value = 0

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

    not_in_range = True

#-------------------------------------------------------
# get an integer 
#-------------------------------------------------------
 
    
    while not_in_range:
        try:
            value = int(raw_input(prompt).strip())
            #check if its in the range
            if value > high:
                print "%d is greater than %d" % (value, high)
            elif value < low:
                print "%d is less than %d" % (value, low)
            else:
                not_in_range = False

        except :
            print "Value must be an integer"
    
    return return_value