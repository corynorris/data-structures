#-------------------------------------------------------
# family_tickets
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-
#-------------------------------------------------------
# calculates ticket price for a family
#-------------------------------------------------------

import ticket_price



#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

purchase_ticket = ''

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

message = ""
total_price = 0.0

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
continue_looping = True

#-------------------------------------------------------
# assign our variables and start loop
#-------------------------------------------------------
message = "Purchase a Ticket? (y/n) "

while continue_looping:
    purchase_ticket = raw_input(message).strip()
    
    if purchase_ticket == 'y':
        total_price += ticket_price.determine_ticket_price()
        message = "Purchase another ticket? (y/n) "
    else:
        continue_looping = False
        
print "Total price: $%.2f" % (total_price)
