#-------------------------------------------------------
# ticket_price
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-20
#-------------------------------------------------------
# calculates the price of a ticket
#-------------------------------------------------------



#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------

def determine_ticket_price():

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

    age = 0.0
    student = ''


#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

    ticket_price = 0.0
    
    

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    continue_looping = True

#-------------------------------------------------------
# Get the persons age
#-------------------------------------------------------


        
    age = int(raw_input("How old are you?").strip())

#-------------------------------------------------------
# determine the ticket price
#-------------------------------------------------------
    if age < 3:#[0,3)
       ticket_price = 0
    elif age<18:# [3,18)
        ticket_price = 2    
    elif age < 65:#[18,65)
        ticket_price = 5
    else: #65 or over [65,infinity)
        ticket_price = 4
        
    if age >= 10 and age <= 15:#are they a student?
       student = raw_input("Are you a student at this school? (Y/N)").strip() 
       if student == 'y' or student == 'Y':
           ticket_price = 1


#-------------------------------------------------------
# print the price
#-------------------------------------------------------
    print "Your price is $%.2f" % (ticket_price)
 
            
            
     
            
    
   
    
    return ticket_price



