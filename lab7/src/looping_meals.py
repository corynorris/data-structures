#-------------------------------------------------------
# looping meals
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-20
#-------------------------------------------------------
# gets the price of several meals
#-------------------------------------------------------

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

breakfast = 0.0
lunch = 0.0
supper = 0.0

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

total = 0.0
away_again = ''
last_total = 0.0

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

continue_looping = True

#-------------------------------------------------------
# set message start loop
#-------------------------------------------------------
print "For Day 1"
while continue_looping:
#-------------------------------------------------------
# get the price of the meals
#-------------------------------------------------------

    breakfast += float(raw_input("How much was breakfast? $").strip())
    lunch += float(raw_input("How much was lunch? $").strip())
    supper += float(raw_input("How much was supper? $").strip())
#-------------------------------------------------------
# display days total
#-------------------------------------------------------    
    print "Days Total: $%.2f" % ((breakfast + lunch + supper) - last_total)
    last_total = (breakfast + lunch + supper)
 #-------------------------------------------------------
# continue the loop?
#-------------------------------------------------------   
    away_again = raw_input("Were you away another day (Y/N)?").strip()
    if away_again != 'Y' and away_again != 'y':
        continue_looping = False
    
    
#-------------------------------------------------------
# display the total
#------------------------------------------------------- 
print "Total\n\tBreakfast: $%.2f\n\tLunch: $%.2f\n\tSupper: $%.2f\n\nGrand Total: $%.2f" % (breakfast, lunch, supper, breakfast+lunch+supper)
        
    
    
    
