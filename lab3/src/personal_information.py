#-------------------------------------------------------
# Personal Information
#-------------------------------------------------------
# Author: Cory Norris    
# ID: 080704410
# Email: norr4410@wlu.ca
# Version: 2008-09-30
#-------------------------------------------------------
# Displays contact information
#-------------------------------------------------------

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
NAME = "Cory Norris"
address = "123 blah"
city = "Guelph"
state = "n/a"
zip =  "n/a"
telephone_number = "(111) 111-2222"
college_major = "Business/CompSci"

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

#-------------------------------------------------------
# Display User Data
#-------------------------------------------------------
print ("Name: %s\nAddress: %s\nCity: %s\nState: %s\nZIP: %s\nTelephone Number: %s\nCollege Major: %s") \
    %(NAME, address, city, state, zip, telephone_number, college_major)