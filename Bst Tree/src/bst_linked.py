#-------------------------------------------------------
# bst_linked.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Linked version of the Binary Search Tree ADT.
#-------------------------------------------------------
# Constant Definitions
#    Comparison value names.
#-------------------------------------------------------
LESSER = - 1
EQUALS = 0
GREATER = 1
#global

import copy

#---------------------------------------------------------
# Returns an empty BST.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
#    cmp_func   - data comparison function
# Preconditions:
#    print_func - valid function reference
#    cmp_func   - valid function reference
# Postconditions:
#    Returns an empty BST.
#-------------------------------------------------------
def bst_init( print_func, cmp_func ):
    bst = { 'root':None, 'print_func':print_func, 'cmp_func':cmp_func }
    return bst

#---------------------------------------------------------
# Determines if bst is empty.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
# Preconditions:
#    bst - valid linked BST.
# Postconditions:
#    Returns True if bst is empty, False otherwise.
#-------------------------------------------------------
def bst_empty( bst ):
    return bst['root'] is None

#---------------------------------------------------------
# Returns the maximum height of a BST.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
# Preconditions:
#    bst - valid linked BST.
# Postconditions:
#    Returns maximum height of bst.
#-------------------------------------------------------
def bst_height( bst ):
   
    height = bst_height_aux(bst['root'])
    return height

def bst_height_aux(node):
    count = 0
    rCount = 1
    lCount = 1
    if node is None:
        rCount = 0
        lCount = 0
    else:   
         rCount += bst_height_aux(node['left'])
         lCount += bst_height_aux(node['right'])
         
    if rCount > lCount:        
        count = rCount
    else:
        count = lCount
        
    return count
    
#---------------------------------------------------------
# Inserts a value into a BST.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    value - a data element
# Preconditions:
#    bst - valid linked BST.
#    value - none
# Postconditions:
#    value is added to bst - only one copy of value is allowed in bst.
#-------------------------------------------------------
def bst_insert( bst, value ):
    bst['root'] = bst_insert_aux( bst['root'], value, bst['cmp_func'] )
    return

# Adds 'value' to bst. Allows only one copy of 'value'
# in the tree. Returns the tree root.
def bst_insert_aux( node, value, cmp_func ):

    
    if node is None:
        # Base case: node is empty, so construct a new node with 'data'.
        node = { 'left':None, 'data':copy.deepcopy( value ), 'right':None }
    else:
        # General case: move to the left or right subtree depending on the value of 'data'.
        result = cmp_func( value, node['data'] )
        
        if result == LESSER:
            # Search the left subtree.
            node['left'] = bst_insert_aux( node['left'], value, cmp_func )
        elif result == GREATER:
            # Search the right subtree.
            node['right'] = bst_insert_aux( node['right'], value, cmp_func )
        # Multiple copies of 'data' are not allowed in the tree - do nothing.
    return node
    
#---------------------------------------------------------
# Finds a value in a BST.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    key - a comparable part of a data element.
# Preconditions:
#    bst - valid linked BST.
#    key - none.
# Postconditions:
#    If exists, returns value of node containing key,
#    otherwise returns None.
#    Uses a recursive algorithm.
#-------------------------------------------------------
def bst_retrieve( bst, key ):
    # Find the node containing the key value.
    return bst_retrieve_aux( bst['root'], key, bst['cmp_func'] )

def bst_retrieve_aux( node, key, cmp_func ):

   # your code here

    return value

#---------------------------------------------------------
# Finds a value in a BST.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    key - a comparable part of a data element.
# Preconditions:
#    bst - valid linked BST.
#    key - none.
# Postconditions:
#    If exists, returns value of node containing key,
#    otherwise returns None.
#    Uses an iterative algorithm.
#-------------------------------------------------------
def bst_retrieve( bst, key ):
    # Find the node containing the key value.
    node = bst['root']
    cmp_func = bst['cmp_func']
    value = None
    
    while node != None and value == None:
        result = cmp_func( key, node['data'] )
        
        if result == LESSER:
            node = node['left']
        elif result == GREATER:
            node = node['right']
        else:
            value = copy.deepcopy( node['data'] )
        
    return value

#---------------------------------------------------------
# Removes value from bst.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    key - a comparable part of a data element.
# Preconditions:
#    bst - valid linked BST.
#    key - none.
# Postconditions:
#    If exists, returns value of node containing key and deletes node,
#    otherwise returns None.
#-------------------------------------------------------
def bst_remove( bst, key ):

    # your code here

    return node, value

# Removes node from the tree and replaces it with the
# maximum node of its subtree.
def bst_delete_node( node ):

    # your code here
    
    return node

#---------------------------------------------------------
# Prints the contents of a BST using the custom
# printing function.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    order - the order to print bst contents in.
# Preconditions:
#    bst - valid linked BST.
#    bst - string 'r', 'i', 'o', or 'l'
# Postconditions:
#    Prints the contents of bst in the given order.
#-------------------------------------------------------
def bst_print( bst, order ):
    if order == 'r':
        bst_preorder_aux( bst['root'], bst['print_func'] )
    elif order == 'i':
        bst_inorder_aux( bst['root'], bst['print_func'] )
    elif order == 'o':
        bst_postorder_aux( bst['root'], bst['print_func'] )
    elif order == 'l':
        bst_levelorder_aux( bst['root'], bst['print_func'] )
    else:
        print "Invalid order value: %s" % ( str( order ) )
    return
    
# Traverses bst in inorder.
def bst_inorder_aux( node, print_func ) :
    
    if( node != None ) :
        bst_inorder_aux( node['left'], print_func )
        print_func( node['data'] )
        bst_inorder_aux( node['right'], print_func )
    return

# Traverses bst in postorder.
def bst_postorder_aux( node, print_func ) :
    
    if( node != None ) :
        bst_postorder_aux( node['left'], print_func )
        bst_postorder_aux( node['right'], print_func )
        print_func( node['data'] )
    return

# Traverses bst in preorder.
def bst_preorder_aux( node, print_func ) :
    
    if( node != None ) :
        print_func( node['data'] )
        bst_preorder_aux( node['left'], print_func )
        bst_preorder_aux( node['right'], print_func )
    return

# Traverses bst in levelorder.
# This uses a Python list like a queue - adds to the end, removes
# from the front. It can also use a queue data structure such as
# 'queueList' or 'queueDictionary'.
def bst_levelorder_aux( node, print_func ):

    # your code here

    return

#---------------------------------------------------------
# Utility function for retrieving the data comparison function.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
# Preconditions:
#    bst - valid linked BST dictionary
# Postconditions:
#    Returns bst's print function.
#-------------------------------------------------------
def bst_get_cmp_func( bst ):
    return bst['cmp_func']

#---------------------------------------------------------
# Utility function for changing the data comparison function.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
#    cmp_func       - data comparison function
# Preconditions:
#    bst - valid linked BST dictionary
#    cmp_func       - valid function reference
# Postconditions:
#    Changes bst's comparison function to cmp_func.
#-------------------------------------------------------
def bst_set_cmp_func( bst, cmp_func ):
    bst['cmp_func'] = cmp_func
    return

#---------------------------------------------------------
# Utility function for changing the data print function.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
#    print_func     - data print function
# Preconditions:
#    bst - valid linked BST dictionary
#    print_func     - valid function reference
# Postconditions:
#    Changes bst's print function to print_func.
#-------------------------------------------------------
def bst_set_print_func( bst, print_func ):
    bst['print_func'] = print_func
    return

#---------------------------------------------------------
# Utility function for retrieving the data print function.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
# Preconditions:
#    bst - valid linked BST dictionary
# Postconditions:
#    Returns bst's print function.
#-------------------------------------------------------
def bst_get_print_func( bst ):
    return bst['print_func']




def bst_traverse( avl ):
    array = []
    bst_traverse_aux(avl['root'],avl['print_func'], array)
    return array
    # your code here
    

def bst_traverse_aux( node, print_func,array ) :
    
    if( node != None ) :
        bst_traverse_aux( node['left'], print_func ,array)      
        array.append( node['data'] )  
        bst_traverse_aux( node['right'], print_func ,array )
        
    

            
    
    
