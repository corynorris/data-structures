from bst_linked import *
from morse import *
#---------------------------------------------------------
# fill tree by letter
'''
Write a function fill_tree_by_letter that fills a BST with letter 
 Morse code tuples ordered by their letter values. Also write a function fill_tree_by_code 
 that fills a BST with letter / Morse code tuples ordered by their Morse code values. Each function must
  have two parameters: an already initialized BST and one of the data tuples in the morse.py file.
'''
#-------------------------------------------------------

def fill_tree_by_letter(bst, data_tuple):
    bst_set_cmp_func(bst,compare_letters)
    for tuple in data_tuple:
        bst_insert(bst, tuple)#what does it mean ordered by their letter values
    return

def fill_tree_by_code(bst, data_tuple):
    bst_set_cmp_func(bst,compare_codes)
    for tuple in data_tuple:
        bst_insert(bst, tuple)#what does it mean ordered by their morse code values
    return   

def encode_morse(bst, text):
    #??
    text = text.upper()
    morse_code = ""
    for char in text:
        if char == ' ':
            morse_code += "\n"   
             
        elif char != '.':
            letter,code = bst_retrieve(bst,(char,0))
            morse_code += code + " "
    
    return morse_code

def decode_morse(bst, morse):
    codes = morse.split(' ')     
    text = ""
    
    for code in codes:
       
        if code != '\n' :
            #print '>%s<' % (code)
            letter,code = bst_retrieve(bst,(0,code))
            text += letter
    
    return text


print "Encoding"
letter_tree = bst_init(None, None)
fill_tree_by_letter(letter_tree, data1)
code = encode_morse(letter_tree,"My name is David Brown.")
print code

print "Height"
print bst_height(letter_tree)

print "Decoding"
code_tree = bst_init(None, None)
fill_tree_by_code(code_tree, data1)
text = decode_morse(code_tree, '... --- ...')
print text





#text = decode_morse(code_tree, code)#code)
#print text
