#-------------------------------------------------------
# morse.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Morse Code functions and data
#-------------------------------------------------------
# Data Definitions
#-------------------------------------------------------

# In order by letters.
data1 = ( ( 'A', '.-' ), ( 'B', '-...' ), ( 'C', '-.-.' ),
          ( 'D', '-..' ), ( 'E', '.' ), ( 'F', '..-.' ), 
          ( 'G', '--.' ), ( 'H', '....' ), ( 'I', '..' ), 
          ( 'J', '.---' ), ( 'K', '-.-' ), ( 'L', '.-..' ), 
          ( 'M', '--' ), ( 'N', '-.' ), ( 'O', '---' ), 
          ( 'P', '.--.' ), ( 'Q', '--.-' ), ( 'R', '.-.' ), 
          ( 'S', '...' ), ( 'T', '-' ), ( 'U', '..--' ), 
          ( 'V', '...-' ), ( 'W', '.--' ), ( 'X', '-..-' ), 
          ( 'Y', '-.--' ), ( 'Z', '--..' ) )

# In order by 'splitting'.
data2 = ( ( 'M', '--' ), ( 'F', '..-.' ), ( 'T', '-' ),
          ( 'C', '-.-.' ), ( 'J', '.---' ), ( 'P', '.--.' ), 
          ( 'W', '.--' ), ( 'A', '.-' ), ( 'D', '-..' ), 
          ( 'H', '....' ), ( 'K', '-.-' ), ( 'N', '-.' ),
          ( 'R', '.-.' ), ( 'U', '..--' ), ( 'Y', '-.--' ),
          ( 'B', '-...' ), ( 'E', '.' ), ( 'I', '..' ),
          ( 'G', '--.' ), ( 'L', '.-..' ), ( 'O', '---' ),
          ( 'Q', '--.-' ), ( 'S', '...' ), ( 'V', '...-' ),
          ( 'X', '-..-' ), ( 'Z', '--..' ) )

# In order by 'popularity'.
data3 = ( ( 'E', '.' ), ( 'T', '-' ), ( 'A', '.-' ), 
          ( 'O', '---' ), ( 'I', '..' ), ( 'N', '-.' ), 
          ( 'S', '...' ), ( 'H', '....' ), ( 'R', '.-.' ), 
          ( 'D', '-..' ), ( 'L', '.-..' ), ( 'U', '..--' ), 
          ( 'C', '-.-.' ), ( 'M', '--' ), ( 'P', '.--.' ), 
          ( 'F', '..-.' ), ( 'Y', '-.--' ), ( 'W', '.--' ), 
          ( 'G', '--.' ),  ( 'B', '-...' ), ( 'V', '...-' ),  
          ( 'K', '-.-' ), ( 'J', '.---' ), ( 'X', '-..-' ), 
          ( 'Z', '--..' ), ( 'Q', '--.-' ) )

#-------------------------------------------------------
# Constant Definitions
#    Comparison value names.
#-------------------------------------------------------
LESSER = - 1
EQUALS = 0
GREATER = 1


#---------------------------------------------------------
# Compares the letters in two morse code letter, code pair.
#-------------------------------------------------------
# Parameters:
#    morse1, morse2 - morse code letter / code pairs.
# Preconditions:
#    morse1, morse2 - valid letter / code tuple.
# Postconditions:
#    Returns LESSER, EQUALS, or GREATER depending on how
#    the letter portions of the morse code tuples compare.
#-------------------------------------------------------

def compare_letters( morse1, morse2 ):
    #unpack the tuples
    
    letter1,code1 = morse1
    letter2,code2 = morse2
    value = EQUALS
    if letter1 > letter2:
        value = GREATER
    elif letter1 < letter2:
        value = LESSER
     

    return value

#---------------------------------------------------------
# Compares the codes in two morse code letter, code pair.
#-------------------------------------------------------
# Parameters:
#    morse1, morse2 - morse code letter / code pairs.
# Preconditions:
#    morse1, morse2 - valid letter / code tuple.
# Postconditions:
#    Returns LESSER, EQUALS, or GREATER depending on how
#    the code portions of the morse code tuples compare.
#-------------------------------------------------------
def compare_codes( morse1, morse2 ):
    #unpack the tuples
    letter1,code1 = morse1
    letter2,code2 = morse2
    value = EQUALS
    if code1 > code2:
        value = GREATER
    elif code1 < code2:
        value = LESSER
     

    return value
#---------------------------------------------------------
# Prints a letter / code combination.
def print_( morse ):
    print "( '%s', '%s' )" % ( morse[0], morse[1] )
    return
