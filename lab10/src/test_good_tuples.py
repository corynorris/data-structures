import double_tuple

#other variables
numbers = ( 4, 5, 6 )
numbers2 = (1,)
numbers3 = (1,2,3,4,5,6)
floats = (1.0, 2.2,3.3)
strings = ('a','car','cat')
empty = ()

#try and unpack it into 3
a,b,c = double_tuple.good_double_tuple( numbers )

#try and print it
print a,b,c

#try and unpack it into 1
a, = double_tuple.good_double_tuple( numbers2 )

#try and print it
print a

#try and unpack it into 6
a,b,c,d,e,f = double_tuple.good_double_tuple( numbers3 )

#try and print it
print a,b,c,d,e,f

#try and unpack it into 6
a,b,c = double_tuple.good_double_tuple( floats )

#try and print it
print a,b,c

#try and unpack it into 6
a,b,c = double_tuple.good_double_tuple( strings )

#try and print it
print a,b,c
#try and unpack it into 6
a = double_tuple.good_double_tuple( empty )

#try and print it
print a