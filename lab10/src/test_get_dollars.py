#-------------------------------------------------------
# [program, library, or function name]
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# tests the get dollars function
#-------------------------------------------------------

import get_dollars

#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------



#-------------------------------------------------------
# Input variables
#-------------------------------------------------------
amount = 0
#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
twenties = 0
tens = 0
fives = 0
toonies = 0
loonies = 0


#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------


#-------------------------------------------------------
# get the input and test the function
#-------------------------------------------------------

amount = int(raw_input("Please enter the amount: ").strip())
twenties, tens, fives, toonies, loonies = get_dollars.get_dollars(amount)

print "The minimum representation of $%d is:"%(amount)
print "Twenties: %d"%(twenties)
print "Tens:     %d"%(tens)
print "Fives:    %d"%(fives)
print "Toonies:  %d"%(toonies)
print "Loonies:  %d"%(loonies)