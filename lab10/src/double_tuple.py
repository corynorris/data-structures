def double_tuple( values ):
    # Unpack the tuple.
    a, b, c = values
    # Update the individual tuple values.
    a = a * 2
    b = b * 2
    c = c * 2
    # Save and return a new tuple.
    new_values = ( a, b, c )
    return new_values

#input parameters
#values:contains a tuple


#-------------------------------------------------------
# Parameters:
#    values
# Preconditions:
#    values: must be a tuple
# Postconditions:
#    return_values: returns a new tuple
#-------------------------------------------------------
def good_double_tuple( values ):
    hold_tuple = ()
    for count in range(len(values)):
        hold_tuple = hold_tuple +(values[count]*2,)
        
    return hold_tuple

