#-------------------------------------------------------
# get_dollars
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# returns the minimum change to dispence
#-------------------------------------------------------

def get_dollars(amount):
#-------------------------------------------------------
# Input variables
#-------------------------------------------------------


#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    twenties = 0
    tens = 0
    fives = 0
    toonies = 0
    loonies = 0


#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    TWENTY = 20
    TEN = 10
    FIVE = 5
    TOONIE = 2
    LOONIE = 1
    
    '''
    QUARTER = 0.25
    DIME = 0.10
    NICKEL = 0.05
    PENNY = 0.01
    '''


#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------

    twenties = amount/TWENTY
    amount -= TWENTY * twenties
    tens = amount/TEN
    amount -= TEN*tens
    fives = amount/FIVE
    amount-= FIVE*fives
    toonies = amount/TOONIE
    amount-= TOONIE*toonies
    loonies = amount/LOONIE
    
    return (twenties,tens,fives,toonies,loonies)





