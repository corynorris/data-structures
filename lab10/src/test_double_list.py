import double_list
#other variables
numbers = [4, 5, 6]
numbers2 = [1]
numbers3 = [1,2,3,4,5,6]
floats = [1.0, 2.2,3.3]
strings = ['a','car','cat']
empty = []

#try and unpack it into 3
a,b,c = double_list.double_list( numbers )

#try and print it
print a,b,c

#try and unpack it into 1
a, = double_list.double_list( numbers2 )

#try and print it
print a

#try and unpack it into 6
a,b,c,d,e,f = double_list.double_list( numbers3 )

#try and print it
print a,b,c,d,e,f

#try and unpack it into 6
a,b,c = double_list.double_list( floats )

#try and print it
print a,b,c

#try and unpack it into 6
a,b,c = double_list.double_list( strings )

#try and print it
print a,b,c
#try and unpack it into 6
a = double_list.double_list( empty )

#try and print it
print a