#test double tuple
import double_tuple
#create a tuple
numbers = ( 4, 5, 6 )

#try and unpack it into 2
x1, x2 = double_tuple.double_tuple( numbers )

#try and print it
print x1, x2

''' Results
Traceback (most recent call last):
  File "I:\Eclipse\workspace\lab10\src\test_code.py", line 12, in <module>
    x1, x2 = double_tuple.double_tuple( numbers )
ValueError: too many values to unpack
'''

#fails because it doesn't make any sense
#double_tuple returns a tuple with 3 but we ask it for 2 values