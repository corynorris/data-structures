#-------------------------------------------------------
# Parameters:
#    values
# Preconditions:
#    values: must be a list
# Postconditions:
#    values: returns a new list
#-------------------------------------------------------
def double_list( values ):
    # loop through and update the list
    for count in range(len(values)):
       values[count] = values[count] *2 
#return the new list
    return values