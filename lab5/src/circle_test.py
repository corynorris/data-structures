#-------------------------------------------------------
# circle_test
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-
#-------------------------------------------------------
# tests the circle library
#-------------------------------------------------------

import circle

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

radius = 0.0

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

diameter = 0.0
circumference = 0.0
area = 0.0
#-------------------------------------------------------
# get the radius
#-------------------------------------------------------


radius = float(raw_input("Enter a radius: ").strip())

#-------------------------------------------------------
# test the functions
#-------------------------------------------------------

diameter = circle.circle_diameter(radius)
circumference = circle.circle_circumference(radius)
area = circle.circle_area(radius)


#-------------------------------------------------------
# show the results
#-------------------------------------------------------

print "The radius was %.2f, the diameter is %.2f and the circumference is %.2f" % (radius, diameter, circumference)
print "The area is %.2f"%(area)

#-------------------------------------------------------
# Show the phythag function
#-------------------------------------------------------

circle.pythag_circle(4.0, 5.0)

