#-------------------------------------------------------
# get_angles
#-------------------------------------------------------
# Author: David Brown
# ID: 
# Email: dbrown@wlu.ca
# Version: 2008-09-26
#-------------------------------------------------------
# Calculates the angles of three corners of a triangle.
#
# Parameters:
#    side_a, side_b, side_c: lengths of the three sides of the triangle.
# Preconditions:
#    side_a, side_b, side_c: floats > 0.
# Postconditions:
#    angle_a, angle_b, angle_c: contain three angles of triangle in radians. 
#-------------------------------------------------------

import math

def get_angles( side_a, side_b, side_c ):

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

    angle_a = 0.0    # Size of angle A.
    angle_b = 0.0    # Size of angle B.
    angle_c = 0.0    # Size of angle C.

#-------------------------------------------------------
# Other variables
#-------------------------------------------------------

    cos_a = 0.0    # Cosine of angle A.
    sin_b = 0.0    # Sine of angle B.

#-------------------------------------------------------
# Calculate the three angles.
#-------------------------------------------------------

    # Returns the angles of a triangle in radians given the lengths
    # of all three sides.
    cos_a = ( pow( side_b, 2 ) + pow( side_c, 2 ) - pow( side_a, 2 ) ) / ( 2 * side_b * side_c )
    # math.acos is the inverse of math.cos. 
    angle_a = math.acos( cos_a )
    sin_b = side_b / side_a * math.sin( angle_a )
    # math.asin is the inverse of math.sin.
    angle_b = math.asin( sin_b )
    angle_c = math.pi - ( angle_a + angle_b )
    return angle_a, angle_b, angle_c