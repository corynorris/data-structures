#-------------------------------------------------------
# circle_diameter
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-07
#-------------------------------------------------------
# calculates the diameter of a circle
#-------------------------------------------------------

import math

#-------------------------------------------------------
# Parameters:
#     radius: radius of the circle
# Preconditions:
#    radius: float > 0
# Postconditions:
#    diameter: returns the diameter of the circle 
#-------------------------------------------------------

def circle_diameter(radius):


#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

    diameter = 0.0

#-------------------------------------------------------
# Calculate the diameter 
#-------------------------------------------------------
    diameter = radius * 2.0
    return diameter






#-------------------------------------------------------
# gets the circumference of a circle
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    radius: the radius of the circle
# Preconditions:
#    radius: float > 0
# Postconditions:
#    circumference: contains the circumference of a circle of the given radius
#-------------------------------------------------------

def circle_circumference(radius):


#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

    circumference = 0.0

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

    diameter = 0.0

#-------------------------------------------------------
# calculate the circumference (pie * diameter)
#-------------------------------------------------------

    diameter = circle_diameter(radius)
    circumference = diameter * math.pi
    
    return circumference





#-------------------------------------------------------
# gets the area of a circle
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    radius: the radius of the circle
# Preconditions:
#    radius: float > 0
# Postconditions:
#    area: contains the area of a circle of the given radius
#-------------------------------------------------------

def circle_area(radius):


#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

    area = 0.0



#-------------------------------------------------------
# calculate the area (pie * radius**2)
#-------------------------------------------------------

    area = math.pi*(radius**2)
    
    return area



#-------------------------------------------------------
# gets the radius of a circle given the two non hypothenuse sides of a triangle within a circle
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    a,b: the two non hypothenuse sides of a triangle within a circle
# Preconditions:
#    a,b: floats > 0
# Postconditions:
#    does not return anything
#-------------------------------------------------------

def pythag_circle(a,b):


#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

    radius = 0.0
    diameter = 0.0
    circumference = 0.0
    area = 0.0

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
  

#-------------------------------------------------------
# get the radius using a and b
#-------------------------------------------------------

    radius = math.sqrt((a**2)+(b**2))

#-------------------------------------------------------
# calculate the values
#-------------------------------------------------------

    diameter = circle_diameter(radius)
    circumference = circle_circumference(radius)
    area = circle_area(radius)
    
#-------------------------------------------------------
# print the values
#-------------------------------------------------------

    print "radius: %.2f diameter: %.2f circumference: %.2f area: %.2f" % (radius, diameter, circumference, area)
    return

    
    