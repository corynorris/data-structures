#-------------------------------------------------------
# get_angles_test
#-------------------------------------------------------
# Author: David Brown
# ID: 
# Email: dbrown@wlu.ca
# Version: 2008-09-26
#-------------------------------------------------------
# Tests the function get_angles().
#-------------------------------------------------------
import get_angles
import math
#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

side1 = 0.0
side2 = 0.0
side3 = 0.0
#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

alpha = 0.0
beta = 0.0
gamma = 0.0

#-------------------------------------------------------
# Get the input.
#-------------------------------------------------------

print "Calculate the angles of a triangle:"
side1 = float(raw_input( "Enter the first side: " ).strip())
side2 = float(raw_input( "Enter the second side: " ).strip())
side3 = float(raw_input( "Enter the third side: " ).strip())
alpha, beta, gamma = get_angles.get_angles( side1, side2, side3 )

# Convert the results from radians to degrees.
alpha = math.degrees( alpha )
beta = math.degrees( beta )
gamma = math.degrees( gamma )

# Display the results.
print
print "The three angles are: %.1f, %.1f and %.1f degrees." % ( alpha, beta, gamma )

raw_input( "Press Enter to continue." ).strip()