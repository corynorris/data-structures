#-------------------------------------------------------
# get_first_customer
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# gets the customer with the highest cash
#-------------------------------------------------------

#-------------------------------------------------------
# Parameters:
#    file_a, id_number
# Preconditions:
#    file_a: file containing data
#    
# Postconditions:
#    record: list
#-------------------------------------------------------

def get_first_customer(file_a):


#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    first_customer = []

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    line = ""
    smallest = 0
    record = []   
   
        
#-------------------------------------------------------
# go to the correct line
#-------------------------------------------------------
    
    line = file_a.readline().strip()
    record = line.split(',')
    smallest = int(record[4])
    
    while line != '':        
       
        record = line.split(',')
        
        
        if int(record[4]) < int(smallest):
            smallest = record[4]
            first_customer = record
        
        line = file_a.readline().strip()
        
        
#-------------------------------------------------------
# return it
#-------------------------------------------------------    
        
    return first_customer