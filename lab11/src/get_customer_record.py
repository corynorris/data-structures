#-------------------------------------------------------
#get customer record
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# returns records from a file as  a tuple
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    file_a, record_number
# Preconditions:
#    file_a: file containing data
#    record_number: int>0
# Postconditions:
#    record: tuple
#-------------------------------------------------------

def get_customer_record(file_a, record_number):

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

    

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

    record = ("",)
#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    line = ""
    

#-------------------------------------------------------
# go to the correct line
#-------------------------------------------------------
    
    for count in range(0,record_number,1):
        line = file_a.readline().strip()
        if line == '':
            return None
        
#-------------------------------------------------------
# split up the line and make the tuple
#-------------------------------------------------------    
    
    record = line.split(','),
    
    return record


 