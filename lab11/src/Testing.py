#-------------------------------------------------------
# [program, library, or function name]
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# tests functions
#-------------------------------------------------------

import get_customer_record
import get_customer_by_id
import get_best_customer
import get_first_customer
import append_customer

#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------



#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

   #f 

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
result = ('',)

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------



#-------------------------------------------------------
# test get_customer_record
#-------------------------------------------------------

f = open( "customer.txt", "rb" )
result = get_customer_record.get_customer_record( f, 10 )
print "get by record: "
print result

#-------------------------------------------------------
# test get_customer_by_id
#-------------------------------------------------------

f = open( "customer.txt", "rb" )
result = get_customer_by_id.get_customer_by_id( f, "23456" )
print "get by id"
print result

f = open( "customer.txt", "rb" )
result = get_best_customer.get_best_customer(f)
print "best customer"
print result

f = open( "customer.txt", "rb" )
result = get_first_customer.get_first_customer(f)
print "first customer"
print result

f = open( "customer.txt", "rb+" )
data = ['35612', 'David', 'Brown', 237.56, '20081010']
append_customer.append_customer( f, data )
print "customer appended"

f.close()
