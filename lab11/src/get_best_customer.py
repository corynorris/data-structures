#-------------------------------------------------------
# get_best_customer
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# gets the customer with the highest cash
#-------------------------------------------------------

#-------------------------------------------------------
# Parameters:
#    file_a, id_number
# Preconditions:
#    file_a: file containing data
#    
# Postconditions:
#    record: list
#-------------------------------------------------------

def get_best_customer(file_a):


#-------------------------------------------------------
# Input variables
#-------------------------------------------------------




#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    best_customer = []

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    line = ""
    highest = 0.0;
    record = []    
#-------------------------------------------------------
# go to the correct line
#-------------------------------------------------------
    
    line = file_a.readline().strip()
    while line != '':        
       
        record = line.split(',')
 
        if (float(record[3]) > float(highest)):
            highest = record[3]
            best_customer = record
        
        line = file_a.readline().strip()
        
        
#-------------------------------------------------------
# return it
#-------------------------------------------------------    
    
 
    
    return best_customer