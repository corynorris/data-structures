#-------------------------------------------------------
# [program, library, or function name]
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2009-01-19
#-------------------------------------------------------
# tests 
#-------------------------------------------------------

from stack_array import *
from list_to_stack import *
from stack_to_list import *
import album

#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------



#-------------------------------------------------------
# Input variables
#-------------------------------------------------------
stack = {}



#-------------------------------------------------------
# Output variables
#-------------------------------------------------------


#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

value = {}

#-------------------------------------------------------
# Fill a list to test, and show that it is full
#-------------------------------------------------------
stack = stack_init(album.print_)
list = album.read_file('albums.txt')
print 'list before'
for x in list:
    print x


#-------------------------------------------------------
# empty the list into the stack
#-------------------------------------------------------
list_to_stack(list, stack)

#-------------------------------------------------------
# show that is has been emptied and the stack now contains the list
#-------------------------------------------------------
print 'list after'
for x in list:
    print x
    
print 'stack after'
while (stack_empty(stack) != 1):
     value = stack_pop(stack)
     print "popped %s" %(value)
     
#refill list     
list = album.read_file('albums.txt')
list_to_stack(list, stack)
         
#switch them back
print 'list after conversion from stack'
list = stack_to_list(stack,list)
for x in list:
    print x
#show that the stack is empty
print 'stack after conversion from stack to list'
while (stack_empty(stack) != 1):
     value = stack_pop(stack)
     print "popped %s" %(value)

   