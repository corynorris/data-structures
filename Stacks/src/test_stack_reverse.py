import album
from list_to_stack import *
from stack_linked import *

#set up a stack to flip
stack = stack_init(album.print_)
list = album.read_file('albums.txt')


print 'Before stack_reverse'
for items in list:
    print items

list_to_stack(list,stack)

#flip said stack
stack_reverse(stack)
print 'After stack_reverse'
while (stack_empty(stack) != 1):
     value = stack_pop(stack)
     print "popped %s" %(value)