#-------------------------------------------------------
# [program, library, or function name]
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2009-01-19
#-------------------------------------------------------
# tests 
#-------------------------------------------------------

from r_stack import *
from list_to_stack import *
import album

#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------



#-------------------------------------------------------
# Input variables
#-------------------------------------------------------
stack = {}



#-------------------------------------------------------
# Output variables
#-------------------------------------------------------


#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

value = {}

#-------------------------------------------------------
# set up a stack to flip
#-------------------------------------------------------
stack = stack_init(album.print_)
list = album.read_file('albums.txt')
list_to_stack(list,stack)
#flip the stack
r_stack(stack)
#print it
while (stack_empty(stack) != 1):
     value = stack_pop(stack)
     print "popped %s" %(value)