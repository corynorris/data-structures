#-------------------------------------------------------
# list_to_stack
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2009-01-19
#-------------------------------------------------------
# reverses a stack
#-------------------------------------------------------

from stack_to_list import *
from list_to_stack import *
def r_stack(stack):

#-------------------------------------------------------
# Parameters:
#    stack
# Preconditions:
#    stack: a stack
#    
# Postconditions:
#    reverses a stack
#-------------------------------------------------------



#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

    

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------


#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    list = []

#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------
    list = stack_to_list(stack,list)
    list.reverse()
    list_to_stack(list,stack)
    #alternatively I could pop it into a list and convert the list
    return
    