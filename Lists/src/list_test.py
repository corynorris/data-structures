from list_array import *
import album

def my_compare(a,b):
    if a['artist'] == b:
        return 0


l1 = list_init( album.print_, my_compare )
l2 = copy.deepcopy(l1)#diff since l1 will have items removed.
l3 = l1#same since l3 points to l1 and will imitate it.

album_list = album.read_file('albums.txt')

for track in album_list:   
    list_insert(l1,track)
    
list_print(l1,'f')




found = list_find(l1,'Oldfield, Mike')

print "FOUND:"
print found

print "LIST AFTER FOUND"
list_print(l1,'f')
remove = list_remove(l1,'Oldfield, Mike' )
print "removed:"
print remove 

print "LIST AFTER REMOVE"
list_print(l1,'f')

print "TESTING IF LISTS ARE IDENTICAL (false)"
print list_identical(l1,l2)
print "TESTING IF LISTS ARE IDENTICAL (true)"
print list_identical(l1,l3)
