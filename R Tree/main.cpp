#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>
#include "rtree.h"

using namespace std;

const int MAX_CMD_LEN = 256;
const int DOMAIN_SIZE = 10000;


void error(const char* cmd)
{
	cerr << "Error: " << cmd << endl;
}

void help()
{
	cout << "============================================================================\n";
	cout << "Commands:\n";
	cout << "============================================================================\n";
	cout << "i x(int) y(int) rid(int) : insert a record with key (x, y)\n";
	cout << "d x(int) y(int) : delete the record with key (x, y)\n";
	cout << "ri s(int) num(int) : random insertions of num records with seed s\n";
	cout << "rd s(int) num(int) : random deletions of num records with seed s\n";
	cout << "qp x(int) y(int) : query the record with key (x, y)\n";
	cout << "qr xmin(int) xmax(int) ymin(int) ymax(int) : find records with key (x, y)\n";
	cout << "     where xmin<=x<=xmax and ymin<=y<=ymax\n";
	cout << "s : print the statistic information of the tree\n";
	cout << "p : print the tree\n";
	cout << "h : show this help menu\n";
	cout << "x : exit\n";
	cout << "============================================================================\n";
}



bool process(char* cmd, RTree& tree)
{
	const int MAX_ARG_NUM = 5; // at most 5 arguments
	char* args[MAX_ARG_NUM];
	char msg[1024]; // error message.

	int num_arg = 0;
	char* token = strtok(cmd, " \t");
	for (; token != NULL && num_arg < MAX_ARG_NUM; num_arg++) {
		args[num_arg] = token;
		token = strtok(NULL, " \t");
	}
	if (num_arg == 0 || token != NULL) {
		sprintf(msg, "Wrong number of command arguments");
		error(msg);
		return true;
	}
	if (strcmp(args[0], "i") == 0) { // insertion.
		if (num_arg != 4) {
			sprintf(msg, "Wrong number of arguments for command 'i'");
			error(msg);
		}
		else {
			int x = atoi(args[1]);
			int y = atoi(args[2]);
			int rid = atoi(args[3]);
			try {
				if (tree.insert(x, y, rid))
					cout << "Insertion done.\n";
				else
					cout << "Insertion failed.\n";
			}
			catch (bad_alloc& ba)  {
				sprintf(msg, "bad_alloc caught <%s> ", ba.what());
				error(msg);
			}
		}
		return true;
	}
	else if (strcmp(args[0], "d") == 0) { // deletion.
		if (num_arg != 3) {
			sprintf(msg, "Wrong number of arguments for command 'd'");
			error(msg);
		}
		else {
			int x = atoi(args[1]);
			int y = atoi(args[2]);
			if (tree.del(x, y))
				cout << "Deletion done.\n";
			else
				cout << "Deletion failed.\n";
		}
		return true;
	}
	else if (strcmp(args[0], "ri") == 0) { // random insertion.
		if (num_arg != 3) {
			sprintf(msg, "Wrong number of arguments for command 'ri'");
			error(msg);
		}
		else {
			srand(atoi(args[1]));
			int num = atoi(args[2]);
			int succeed = 0;
			for (int i = 0; i < num; i++) {
				int x = rand() % DOMAIN_SIZE;
				int y = rand() % DOMAIN_SIZE;
				int rid = rand();
				try {
					if (tree.insert(x, y, rid)) {
						succeed++;
					}
				}
				catch (bad_alloc& ba)  {
					sprintf(msg, "bad_alloc caught <%s> ", ba.what());
					error(msg);
				}
			}
			cout << succeed << " out of " << num << " insertion(s) suceeded.\n";
		}
		return true;
	}
	else if (strcmp(args[0], "rd") == 0) { // random deletion.
		if (num_arg != 3) {
			sprintf(msg, "Wrong number of arguments for command 'rd'");
			error(msg);
		}
		else {
			srand(atoi(args[1]));
			int num = atoi(args[2]);
			int succeed = 0;
			for (int i = 0; i < num; i++) {
				int x = rand() % DOMAIN_SIZE;
				int y = rand() % DOMAIN_SIZE;
				int dummy = rand(); // to be compatible with ``ri''.
				if (tree.del(x, y)) {
					succeed++;
				}
			}
			cout << succeed << " out of " << num << " deletion(s) suceeded.\n";
		}
		return true;
	}
	else if (strcmp(args[0], "qr") == 0) { // range query.
		if (num_arg != 5) {
			sprintf(msg, "Wrong number of arguments for command 'qr'");
			error(msg);
		}
		else {
			BoundingBox mbr;
			mbr.xmin = atoi(args[1]);
			mbr.xmax = atoi(args[2]);
			mbr.ymin = atoi(args[3]);
			mbr.ymax = atoi(args[4]);
			if (mbr.xmin > mbr.xmax || mbr.ymin > mbr.ymax) {
				sprintf(msg, "Invalid query range {xmin=%d,xmax=%d,ymin=%d,ymax=%d}.", mbr.xmin,mbr.xmax,mbr.ymin,mbr.ymax);
				error(msg);
			}
			else {
				int result_count = 0;
				int node_travelled = 0;
				tree.query_range(mbr, result_count, node_travelled);
				cout << "Number of results: " << result_count << endl;
				cout << "Number of nodes visited: " << node_travelled << endl;
			}
		}
		return true;
	}
	else if (strcmp(args[0], "qp") == 0) { // point query.
		if (num_arg != 3) {
			sprintf(msg, "Wrong number of arguments for command 'qp'");
			error(msg);
		}
		else {
			Entry result;
			int x = atoi(args[1]);
			int y = atoi(args[2]);
			if (tree.query_point(x, y, result)) {
				cout << "Record: <" << result.mbr.xmin << ", " << result.mbr.ymin << ", " << result.rid << ">\n";
			}
			else {
				cout << "Record not found.\n";
			}
		}
		return true;
	}
	else if (strcmp(args[0], "s") == 0) { // statistics.
		tree.stat();
		return true;
	}
	else if (strcmp(args[0], "p") == 0) { // print tree.
		tree.print_tree();
		return true;
	}
	else if (strcmp(args[0], "h") == 0) { // print help menu.
		help();
		return true;
	}
	else if (strcmp(args[0], "x") == 0) { // exit
		return false;
	}
	else {
		sprintf(msg, "Invalid command '%s'.\nType 'h' to print the help menu.", args[0]);
		error(msg);
		return true;
	}
}



int main(int argc, char *argv[])
{
	if (argc < 2) {
		cerr << "Usage: " << argv[0] << " Max_#entries_in_a_node\" [file_containing_commmands].\n";
		return 0;
	}

	// Create an R-tree.
	int max_entry_num = atoi(argv[1]);
	if (max_entry_num < 2) {
		cerr << "Number of entries should be an integer > 2.\n";
		return 0;
	}
	RTree tree(max_entry_num);

	// Processing input commands.
	char command[MAX_CMD_LEN];
	if (argc == 3) {
		ifstream fin(argv[2]);
		while (fin.getline(command, MAX_CMD_LEN)) {
			if (! process(command, tree))
				break;
		}
	}
	else {
		while (true) {
			cout << ">> ";
			cin.getline(command, MAX_CMD_LEN);
			if (! process(command, tree))
				break;
		}
	}

	return 0;
}
