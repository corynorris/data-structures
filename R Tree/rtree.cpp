/* Implementations of R tree */
#include <iostream>
#include <cstring>
#include <cmath>
#include "rtree.h"

#include <vector>

using namespace std;


RTNode::RTNode(int lev, int s)
{
    entry_num = 0;
    entries = new Entry[s];
    level = lev;
    size = s;
}

RTNode::RTNode(const RTNode& other)
{
    entries = new Entry[other.size];
    *this = other;
}

RTNode& RTNode::operator=(const RTNode& other)
{
    if (&other != this) {
        entry_num = other.entry_num;
        level = other.level;
        size = other.size;
        for (int i = 0; i < entry_num; i++)
            entries[i] = other.entries[i];
    }
    return *this;
}


RTNode::~RTNode()
{
    if (level != 0) {
        for (int i = 0; i < entry_num; i++) {
            delete entries[i].ptr;
            entries[i].ptr = NULL;
        }
    }
    delete []entries;
    entries = NULL;
}


RTree::RTree(int entry_num)
{
    max_entry_num = entry_num;
    root = new RTNode(0, entry_num);
}

RTree::~RTree()
{
    delete root;
    root = NULL;
}



bool RTree::del(int x, int y)
{

    //keep track of if we deleted anything
    bool statusDelete = false;


    //Create an entry with x and y
    Entry e;
    e.mbr.xmin = e.mbr.xmax = x;
    e.mbr.ymin = e.mbr.ymax = y;
    e.ptr = NULL; // invalid filed.

    // stack contains the path to the leaf (not including the leaf node).

    RTNode** stack = new RTNode*[root->level];
    int stack_size = 0;
    // entry_idx contains the index of each entry in the node from the path.
    int* entry_idx = new int[root->level];


    //invoke FindLeaf to locate the leaf node L containing E
    //find_leaf finds and deletes the entry e from L
     RTNode* L = find_leaf(root, stack, entry_idx, stack_size, e);
    if (L)
    {
        statusDelete = true;
        //invoke CondenseTree on L
        RTNode* node = L;

          //let set Q = the set of eliminated nodes to be empty
        vector<RTNode*> q;

        while (stack_size > 0) {


            //let P be the parent of N and let En be N's entries in P
            stack_size--;
            RTNode* parent = stack[stack_size];
            int idx = entry_idx[stack_size];

            //if N has fewer than m entries (
            int m = ceil(max_entry_num/2);
            if (node->entry_num < m)
            {
                //delete En from P
                swap_entry(parent->entries, idx, parent->entry_num-1); // move the record the the end to indicate ``deleted''
                parent->entry_num--;

                //Add N to set Q
                q.push_back(node);
            } else {
                //Adjust En I to tightly contain all entries in N
                parent->entries[idx].mbr = get_mbr(node->entries, node->entry_num);

            }

            //Move up a level in the tree, set N = P and repeat
            node = parent;


        }

        //Re-insert all entries of nodes in set Q, biggest to smallest
        while (!q.empty()){
            RTNode* reInsert = q.back();
            q.pop_back();


            if (reInsert->entry_num > 1)
            {

                //sort the entries
                // (bubble) sort the entries in the remaining set
                // last one should be picked first.
                int remain = reInsert->entry_num;
                for (int i = 1; i < remain; i++) {
                    for (int j = 0; j < remain - i; j++) {
                        if (tie_breaking(reInsert->entries[j].mbr, reInsert->entries[j+1].mbr)) {
                            swap_entry(reInsert->entries, j, j+1);
                        }
                    }
                }

            }
            for (int i = reInsert->entry_num -1; i >= 0; i--)
            {
                //insert x, y, rid

                insert(reInsert->entries[i], reInsert->level);

            }

        }


        //If the root note has only one child entry after the tree
        //has been adjusted, make the child the new root
        if (root->entry_num == 1 )
        {
            if (root->entries[0].ptr != NULL){

                 root = root->entries[0].ptr;

            }
        }
    }



    delete[] stack;
    delete[] entry_idx;


    return statusDelete;
}

//
// Check whether two entries are the same.
// Return true if same, otherwise false.
//
bool RTree::same_entry(const Entry& e1, const Entry& e2)
{
    const BoundingBox& mbr1 = e1.mbr;
    const BoundingBox& mbr2 = e2.mbr;
    return mbr1.xmax == mbr2.xmax && mbr1.xmin == mbr2.xmin && mbr1.ymax == mbr2.ymax && mbr1.ymin == mbr2.ymin;
}


//
// Check whether two boundingbox are the same.
// Return true if same, otherwise false.
//
bool RTree::overlap(const BoundingBox box1, const BoundingBox box2)
{
    return box2.xmax >= box1.xmin && box1.xmax >= box2.xmin && box2.ymax >= box1.ymin && box1.ymax >= box2.ymin;
}


//
// Update the current MBR ``mbr'' with the merging result of ``mbr'' and ``new_mbr''
//
void RTree::update_mbr(BoundingBox& mbr, const BoundingBox& new_mbr)
{
    mbr.xmin = min(mbr.xmin, new_mbr.xmin);
    mbr.xmax = max(mbr.xmax, new_mbr.xmax);
    mbr.ymin = min(mbr.ymin, new_mbr.ymin);
    mbr.ymax = max(mbr.ymax, new_mbr.ymax);
}


//
// Calcuate the MBR of a set of entries, of size ``len''.
//
BoundingBox RTree::get_mbr(Entry* entry_list, int len)
{
    BoundingBox mbr = entry_list[0].mbr;
    for (int i = 1; i < len; i++) {
        mbr.xmin = min(mbr.xmin, entry_list[i].mbr.xmin);
        mbr.xmax = max(mbr.xmax, entry_list[i].mbr.xmax);
        mbr.ymin = min(mbr.ymin, entry_list[i].mbr.ymin);
        mbr.ymax = max(mbr.ymax, entry_list[i].mbr.ymax);
    }
    return mbr;
}


//
// Return the area of a boundingbox ``mbr''.
//
int RTree::area(const BoundingBox& mbr)
{
    return (mbr.ymax - mbr.ymin) * (mbr.xmax - mbr.xmin);
}


//
// Swap two entries: entry_list[id1] and entry_list[id2].
//
void RTree::swap_entry(Entry* entry_list, int id1, int id2)
{
    Entry temp = entry_list[id1];
    entry_list[id1] = entry_list[id2];
    entry_list[id2] = temp;
}


//
// Calculate the area enlarged by add the new entry to the existing MBR.
//
int RTree::area_inc(const BoundingBox& mbr, const BoundingBox& entry_mbr)
{
    BoundingBox new_mbr;
    new_mbr.xmin = min(mbr.xmin, entry_mbr.xmin);
    new_mbr.xmax = max(mbr.xmax, entry_mbr.xmax);
    new_mbr.ymin = min(mbr.ymin, entry_mbr.ymin);
    new_mbr.ymax = max(mbr.ymax, entry_mbr.ymax);
    return area(new_mbr) - area(mbr);
}

//
// Linear Pick Seeds algorithm for Lienar Cost Algorithm.
//
void RTree::linear_pick_seeds(Entry* entry_list, int len, int& m1, int& m2)
{
    int iminx = 0, imaxx = 0, iminy = 0, imaxy = 0;
    // pick the two entries with the largest gap on each dimension
    for (int i = 1; i < len; i++) {
        //highest low side on x dimension
        if (entry_list[i].mbr.xmin > entry_list[iminx].mbr.xmin) {
            iminx = i;
        }
        else if (entry_list[i].mbr.xmin == entry_list[iminx].mbr.xmin) {
            if (tie_breaking(entry_list[i].mbr, entry_list[iminx].mbr)) {
                iminx = i;
            }
        }
        //highest low side on y dimension
        if (entry_list[i].mbr.ymin > entry_list[iminy].mbr.ymin) {
            iminy = i;
        }
        else if (entry_list[i].mbr.ymin == entry_list[iminy].mbr.ymin) {
            if (tie_breaking(entry_list[i].mbr, entry_list[iminy].mbr)) {
                iminy = i;
            }
        }
        //lowest high side on x dimension
        if (entry_list[i].mbr.xmax < entry_list[imaxx].mbr.xmax) {
            imaxx = i;
        }
        else if (entry_list[i].mbr.xmax == entry_list[imaxx].mbr.xmax) {
            if (tie_breaking(entry_list[i].mbr, entry_list[imaxx].mbr)) {
                imaxx = i;
            }
        }
        //lowest high side on y dimension
        if (entry_list[i].mbr.ymax < entry_list[imaxy].mbr.ymax) {
            imaxy = i;
        }
        else if (entry_list[i].mbr.ymax == entry_list[imaxy].mbr.ymax) {
            if (tie_breaking(entry_list[i].mbr, entry_list[imaxy].mbr)) {
                imaxy = i;
            }
        }
    }
    BoundingBox box = get_mbr(entry_list, len);
    double normalized_x_dis = 0;
    if (box.xmax - box.xmin != 0)
        normalized_x_dis = abs(entry_list[iminx].mbr.xmin - entry_list[imaxx].mbr.xmax) * 1.0 / (box.xmax - box.xmin);
    double normalized_y_dis = 0;
    if (box.ymax - box.ymin != 0)
        normalized_y_dis = abs(entry_list[iminy].mbr.ymin - entry_list[imaxy].mbr.ymax) * 1.0 / (box.ymax - box.ymin);
    if(normalized_x_dis - normalized_y_dis >= -EPSILON) {
        m1 = iminx;
        m2 = imaxx;
    }
    else {
        m1 = iminy;
        m2 = imaxy;
    }
    if(m1 == m2) {
        m2 = (m1 == 0 ? 1 : 0);
        for (int i = 1; i < len; i++) {
            if(i != m1 && i != m2) {
                if(tie_breaking(entry_list[i].mbr, entry_list[m2].mbr))
                    m2 = i;
            }
        }
    }
}


//
// Find the leaf node and delete the ``record''.
//
RTNode* RTree::find_leaf(RTNode* node, RTNode** stack, int* entry_idx, int& stack_size, const Entry& record)
{
    if (node->level == 0) {
        for (int i = 0; i < node->entry_num; i++) {
            if (overlap(node->entries[i].mbr, record.mbr)) {
                swap_entry(node->entries, i, node->entry_num-1); // move the record the the end to indicate ``deleted''
                node->entry_num--;
                return node;
            }
        }
    }
    else {
        for (int i = 0; i < node->entry_num; i++) {
            if (overlap(node->entries[i].mbr, record.mbr)) {
                stack[stack_size] = node;
                entry_idx[stack_size] = i;
                stack_size++;
                RTNode* ret = find_leaf(node->entries[i].ptr, stack, entry_idx, stack_size, record);
                if (ret != NULL) {
                    return ret;
                }
                stack_size--;
            }
        }
    }
    return NULL;
}

//
// Find the node to insert the new entry ``e'' at the specified level ``dest_level''.
// In particular, find the leaf node for new record if ``dest_level == 0''.
//
RTNode* RTree::choose_leaf(RTNode** stack, int* entry_idx, int& stack_size, const Entry& e, int dest_level)
{
    RTNode* node = root;
    while (node->level != dest_level) {
        int min_idx = 0;
        int min_enlargement = area_inc(node->entries[0].mbr, e.mbr);
        for (int i = 1; i < node->entry_num; i++) {
            // compare with other entries
            int cur_enlargement = area_inc(node->entries[i].mbr, e.mbr);
            if (cur_enlargement < min_enlargement) {
                min_idx = i;
                min_enlargement = cur_enlargement;
            }
            else if (cur_enlargement == min_enlargement) {
                // do not need to change min_enlargement as they are the same.
                int cur_area = area(node->entries[i].mbr);
                int min_area = area(node->entries[min_idx].mbr);
                // select the one with min area.
                if (cur_area < min_area) {
                    min_idx = i;
                }
                else if (cur_area == min_area) {
                    // tie breaking
                    if (tie_breaking(node->entries[i].mbr, node->entries[min_idx].mbr)) {
                        min_idx = i;
                    }
                }
            }
        }
        stack[stack_size] = node;
        entry_idx[stack_size] = min_idx;
        stack_size++;
        node = node->entries[min_idx].ptr;
    }
    return node;
}


//
// Adjust the MBR of nodes involved in insertion.
//
void RTree::adjust_tree(RTNode** stack, int* entry_idx, int size)
{
    while (size > 0) {
        size--;
        RTNode* node = stack[size]->entries[entry_idx[size]].ptr;
        stack[size]->entries[entry_idx[size]].mbr = get_mbr(node->entries, node->entry_num);
    }
}


//
// Helper function for query_range(), with range specified in ``mbr''.
// Return: number of results in ``result_cnt''.
//		number of R-tree nodes traveled in ``node_traveled''.
void RTree::query_range(const RTNode* node, const BoundingBox mbr, int& result_cnt, int& node_traveled)
{
    node_traveled++;
    if (node->level == 0) {
        for (int i = 0;i < node->entry_num;i++) {
            if (overlap(node->entries[i].mbr, mbr)) {
                result_cnt++;
            }
        }
    } else {
        for (int i = 0;i < node->entry_num; i++) {
            if (overlap(node->entries[i].mbr, mbr)) {
                query_range(node->entries[i].ptr, mbr, result_cnt, node_traveled);
            }
        }
    }
}


//
// Helper function for point_query().
//
bool RTree::query_point(const RTNode* node, const BoundingBox& mbr, Entry& result)
{
    if (node->level == 0) {
        for (int i = 0; i < node->entry_num; i++) {
            if (overlap(node->entries[i].mbr, mbr)) {
                result = node->entries[i];
                return true;
            }
        }
    }
    else {
        for (int i = 0; i < node->entry_num; i++) {
            if (overlap(node->entries[i].mbr, mbr)) {
                if (query_point(node->entries[i].ptr, mbr, result)) {
                    return true;
                }
            }
        }
    }
    return false;
}	


bool RTree::insert(int x, int y, int rid)
{
    Entry e;
    e.mbr.xmin = e.mbr.xmax = x;
    e.mbr.ymin = e.mbr.ymax = y;
    e.rid = rid;
    e.ptr = NULL; // invalid filed.
    return insert(e, 0);
}


//
// Helper function for insertion.
//
bool RTree::insert(const Entry& e, int dest_level)
{
    /*
       Add your codes here
       */
    Entry dummy;
    if (dest_level == 0 && query_point(root, e.mbr, dummy)) {
        return false;
    }

    // stack contains the path to the leaf (not including the leaf node).
    RTNode** stack = new RTNode*[root->level];
    int stack_size = 0;
    // entry_idx contains the index of each entry in the node from the path.
    int* entry_idx = new int[root->level];

    RTNode* leaf = choose_leaf(stack, entry_idx, stack_size, e, dest_level);
    // check if there is space for the new entry
    if (leaf->entry_num < max_entry_num) {
        leaf->entries[leaf->entry_num] = e;
        leaf->entry_num++;
        adjust_tree(stack, entry_idx, stack_size);
        delete[] stack;
        delete[] entry_idx;
        return true;
    }


    // split is needed.
    bool split = true;
    RTNode* node = leaf;
    Entry new_entry = e;
    while (split) {
        Entry* entry_buffer = new Entry[node->entry_num + 1];
        for (int i = 0; i < node->entry_num; i++) {
            entry_buffer[i] = node->entries[i];
        }
        entry_buffer[max_entry_num] = new_entry;
        int m1, m2;
        linear_pick_seeds(entry_buffer, max_entry_num+1, m1, m2);
        RTNode* new_node = new RTNode(node->level, max_entry_num);
        node->entries[0] = entry_buffer[m1];
        node->entry_num=1;
        new_node->entries[0] = entry_buffer[m2];
        new_node->entry_num = 1;
        // move the selected nodes to the end of the buffer
        swap_entry(entry_buffer, m2, max_entry_num);
        if (m1 == max_entry_num) {
            m1 = m2;
        }
        swap_entry(entry_buffer, m1, max_entry_num-1);
        // (bubble) sort the entries in the remaining set
        // last one should be picked first.
        int remain = max_entry_num-1;
        for (int i = 1; i < remain; i++) {
            for (int j = 0; j < remain - i; j++) {
                if (tie_breaking(entry_buffer[j].mbr, entry_buffer[j+1].mbr)) {
                    swap_entry(entry_buffer, j, j+1);
                }
            }
        }
        // split procedure
        int max_split_size = (max_entry_num) / 2 + 1;
        BoundingBox old_mbr = node->entries[0].mbr;
        BoundingBox new_mbr = new_node->entries[0].mbr;
        while (node->entry_num < max_split_size && new_node->entry_num < max_split_size) {
            int old_inc = area_inc(old_mbr, entry_buffer[remain-1].mbr);
            int new_inc = area_inc(new_mbr, entry_buffer[remain-1].mbr);
            bool add_to_old = false;
            if (old_inc != new_inc) // less enlargement better.
                add_to_old = old_inc < new_inc;
            else if (area(old_mbr) != area(new_mbr)) // smaller area better.
                add_to_old = area(old_mbr) < area(new_mbr);
            else if (node->entry_num != new_node->entry_num) // fewer entries num better.
                add_to_old = node->entry_num < new_node->entry_num;
            else
                add_to_old = tie_breaking(old_mbr, new_mbr);

            if (add_to_old) {
                node->entries[node->entry_num] = entry_buffer[remain-1];
                node->entry_num++;
                update_mbr(old_mbr, entry_buffer[remain-1].mbr);
            }
            else {
                new_node->entries[new_node->entry_num] = entry_buffer[remain-1];
                new_node->entry_num++;
                update_mbr(new_mbr, entry_buffer[remain-1].mbr);
            }
            remain--;
        }
        // one node reaches max num nodes, assign the remaining to the other node
        if (node->entry_num == max_split_size) {
            for (int i = remain-1; i >= 0; i--) {
                new_node->entries[new_node->entry_num] = entry_buffer[i];
                new_node->entry_num++;
                update_mbr(new_mbr, entry_buffer[i].mbr);
            }
        }
        else {
            for (int i = remain-1; i >= 0; i--) {
                node->entries[node->entry_num] = entry_buffer[i];
                node->entry_num++;
                update_mbr(old_mbr, entry_buffer[i].mbr);
            }
        }
        // two nodes now. go to a higher level
        if (stack_size == 0) {
            // root reached.
            RTNode* new_root = new RTNode(node->level+1, max_entry_num);
            new_root->entries[0].mbr = old_mbr;
            new_root->entries[0].ptr = node;
            new_root->entries[1].mbr = new_mbr;
            new_root->entries[1].ptr = new_node;
            new_root->entry_num = 2;
            root = new_root;
            split = false;
        }
        else {
            stack_size--;
            RTNode* parent = stack[stack_size];
            int idx = entry_idx[stack_size];
            parent->entries[idx].mbr = old_mbr;
            new_entry.mbr = new_mbr;
            new_entry.ptr = new_node;
            if (parent->entry_num < max_entry_num) {
                parent->entries[parent->entry_num] = new_entry;
                parent->entry_num++;
                split = false;
            }
            else
                node = parent;
        }

        delete []entry_buffer;
    }
    adjust_tree(stack, entry_idx, stack_size);
    delete []stack;
    delete []entry_idx;
    return true;
}


void RTree::query_range(const BoundingBox& mbr, int& result_count, int& node_travelled)
{
    /*
       Add your codes here
       */
    result_count = 0;
    node_travelled = 0;
    query_range(root, mbr, result_count, node_travelled);
}


bool RTree::query_point(int x, int y, Entry& result)
{
    BoundingBox mbr;
    mbr.xmin = mbr.xmax = x;
    mbr.ymin = mbr.ymax = y;
    return query_point(root, mbr, result);
}


/**********************************
 *
 * Please do not modify the codes below
 *
 **********************************/

/*********************************************************
  The order is: left most -> highest -> lowest -> right most
  If the two boxes is the same, choose box1
 *********************************************************/
bool RTree::tie_breaking(const BoundingBox& box1, const BoundingBox& box2)
{
    if (box1.xmin != box2.xmin)
        return box1.xmin < box2.xmin;
    else if (box1.ymax != box2.ymax)
        return box1.ymax > box2.ymax;
    else if (box1.ymin != box2.ymin)
        return box1.ymin < box2.ymin;
    else if (box1.xmax != box2.xmax)
        return box1.xmax > box2.xmax;
    else
        return true;
}


void RTree::stat(RTNode* node, int& record_cnt, int& node_cnt)
{
    if (node->level == 0) {
        record_cnt += node->entry_num;
        node_cnt++;
    }
    else {
        node_cnt++;
        for (int i = 0; i < node->entry_num; i++)
            stat((node->entries[i]).ptr, record_cnt, node_cnt);
    }
}

void RTree::stat()
{
    int record_cnt = 0, node_cnt = 0;
    stat(root, record_cnt, node_cnt);
    cout << "Height of R-tree: " << root->level + 1 << endl;
    cout << "Number of nodes: " << node_cnt << endl;
    cout << "Number of records: " << record_cnt << endl;
}


void RTree::print_node(RTNode* node, int indent_level)
{
    BoundingBox mbr = get_mbr(node->entries, node->entry_num);

    char* indent = new char[4*indent_level+1];
    memset(indent, ' ', sizeof(char) * 4 * indent_level);
    indent[4*indent_level] = '\0';

    if (node->level == 0) {
        cout << indent << "Leaf node (level = " << node->level << ") mbr: (" << mbr.xmin << " " << mbr.xmax
             << " " << mbr.ymin << " " << mbr.ymax << ")\n";
    }
    else {
        cout << indent << "Non leaf node (level = " << node->level << ") mbr: (" << mbr.xmin << " " << mbr.xmax
             << " " << mbr.ymin << " " << mbr.ymax << ")\n";
    }

    Entry *copy = new Entry[node->entry_num];
    for (int i = 0; i < node->entry_num; i++) {
        copy[i] = node->entries[i];
    }

    for (int i = 0; i < node->entry_num; i++) {
        int index = 0; // pick next.
        for (int j = 1; j < node->entry_num - i; j++) {
            if (tie_breaking(copy[j].mbr, copy[index].mbr)) {
                index = j;
            }
        }

        if (node->level == 0) {
            Entry& e = copy[index];
            cout << indent << "    Entry: <" << e.mbr.xmin << ", " << e.mbr.ymin << ", " << e.rid << ">\n";
        }
        else {
            print_node(copy[index].ptr, indent_level+1);
        }
        // Move the output one to the rear.
        Entry tmp = copy[node->entry_num - i - 1];
        copy[node->entry_num - i - 1] = copy[index];
        copy[index] = tmp;

    }

    delete []indent;
    delete []copy;
}

void RTree::print_tree()
{
    if (root->entry_num == 0)
        cout << "The tree is empty now." << endl;
    else
        print_node(root, 0);
}
