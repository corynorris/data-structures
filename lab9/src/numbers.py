#-------------------------------------------------------
# numbers
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-4
#-------------------------------------------------------
# finds the average, lowest and highest numbers in a given set
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    average: float representing the average of the numbers entered
#    smallest, largest: integers representing the smallest and largest values entered
#-------------------------------------------------------

def numbers():

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------
    num_numbers = 0
    number = 0

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    smallest = 0
    largest = 0
    average = 0.0

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

    cur_number = 0
    total_numbers = 0.0

#-------------------------------------------------------
# Get the data from the user
#-------------------------------------------------------

    num_numbers = int(raw_input("Enter the number of values to compare: ").strip())

    number = int(raw_input("Value %d: " %(cur_number + 1)).strip())
    largest = smallest = number  
    total_numbers += number
    cur_number += 1
    
    for cur_number in range(1,num_numbers):
    #while cur_number < num_numbers:
         
         number = int(raw_input("Value %d: " %(cur_number + 1)).strip())
      
             
       
         if number < smallest:
            smallest = number
         if number > largest:
            largest = number
        
         total_numbers += number
         cur_number += 1
    
#-------------------------------------------------------
# Calculate average and return all variables
#-------------------------------------------------------
    
    average = total_numbers / float(num_numbers)
        
    return average, smallest, largest
    


    
    
    