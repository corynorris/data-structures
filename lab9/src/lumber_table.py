#-------------------------------------------------------
# [program, library, or function name]
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-4
#-------------------------------------------------------
# prints a table with lumber info
#-------------------------------------------------------

import lumber_functions


#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

base = 0.0
height = 0.0

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

area = 0.0
inertia = 0.0
modulus = 0.0

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------


#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------

print "Lumber  Cross-Sectional Moment of Section"
print "Size    Area            Inertia   Modulus"
for base in range(2,11,2):
    for height in range(2,13,2):
        area = lumber_functions.cross_section_area(base,height)
        inertia = lumber_functions.moment_inertia(base,height)
        modulus = lumber_functions.section_modulus(base,height)
        
        print "%2d x %2d %2.2f\t\t%7.2f%10.2f"%(base,height,area,inertia,modulus)

