#-------------------------------------------------------
# [program, library, or function name]
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-4
#-------------------------------------------------------
# draws a diamond of defined width
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    width: the width of the diamond
# Preconditions:
#    width: integer > 0
# Postconditions:
#    none
#-------------------------------------------------------

def diamond(width):
#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    star_line = ""
    
#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    star_num = 0
    EVEN = 2
    # for all numbers
    #(lines*2)+(width%EVEN) = # stars up to half


#-------------------------------------------------------
# draw the star
#-------------------------------------------------------

    for lines in range(0,width):#each line
        #reset the line
        star_line = ""
        
        #determine the # of stars / line
        #was pretty until I had to account for even stars
        if lines <= abs(width/2): #/2 for half
            star_num = (lines*2)+(width%EVEN)
        else:
            if(width%EVEN)==0:
                star_num = ((width -(lines +1) )*2)+2
            else:
                star_num = ((width -(lines +1) )*2)+1
        #print the stars
        for star in range(0,star_num):
            star_line+="*"    
               
        
        print star_line.center(width)
   
