#-------------------------------------------------------
# [program, library, or function name]
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-4
#-------------------------------------------------------
# gets the cross section area
#-------------------------------------------------------



#-------------------------------------------------------
# Parameters:
#    base,height
# Preconditions:
#    base,height: floats > 0
# Postconditions:
#    area: contains cross section area
#-------------------------------------------------------
def cross_section_area(base,height):
#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    area = 0.0

#-------------------------------------------------------
# gets the cross section area
#-------------------------------------------------------
    base = float(base)
    height = float(height)
    area = base*height
    return area

#-------------------------------------------------------
# Parameters:
#    base,height
# Preconditions:
#    base,height: floats > 0
# Postconditions:
#    inertia: contains cross section area
#-------------------------------------------------------

def moment_inertia(base,height):
#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    inertia = 0.0

#-------------------------------------------------------
# gets the inertia
#-------------------------------------------------------
    base = float(base)
    height = float(height)
    inertia = (base*(height*height*height))/12
    return inertia


#-------------------------------------------------------
# Parameters:
#    base,height
# Preconditions:
#    base,height: floats > 0
# Postconditions:
#    modulus: contains section modulus
#-------------------------------------------------------
def section_modulus(base,height):
#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    modulus = 0.0

#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------
    base = float(base)
    height = float(height)
    return (base*(height*height))/6


