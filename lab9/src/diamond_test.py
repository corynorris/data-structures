#-------------------------------------------------------
# [program, library, or function name]
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# test 1 / n function
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------

import diamond
#-------------------------------------------------------
# Input variables
#-------------------------------------------------------
n = 0


#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------

n = int(raw_input("Enter max width: ").strip())
diamond.diamond(n)
    
print "Done"