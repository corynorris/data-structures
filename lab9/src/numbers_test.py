#-------------------------------------------------------
# numbers_test
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-20
#-------------------------------------------------------
# tests the numbers function
#-------------------------------------------------------
import numbers


#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

average = 0.0
smallest = 0
largest = 0

#-------------------------------------------------------
# Test the function
#-------------------------------------------------------

average, smallest, largest =numbers.numbers()
print "average: %.2f smallest: %d largest: %d" %(average, smallest, largest) 