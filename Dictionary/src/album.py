#-------------------------------------------------------
# Album Data Functions
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-16
#-------------------------------------------------------
# Album dictionary functions.
#-------------------------------------------------------
import copy

#-------------------------------------------------------
# Constant Definitions
#    Dictionary field names.
#-------------------------------------------------------
TITLE = 'title'
ARTIST = 'artist'
RELEASED = 'released'
LESSER = - 1
EQUALS = 0
GREATER = 1

#-------------------------------------------------------
# Returns a dictionary of album data given parameters.
#-------------------------------------------------------
# Parameters:
#    title - album title
#    artist - band, performer, or owner of album
#    released - year of album release
# Preconditions:
#    title - string
#    artist - string
#    released - string
# Postconditions:
#    Returns a dictionary containing album data.
#-------------------------------------------------------
def set( title, artist, released ):
    
#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    album = {}  # The initialized dictionary for the album data.

    album[TITLE] = title
    album[ARTIST] = artist
    album[RELEASED] = released
    return album

#-------------------------------------------------------
# Returns a dictionary of album data by requesting data
# from a user.
#-------------------------------------------------------
# Parameters:
#    None.
# Preconditions:
#    None.
# Postconditions:
#    Returns a dictionary containing album data.
#-------------------------------------------------------
def get():

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    album = {}  # The initialized dictionary for the album data.

    title = raw_input( "Enter a album title: " ).strip()
    artist = raw_input( "Enter its artist: " ).strip()
    released = raw_input( "Enter its year of release: " ).strip()
    album = set( title, artist, released )
    return album

#-------------------------------------------------------
# Performs a formatted print of album data.
#-------------------------------------------------------
# Parameters:
#    album - a dictionary of album data.
# Preconditions:
#    album - a valid dictionary
# Postconditions:
#    Prints the formatted contents of album.
#-------------------------------------------------------
def print_( album ):
    print "Title:    %s" % ( album[TITLE] )
    print "Artist:   %s" % ( album[ARTIST] )
    print "Released: %s" % ( album[RELEASED] )
    return

#-------------------------------------------------------
# Reads a single line of album data from an open file handle.
#-------------------------------------------------------
# Parameters:
#    handle - link to a file of album data.
# Preconditions:
#    handle - a valid file handle.
# Postconditions:
#    If handle contains another line of data that data is
#    returned as an album dictionary, otherwise returns None.
#-------------------------------------------------------
def read( handle ):

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    line = ""  # The next string available in handle.
    title = ""  # Temporary data variables.
    artist = ""
    released = ""
    
#-------------------------------------------------------
# Main code.
#-------------------------------------------------------
    line = handle.readline().strip()  
    
    if line != "":
        title, artist, released = line.split( '|' )
        album = set( title, artist, released ) 
    else:
        album = None
    return album

#-------------------------------------------------------
# Read a set of album dictionaries from a file.
#-------------------------------------------------------
# Parameters:
#    filename - name of the album data file to rread.
# Preconditions:
#    filename - string, valid filename
# Postconditions:
#    Returns the contents of filename as a list of album data.
#-------------------------------------------------------
def read_file( filename ):

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    albums = []  # A list of album data.
    handle = ""  # The open file handle.
    album = {}   # A temporary album dictionary.
    
#-------------------------------------------------------
# Main code.
#-------------------------------------------------------
    handle = open( filename, "r" )
    album = read( handle )
        
    while album != None:
        albums += [ album ]
        album = read( handle )
            
    handle.close()
    return albums

#-------------------------------------------------------
# Writes a single line of album data to an open file handle.
#-------------------------------------------------------
# Parameters:
#    handle - link to a file of album data.
#    album - a dictionary of album data.
# Preconditions:
#    handle - a valid file handle.
#    album - a valid dictionary.
# Postconditions:
#    The contents of album are written as a string in the format
#    title|artist|released to handle.
#-------------------------------------------------------
def write( handle, album ):

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    line = ""  # The formatted line to write to the file.
    
#-------------------------------------------------------
# Main code.
#-------------------------------------------------------
    line = "%s|%s|%s" % ( album[TITLE], album[ARTIST], album[RELEASED] )
    print >> handle, line
    return

#-------------------------------------------------------
# Write a list of album dictionaries to a file.
#-------------------------------------------------------
# Parameters:
#    filename - name of the album data file to write.
#    albums - the list of album data to write.
#    new - whether the data should be appended to an existing file or not.
# Preconditions:
#    filename - string containing a valid file name.
#    albums - list containing valid album dictionaries.
#    new - boolean.
# Postconditions:
#    filename contains the contents of albums.
#-------------------------------------------------------
def write_file( albumFile, albums, new ):

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    album = {}     # Temporary variable for album data.
    handle = None  # The data file handle.
    
#-------------------------------------------------------
# Main code.
#-------------------------------------------------------
    if new:
        # Open a new file.
        handle = open( albumFile, "wb" )
    else:
        # Append to an existing file.
        handle = open( albumFile, "ab" )
    
    # Write the data to the file.
    for album in albums:
        write( handle, album )
        
    # Close the file.
    handle.close()
    return   

#---------------------------------------------------------
# Compares two album dictionaries first alphabetically by title,
# then by artist, then by date. Returns -1 if the first album 
# comes before the second, 1 if the first album comes after 
# the second, and 0 if the two albums are identical.
#---------------------------------------------------------
# Parameters:
#    album1 - the first ('left') album dictionary to compare
#    album2 - the second ('right') album dictionary to compare
# Preconditions:
#    album1, album2 - valid album dictionaries
# Postconditions:
#    Returns -1 if album 1 < album 2, 0 if album1 == album2,
#    1 if album1 > album2
#-------------------------------------------------------
def compare( album1, album2 ):
    # Compare the titles.
    if album1[TITLE] < album2[TITLE]:
        result = LESSER
    elif album1[TITLE] > album2[TITLE]:
        result = GREATER
    else:        
        # Compare the artists.
        if album1[ARTIST] < album2[ARTIST]:
            result = LESSER
        elif album1[ARTIST] > album2[ARTIST]:
            result = GREATER
        else:
            # Compare the release dates.
            if album1[RELEASED] < album2[RELEASED]:
                result = LESSER
            elif album1[RELEASED] > album2[RELEASED]:
                result = GREATER
            else:
                result = EQUALS
    return result

#---------------------------------------------------------
# Returns a list of albums released in a given year. 
#---------------------------------------------------------
# Parameters:
#    albums - the list of album data to search.
#    year   - the year of release to search for.
# Preconditions:
#    albums - list containing valid album dictionaries.
#    year   - string.
# Postconditions:
#    Returns a list of copies of albums where the year
#    of release == year.
#-------------------------------------------------------
def albums_by_year( albums, year ):

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    result = []  # List of selected albums.
    
    for a in albums:
        if a[RELEASED] == year:
            result.append( copy.deepcopy( a ) )        
    return result

#---------------------------------------------------------
# Returns the first album in a list of albums.
#---------------------------------------------------------
# Parameters:
#    albums - the list of album data to search.
# Preconditions:
#    albums - list containing valid album dictionaries.
# Postconditions:
#    Returns a copy of the first album in a list of albums
#    as defined by the album compare function. Returns None 
#    if albums is empty.
#-------------------------------------------------------
def find_first_album( albums ):

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    first = None  # Contains the first album in albums.
    
    if len( albums ) > 0:
        first = albums[0]  # Set to first album in albums.
        
        # Compare against the rest of albums.
        for a in albums[1:]:
            if compare( a, first ) == LESSER:
                first = a
        first = copy.deepcopy( first )  
    return first
