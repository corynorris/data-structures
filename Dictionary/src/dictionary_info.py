#-------------------------------------------------------
# Parameters:
#   dict, print_func
# Preconditions:
#    dict: dictionary
#    print_func: a printing function
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------      
       
def dictionary_info(dict, print_func):
    print "Length: %d"%(len(dict))
    print "Keys:   %s"%(dict.keys())
    print "Values: %s"%(dict.values())
    print "\nPrint Function:\n"
    print_func(dict)
    
    
     