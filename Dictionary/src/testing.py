#-------------------------------------------------------
# testing
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# test our album class
#-------------------------------------------------------


import album
import dictionary_info

#other
list = []

#test all the functions
new_album = album.get()
print"\n"
album.print_(new_album)
print"\n"
dictionary_info.dictionary_info(new_album, album.print_)
print"\n"
list = album.read_file('albums.txt')
for items in list:
    album.print_(items)
