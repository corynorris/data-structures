from morse import *
from bst_linked import *
from avl_linked import *
import random
cmp_count = 0

def avl_sort(array, comparison):
    avl = avl_init(None, comparison)
        
    for x in array:
        avl_insert(avl, x)
    
    array = avl_traverse(avl)
    return
  

def bst_sort(array, comparison):
    bst = bst_init(None, comparison)
        
    for x in array:
        bst_insert(bst, x)

    array = bst_traverse(bst)
    return

def cmp(a,b):
    global cmp_count
    cmp_count += 1
    
    value = 0
    if a > b:
        value = -1
    if b > a:
        value = 1
    return value

print 'creating arrays'
ordered_array = []
rev_array = []
rand_array = []
#set up the 3 arrays to test
for count in range(100):
    ordered_array += [count]
ordered_array1 = copy.deepcopy(ordered_array)
for count in range(100):
    rev_array += [(1001-count)]
rev_array1 = copy.deepcopy(rev_array)
for count in range(100):
    rand_array+=[random.randint(1,1000)]
rand_array1 = copy.deepcopy(rand_array)
   
#test all 3 arrays through both functions
#global


print 'testing bst arrays'
bst_sort(ordered_array,cmp)
bst_ordered = cmp_count
cmp_count = 0

bst_sort(rev_array,cmp)
bst_rev = cmp_count
cmp_count = 0

bst_sort(rand_array,cmp)
bst_rand = cmp_count
cmp_count = 0


print 'creating arrays'
ordered_array = []
rev_array = []
rand_array = []
#set up the 3 arrays to test


print 'testing avl arrays'


avl_sort(ordered_array1,cmp)
avl_ordered = cmp_count
cmp_count = 0

avl_sort(rev_array1,cmp)
avl_rev = cmp_count
cmp_count = 0

avl_sort(rand_array1,cmp)
avl_rand = cmp_count
cmp_count = 0


print '\n                     Comparisons'
print 'Algorithm    In Order  Reversed   Random'
print '---------    --------  --------   ------'
print 'BST Sort         %4d      %4d     %4d '%(bst_ordered,bst_rev,bst_rand)
print 'AVL Sort         %4d      %4d     %4d '%(avl_ordered,avl_rev,avl_rand)