#-------------------------------------------------------
# avl_linked.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Linked version of the AVL Tree ADT.
#-------------------------------------------------------
# Constant Definitions
#    Comparison value names.
#-------------------------------------------------------
LESSER = - 1
EQUALS = 0
GREATER = 1
#global variable



import copy

#---------------------------------------------------------
# Returns an empty AVL.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
#    cmp_func   - data comparison function
# Preconditions:
#    print_func - valid function reference
#    cmp_func   - valid function reference
# Postconditions:
#    Returns an empty AVL.
#-------------------------------------------------------
def avl_init( print_func, cmp_func ):
    avl = { 'root':None, 'print_func':print_func, 'cmp_func':cmp_func }
    return avl

#---------------------------------------------------------
# Determines if avl is empty.
#-------------------------------------------------------
# Parameters:
#    avl - an AVL.
# Preconditions:
#    avl - valid linked AVL.
# Postconditions:
#    Returns True if avl is empty, False otherwise.
#-------------------------------------------------------
def avl_empty( avl ):
    return avl['root'] == None

#---------------------------------------------------------
# Inserts a value into an AVL.
#-------------------------------------------------------
# Parameters:
#    avl - an AVL.
#    value - a data element
# Preconditions:
#    avl - valid linked AVL.
#    value - none
# Postconditions:
#    value is added to avl - only one copy of value is allowed in avl.
#-------------------------------------------------------
def avl_insert( avl, value ):
    
    avl['root'], heavier = avl_insert_aux( avl['root'], value, avl['cmp_func'] )
    return

# Adds 'data' to a AVL. Allows only one copy of 'value'
# in the tree. Returns the tree root.
def avl_insert_aux( node, value, cmp_func ):
    
    if node == None:
        # Base case: node is empty, so construct a new node with 'value'.
        node = avl_create_node( value )
        heavier = True
    else:
        # General case: move to the left or right subtree
        # depending on the value of 'data'.
        result = cmp_func( value, node['data'] )
        # Search the left subtree.
        if result == LESSER:
            node['left'], heavier = avl_insert_aux( node['left'], value, cmp_func )
        
            if heavier:
                
                if node['balance'] < 0:
                    # Subtree is left heavy
                    node, heavier = avl_left_rebalance( node )
                elif node['balance'] > 0:
                    # Subtree is right heavy
                    node['balance'] = 0
                    heavier = False
                else:
                    # Subtree is balanced
                    node['balance'] = - 1
                    heavier = True
        # Search the right subtree.
        else:
            node['right'], heavier = avl_insert_aux( node['right'], value, cmp_func )
            
            if heavier:
                if node['balance'] < 0:
                    # Subtree is left heavy
                    node['balance'] = 0
                    heavier = False
                elif node['balance'] > 0:
                    # Subtree is right heavy
                    node, heavier = avl_right_rebalance( node )
                else:
                    # Subtree is balanced
                    node['balance'] = 1
                    heavier = True            
                
        # Multiple copies of 'data' are not allowed in the tree - do nothing.
    return node, heavier
    
# 'balance' is the balance factor:
# -1: left heavy, 0: balanced, 1: right heavy
def avl_create_node( value ):
    node = { 'left':None, 'data':value, 'right':None, 'balance':0 }
    return node

#---------------------------------------------------------
# Finds a value in an AVL.
#-------------------------------------------------------
# Parameters:
#    avl - an AVL.
#    key - a comparable part of a data element.
# Preconditions:
#    avl - valid linked AVL.
#    key - none.
# Postconditions:
#    If exists, returns value of node containing key,
#    otherwise returns None.
#    Uses a recursive algorithm.
#-------------------------------------------------------
def avl_retrieve( avl, key ):
    # Find the node containing the key value.
    return avl_retrieve_aux( avl['root'], key, avl['cmp_func'] )

# Attempts to find a key value in a AVL. Returns the full
# node value if found, otherwise returns None.
def avl_retrieve_aux( node, key, cmp_func ):
    
    if node == None:        
        # General case: search the left or right subtrees for the key value.
        value = None
    else:
        result = cmp_func( key, node['data'] )

        if result == LESSER:
            # Search the left subtree.
            value = avl_retrieve_aux( node['left'], key, cmp_func )
        elif result == GREATER:
            # Search the right subtree.
            value = avl_retrieve_aux( node['right'], key, cmp_func )
        else:
            # Value has been found.
            value = copy.deepcopy( node['data'] )
    return value

#---------------------------------------------------------
# Removes value from avl.
#-------------------------------------------------------
# Parameters:
#    avl - an AVL.
#    key - a comparable part of a data element.
# Preconditions:
#    avl - valid linked AVL.
#    key - none.
# Postconditions:
#    If exists, returns value of node containing key and deletes node,
#    otherwise returns None.
#-------------------------------------------------------
def avl_remove( avl, key ):
    avl['root'], value, lighter = avl_delete_node( avl['root'], key, avl['cmp_func'] )
    return value

def avl_delete_node( node, key, cmp_func ):
    if node == None:
        value = None
        lighter = False
    else:
        result = cmp_func( key, node['data'] )
        
        if result == LESSER:
            node['left'], value, lighter = avl_delete_node( node['left'], key, cmp_func )
            
            if lighter:
                node, lighter = avl_rebalance_to_left( node )
            
        elif result == GREATER:
            node['right'], value, lighter = avl_delete_node( node['right'], key, cmp_func )
            
            if lighter:
                node, lighter = avlRebalanceToRight( node )
            
        else:
            # Value has been found.
            value = node['data']
            node, lighter = avl_delete_root( node )           
    
    return node, value, lighter

def avl_delete_root( node ):
    if node['left'] == None and node['right'] == None:
        node = None
        lighter = True
    else:
        if node['balance'] < 0:
            # node is left heavy
            node['left'], node['data'], lighter = avl_delete_largest( node['left'] )
            
            if lighter:
                node['balance'] = 0
        elif node['balance'] > 0:
            # node is right heavy
            node['right'], node['data'], lighter = avl_delete_smallest( node['right'] )
            
            if lighter:
                node['balance'] = 0
        else:
            # node is balanced
            node['right'], node['data'], lighter = avl_delete_smallest( node['right'] )
            
            if lighter:
                node['balance'] = - 1
            lighter = False

    return node, lighter

def avl_delete_smallest( node ):
    if node['left'] == None:
        data = node['data']
        lighter = True
        
        if node['right'] == None:
            node = None
        else:
            node = node['right']
    else:
        node['left'], data, lighter = avl_delete_smallest( node['left'] )
        
        if lighter:
            node, lighter = avl_rebalance_to_left( node )
        
    return node, data, lighter

def avl_delete_largest( node ):
    if node['right'] == None:
        data = node['data']
        lighter = True
        
        if node['left'] == None:
            node = None
        else:
            node = node['left']
    else:
        node['right'], data, lighter = avl_delete_largest( node['right'] )
        
        if lighter:
            node, lighter = avl_rebalance_to_right( node )
        
    return node, data, lighter

#----------------------------------------------------------
# Performs a left rotation on a node. 'node' is a non-empty
# tree having a non-empty right subtree.
# Called by 'node = avl_rotate_left( node )'
def avl_rotate_left( node ):
    temp = node['right']
    node['right'] = temp['left']
    temp['left'] = node
    return temp

#----------------------------------------------------------
# Performs a right rotation on a node. 'node' is a non-empty
# tree having a non-empty left subtree.
# Called by 'node = avl_rotate_right( node )'
def avl_rotate_right( node ):
    temp = node['left']
    node['left'] = temp['right']
    temp['right'] = node
    return temp

#----------------------------------------------------------
def avl_right_rebalance( t ):
    n = t['right']
    
    if n['balance'] < 0:
        # Tree is left heavy
        p = n['left']
        
        if p['balance'] < 0:
            # Subtree is left heavy
            t['balance'] = 0
            n['balance'] = 1
        elif p['balance'] > 0:
            # Subtree is right heavy
            t['balance'] = - 1
            n['balance'] = 0
        else:
            # Subtree is balanced
            t['balance'] = 0
            n['balance'] = 0
        
        p['balance'] = 0
        n = avl_rotate_right( n )
        t['right'] = n
        t = avl_rotate_left( t )
        heavier = False
    elif n['balance'] > 0:
        t['balance'] = 0
        n['balance'] = 0
        t = avl_rotate_left( t )
        heavier = False

    return t, heavier

#----------------------------------------------------------
def avl_left_rebalance( t ):
    n = t['left']
    
    if n['balance'] < 0:
        # Tree is left heavy
        t['balance'] = 0
        n['balance'] = 0
        t = avl_rotate_right( t )
        heavier = False
    elif n['balance'] > 0:
        # Tree is right heavy
        p = n['right']
        
        if p['balance'] < 0:
            # Subtree is left heavy
            t['balance'] = 1
            n['balance'] = 0
        elif p['balance'] > 0:
            # Subtree is right heavy
            t['balance'] = 0
            n['balance'] = - 1
        else:
            # Subtree is balanced
            t['balance'] = 0
            n['balance'] = 0
        
        p['balance'] = 0
        n = avl_rotate_left( n )
        t['left'] = n
        t = avl_rotate_right( t )
        heavier = False
    return t, heavier
 
#----------------------------------------------------------
# Rebalance after 't's right subtree became lighter.
def avl_rebalance_to_right( t ):
    if t['balance'] < 0:
        # Tree is left heavy
        n = t['left']
        
        if n['balance'] < 0:
            # Subtree is left heavy
            t['balance'] = 0
            n['balance'] = 0
            t = avl_rotate_right( t )
            lighter = True
        elif n['balance'] > 0:
            # Subtree is right heavy
            p = n['right']
            
            if p['balance'] < 0:
                # Subtree is left heavy
                t['balance'] = 1
                n['balance'] = 0
            elif p['balance'] > 0:
                # Subtree is right heavy
                t['balance'] = 0
                n['balance'] = - 1
            else:
                # Subtree is balanced
                t['balance'] = 0
                n['balance'] = 0
                
            n = avl_rotate_left( n )
            t['left'] = n
            t = avl_rotate_right( t )
            lighter = True
            
        else:
            # Subtree is balanced
            t['balance'] = - 1
            n['balance'] = 1
            t = avl_rotate_right( t )
            lighter = False
            
    elif t['balance'] > 0:
        # Tree is right heavy
        t['balance'] = 0
        lighter = True
    else:
        # Tree is balanced
        t['balance'] = - 1
        lighter = False
    
    return t, lighter        
            
#----------------------------------------------------------
# Rebalance after 't's left subtree became lighter.
def avl_rebalance_to_left( t ):
    if t['balance'] < 0:
        # Tree is left heavy
        t['balance'] = 0
        lighter = True
    elif t['balance'] > 0:
        # Tree is right heavy
        n = t['right']

        if n['balance'] < 0:
            # Subtree is left heavy
            p = n['left']
            
            if p['balance'] < 0:
                # Subtree is left heavy
                t['balance'] = 0
                n['balance'] = 1
            elif p['balance'] > 0:
                # Subtree is right heavy
                t['balance'] = - 1
                n['balance'] = 0
            else:
                # Subtree is balanced
                t['balance'] = 0
                n['balance'] = 0
                
            n = avl_rotate_right( n )
            t['right'] = n
            t = avl_rotate_left( t )
            lighter = True

        elif n['balance'] > 0:
            # Subtree is right heavy
            t['balance'] = 0
            n['balance'] = 0
            t = avl_rotate_left( t )
            lighter = True            
        else:
            # Subtree is balanced
            t['balance'] = 1
            n['balance'] = - 1
            t = avl_rotate_left( t )
            lighter = False
            
    else:
        # Tree is balanced
        t['balance'] = 1
        lighter = False
    
    return t, lighter

#---------------------------------------------------------
# Prints the contents of an AVL using the custom
# printing function.
#-------------------------------------------------------
# Parameters:
#    avl - an AVL.
#    order - the order to print avl contents in.
# Preconditions:
#    avl - valid linked AVL.
#    avl - string 'r', 'i', 'o', or 'l'
# Postconditions:
#    Prints the contents of avl in the given order.
#-------------------------------------------------------
def avl_print( avl, order ):
    if order == 'r':
        avl_preorder( avl['root'], avl['print_func'] )
    elif order == 'i':
        avl_inorder( avl['root'], avl['print_func'] )
    elif order == 'o':
        avl_postorder( avl['root'], avl['print_func'] )
    elif order == 'l':
        avl_levelorder( avl['root'], avl['print_func'] )
    else:
        print "Invalid order value: %s" % ( str( order ) )
    return
    
# Traverse the AVL in preorder, applying the function 'print_func'
# to each node.
def avl_preorder( node, print_func ) :
    if( node != None ) :
        print_func( node['data'] )
        avl_preorder( node['left'], print_func )
        avl_preorder( node['right'], print_func )
    return

# Traverse the AVL in inorder, applying the function 'print_func'
# to each node.
def avl_inorder( node, print_func ) :
    if( node != None ) :
        avl_inorder( node['left'], print_func )
        print_func( node['data'] )
        avl_inorder( node['right'], print_func )
    return

# Traverse the AVL in postorder, applying the function 'print_func'
# to each node.
def avl_postorder( node, print_func ) :
    if( node != None ) :
        avl_postorder( node['left'], print_func )
        avl_postorder( node['right'], print_func )
        print_func( node['data'] )
    return

# Traverse the AVL in levelorder, applying the function 'print_func'
# to each node.
def avl_levelorder( node, print_func ):

    # your code here

    return

#---------------------------------------------------------
# Returns the contents of avl in an array in sorted order.
#-------------------------------------------------------
# Parameters:
#    avl - an AVL
# Preconditions:
#    avl - valid linked AVL dictionary
# Postconditions:
#    Returns the contents of avl in an array in sorted order.
#-------------------------------------------------------
def avl_traverse( avl ):
    array = []
    avl_traverse_aux(avl['root'],avl['print_func'], array)
    return array
    # your code here
    

def avl_traverse_aux( node, print_func,array ) :
    
    if( node != None ) :
        avl_traverse_aux( node['left'], print_func ,array)
        array.append( node['data'] )        
        avl_traverse_aux( node['right'], print_func ,array )
    return
    
    
#---------------------------------------------------------
# Utility function for retrieving the data comparison function.
#-------------------------------------------------------
# Parameters:
#    avl - an AVL
# Preconditions:
#    avl - valid linked AVL dictionary
# Postconditions:
#    Returns avl's print function.
#-------------------------------------------------------
def avl_get_cmp_func( avl ):
    return avl['cmp_func']

#---------------------------------------------------------
# Utility function for changing the data comparison function.
#-------------------------------------------------------
# Parameters:
#    avl - an AVL
#    cmp_func       - data comparison function
# Preconditions:
#    avl - valid linked AVL dictionary
#    cmp_func       - valid function reference
# Postconditions:
#    Changes avl's comparison function to cmp_func.
#-------------------------------------------------------
def avl_set_cmp_func( avl, cmp_func ):
    avl['print_func'] = print_func
    return

#---------------------------------------------------------
# Utility function for changing the data print function.
#-------------------------------------------------------
# Parameters:
#    avl - an AVL
#    print_func     - data print function
# Preconditions:
#    avl - valid linked AVL dictionary
#    print_func     - valid function reference
# Postconditions:
#    Changes avl's print function to print_func.
#-------------------------------------------------------
def avl_set_print_func( avl, print_func ):
    avl['print_func'] = print_func
    return

#---------------------------------------------------------
# Utility function for retrieving the data print function.
#-------------------------------------------------------
# Parameters:
#    avl - an AVL
# Preconditions:
#    avl - valid linked AVL dictionary
# Postconditions:
#    Returns avl's print function.
#-------------------------------------------------------
def avl_get_print_func( avl ):
    return avl['print_func']
