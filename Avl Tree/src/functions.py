def avl_traverse (avl):
    print 'in avl_traverse'
    print avl['root']
    array = avl_traverse_aux(avl['root'],avl['print_func'])
    return array

def avl_traverse_aux( node, print_func ) :
    
    if( node != None ) :
        print node['data']
        array.append( node['data'] )
        avl_preorder( node['left'], print_func )
        avl_preorder( node['right'], print_func )
    return array