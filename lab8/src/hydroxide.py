#-------------------------------------------------------
# hydroxide
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-10-28
#-------------------------------------------------------
# determines if a chemical is that of a hyrdoxide
#-------------------------------------------------------



#-------------------------------------------------------
# Parameters:
#    formula
# Preconditions:
#    formula : string containing a chemical formula.
# Postconditions:
#    is_hydroxide: True if the formula is a hyrdoxide
#-------------------------------------------------------

def hydroxide(formula):

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------

    is_hydroxide = False

#-------------------------------------------------------
# other variables
#-------------------------------------------------------


#-------------------------------------------------------
# check if the last 2 characters are o and h respectively
#-------------------------------------------------------

    formula = formula[(len(formula)-2):len(formula)]
 
    if (formula == "oh"):
        is_hydroxide = True
        
    return is_hydroxide

