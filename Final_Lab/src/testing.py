#-------------------------------------------------------
# testing
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# test the other functions
#-------------------------------------------------------
import task1
import task2
import task3
import task4
import task5
import task6

#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------



#-------------------------------------------------------
# Input variables
#-------------------------------------------------------
base_value = 0
exponent = 0
string = ""

x = 0
y = 0

a = [4, 5, 3, 4, 5, 2, 2, 4]
#-------------------------------------------------------
# Output variables
#-------------------------------------------------------


#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------



#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------

print "TESTING 1"
base_value = float(raw_input("Enter a base value (may have a decimal): ").strip())
exponent = int(raw_input("Enter a power (must be integer): ").strip())
print "Result"
print task1.to_power(base_value, exponent)

print "TESTING 2"
string = raw_input("Enter a string: ").strip()
print "# vowels"
print task2.vowel_count(string)

print "TESTING 3"
string = raw_input("Enter a string: ").strip()
print "digits"
print task3.find_digits(string)

print "TESTING 4"
string = raw_input("Enter a path: ").strip()
print "path sections"
print task4.split_path(string)

print "TESTING 5"
x = float(raw_input("Enter an x: ").strip())
y = float(raw_input("Enter an y: ").strip())
print "Result"
print task5.f(x, y)


print "Old list: [4, 5, 3, 4, 5, 2, 2, 4]"
print "New List:",
print task6.bag_to_set(a)











