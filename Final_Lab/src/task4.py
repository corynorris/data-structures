#-------------------------------------------------------
# split_path
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# spits up a path into parts at teh backslash
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    path
# Preconditions:
#    is a path
# Postconditions:
#    path_elements: returns the elements in a tuple
#-------------------------------------------------------
def split_path(path):


#-------------------------------------------------------
# Input variables
#-------------------------------------------------------



#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    path_elements= ()

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------



#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------

    if len(path) == 0:
        path_elements= (),
    elif path.find('/') == -1:
        path_elements+= path,       
    else:
        
        #if the last char is a digit
#n = path.find('/');
        path_elements += path[0:path.find('/')],       
        path_elements += split_path(path[path.find('/')+1:])
        

    return path_elements


