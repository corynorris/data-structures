#-------------------------------------------------------
# vowel_count
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-
#-------------------------------------------------------
# counts the values in a list
#-------------------------------------------------------


#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------

def vowel_count( s ):

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

    vowels = "aeiou"
    count = 0

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------


#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------



#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------


    
    if len(s) == 0:
        count = 0
    else:
        
        #if the last char is a vowel
     #   if s[len(s)-1] in vowels:
        if vowels.__contains__(s[len(s)-1]):
            count = 1
                       
        count += vowel_count(s[0:(len(s)-1)])
        


    return count


