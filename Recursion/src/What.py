def rev_list_bad( data ):
    n = len( data )
    print 'length: %d' %(n)
    for x in data:
        print x,
        
    print
        
    if n > 1:
       swap( data, 0, n-1 )
       rev_list_bad( data[1:n - 1] )
       #Here it doesnt work because although we ARE swapping the values
       # we do not return the values.  We would need to swap the refernces
       #To fix this, put the return before rev_list_bad(data[1:n - 1] ))
    return
     

def swap( data, i, j ):
    temp = data[i]
    data[i] = data[j]
    data[j] = temp
    return

list = [1,2,3,4,5]
rev_list_bad(list)

print "final:"
for x in list:
    print x,