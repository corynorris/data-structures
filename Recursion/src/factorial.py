#-------------------------------------------------------
# factorial
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2009-01-19
#-------------------------------------------------------
# tail recursive factorial program
#-------------------------------------------------------

#-------------------------------------------------------
# Parameters:
#    
# Preconditions:
#   
# Postconditions:
# 
#-------------------------------------------------------

def factorial ( n ):
   
   return aux_f(n,n-1)

def aux_f(t,n):
    
    if n == 0:
        return t
    else:    
        t = t*n
        return aux_f(t, n-1)

print factorial(4)
