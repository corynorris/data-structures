#-------------------------------------------------------
# count_ns
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410
# Email:   norr4410@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Functions for Stacks (cont'd) Lab.
#-------------------------------------------------------
def count_ns( value, list ):
    
    mid = len(list)/2
    count = 0
    if len( list ) == 0:
        count = 0
    else:
        if list[mid] == value:
            count += 1
        count = count + count_ns( value, list[mid+1:] ) + count_ns( value, list[:mid])         
    
       
        
    return count






list = [1,2,3,4,3,5,6,7,4]
print count_ns(4,list)
