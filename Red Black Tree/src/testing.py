from bst_linked import *
from avl_linked import *
from rbt_linked import *
import random
cmp_count = 0

def compare_( v1,v2):
    global cmp_count
    cmp_count += 1
    
    #unpack the tuples 
    value = EQUALS
    if v1 > v2:
        value = GREATER
    elif v1 < v2:
        value = LESSER     

    return value

def print_(x):
    print x
    

def bst_sort(array, comparison):
    bst = bst_init(print_,compare_)
        
    for x in array:
        bst_insert(bst, x)

    array = bst_traverse(bst)
    return
def avl_sort(array, comparison):
    avl = avl_init(print_,compare_)
        
    for x in array:
        avl_insert(avl, x)

    array = avl_traverse(avl)
    return
def rbt_sort(array, comparison):
    rbt = rbt_init(print_,compare_)
        
    for x in array:
        rbt_insert(rbt, x)

    array = rbt_traverse(rbt)
    return
  

   
#test all 3 arrays through both functions
#global
def sort_test(sort_func, cmp_func, array):
    global cmp_count
    #initialize the trees to test
    cmp_count = 0
    sort_func(copy.deepcopy(array),cmp_func)
    return 
    
    
    
ordered_array = []
rev_array = []

#set up the 3 arrays to test
for count in range(100):
    ordered_array += [count]
for count in range(100):
    rev_array += [(101-count)]

#test ordered and reversed 
sort_test(bst_sort,compare_,ordered_array)
bst_ordered = cmp_count
sort_test(bst_sort,compare_,rev_array)
bst_rev = cmp_count

sort_test(avl_sort,compare_,ordered_array)
avl_ordered = cmp_count
sort_test(avl_sort,compare_,rev_array)
avl_rev = cmp_count

sort_test(rbt_sort,compare_,ordered_array)
rbt_ordered = cmp_count
sort_test(rbt_sort,compare_,rev_array)
rbt_rev = cmp_count

bst_rand = 0
rbt_rand = 0
avl_rand = 0    
#for the random.    
for count in range(1000):
    cur_array = []
    for num in range(100):
        cur_array += [random.randint(0,1000)]
    
    sort_test(bst_sort,compare_, cur_array)
    bst_rand += cmp_count
    sort_test(rbt_sort,compare_, cur_array)
    rbt_rand += cmp_count
    sort_test(avl_sort,compare_, cur_array)
    avl_rand += cmp_count



print '\n                     Comparisons'
print 'Algorithm    In Order  Reversed   Random'
print '---------    --------  --------   ------'
print 'BST Sort         %4d      %4d     %4d '%(bst_ordered,bst_rev,(bst_rand/1000))
print 'AVL Sort         %4d      %4d     %4d '%(avl_ordered,avl_rev,(avl_rand/1000))
print 'BRT Sort         %4d      %4d     %4d '%(rbt_ordered,rbt_rev,(rbt_rand/1000))
