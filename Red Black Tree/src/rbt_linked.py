#-------------------------------------------------------
# rbt_linked.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-03-01
#-------------------------------------------------------
# Linked version of the Red-Black Tree ADT.
#-------------------------------------------------------
# Constant Definitions
#    Comparison value names.
#    Color names.
#-------------------------------------------------------
LESSER = - 1
EQUALS = 0
GREATER = 1

BLACK = 0
RED = 1

import copy

#---------------------------------------------------------
# Returns an empty RBT.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
#    cmp_func   - data comparison function
# Preconditions:
#    print_func - valid function reference
#    cmp_func   - valid function reference
# Postconditions:
#    Returns an empty RBT.
#-------------------------------------------------------
def rbt_init( print_func, cmp_func ):
    rbt = { 'print_func':print_func, 'cmp_func':cmp_func, 'root':None }
    return rbt

#---------------------------------------------------------
# Determines if rbt is empty.
#-------------------------------------------------------
# Parameters:
#    rbt - an RBT.
# Preconditions:
#    rbt - valid linked RBT.
# Postconditions:
#    Returns True if rbt is empty, False otherwise.
#-------------------------------------------------------
def rbt_empty( rbt ):
    return rbt['root'] == None

#---------------------------------------------------------
# Determines if a node is RED.
#-------------------------------------------------------
# Parameters:
#    node - an RBT node.
# Preconditions:
#    rbt - None.
# Postconditions:
#    Returns True if node is RED, False otherwise.
#-------------------------------------------------------
def rbt_is_red( node ):
    return node != None and node['color'] == RED

#---------------------------------------------------------
# Inserts a value into an RBT.
#-------------------------------------------------------
# Parameters:
#    rbt - an RBT.
#    value - a data element
# Preconditions:
#    rbt - valid linked RBT.
#    value - none
# Postconditions:
#    value is added to rbt - only one copy of value is allowed in rbt.
#-------------------------------------------------------
def rbt_insert( rbt, value ):
    cmp_func = rbt['cmp_func']
    rbt['root'] = rbt_insert_aux( rbt['root'], value, rbt['cmp_func'] )
    rbt['root']['color'] = BLACK
    return
    
# Adds 'data' to a RBT. Allows only one copy of 'value'
# in the tree. Returns the tree root.
def rbt_insert_aux( node, value, cmp_func ):
    
    if node == None:
        node = rbt_create_node( value )
    else:
        result = cmp_func( value, node['data'] )
        # Search the left subtree.        
        if result == LESSER:
            node['left'] = rbt_insert_aux( node['left'], value, cmp_func )
            
            if rbt_is_red( node['left'] ):
                if rbt_is_red( node['right'] ):
                    # Case 1
                    node['color'] = RED
                    node['left']['color'] = BLACK
                    node['right']['color'] = BLACK
                else:
                    # Cases 2 and 3
                    if rbt_is_red( node['left']['left'] ):
                        # Rebalance the tree.
                        node = rbt_rotate_right( node )
                    elif rbt_is_red( node['left']['right'] ):
                        # Rebalance the tree.
                        node = rbt_double_rotate_right( node )
        # Search the right subtree.
        elif result == GREATER:
            node['right'] = rbt_insert_aux( node['right'], value, cmp_func )
            
            if rbt_is_red( node['right'] ):
                if rbt_is_red( node['left'] ):
                    # Case 1
                    node['color'] = RED
                    node['right']['color'] = BLACK
                    node['left']['color'] = BLACK
                else:
                    # Cases 2 and 3
                    if rbt_is_red( node['right']['right'] ):
                        # Rebalance the tree.
                        node = rbt_rotate_left( node )
                    elif rbt_is_red( node['right']['left'] ):
                        # Rebalance the tree.
                        node = rbt_double_rotate_left( node )
    return node

# By default assumes node color is RED.
def rbt_create_node( value ):
    node = { 'left':None, 'data':copy.deepcopy( value ), 'right':None , 'color':RED }
    return node

#----------------------------------------------------------
# Performs a left rotation on a node. 'node' is a non-empty
# tree having a non-empty right subtree.
# Called by 'node = rbt_rotate_left( node )'
def rbt_rotate_left( node ):
    temp = node['right']
    node['right'] = temp['left']
    temp['left'] = node
    node['color'] = RED
    temp['color'] = BLACK
    return temp

#----------------------------------------------------------
# Performs a right rotation on a node. 'node' is a non-empty
# tree having a non-empty left subtree.
# Called by 'node = rbt_rotate_right( node )'
def rbt_rotate_right( node ):
    temp = node['left']
    node['left'] = temp['right']
    temp['right'] = node
    node['color'] = RED
    temp['color'] = BLACK
    return temp

#-------------------------------------------------------
# Performs a double left rotation on a node.
def rbt_double_rotate_left( node ):
    node['right'] = rbt_rotate_right( node['right'] )
    node = rbt_rotate_left( node )
    return node

#-------------------------------------------------------
# Performs a double right rotation on a node.
def rbt_double_rotate_right( node ):
    node['left'] = rbt_rotate_left( node['left'] )
    node = rbt_rotate_right( node )
    return node

#---------------------------------------------------------
# Finds a value in an RBT.
#-------------------------------------------------------
# Parameters:
#    rbt - an RBT.
#    key - a comparable part of a data element.
# Preconditions:
#    rbt - valid linked RBT.
#    key - none.
# Postconditions:
#    If exists, returns value of node containing key,
#    otherwise returns None.
#    Uses a recursive algorithm.
#-------------------------------------------------------
def rbt_retrieve( rbt, key ):
    current = rbt['root']
    
		# Find the location of the matching data - if any.
    while current != None:
        result = cmp_func( key, current['data'] )
        
        if result == LESSER:
            current = current['left']
        elif result == GREATER:
            current = current['right']
            
		# Has the data been found?
    if current != None:
        value = copy.deepcopy( current['data'] )
    else:
        value = None

    return value
        
#---------------------------------------------------------
# Prints the contents of an rbt using the custom
# printing function.
#-------------------------------------------------------
# Parameters:
#    rbt - an rbt.
#    order - the order to print rbt contents in.
# Preconditions:
#    rbt - valid linked rbt.
#    rbt - string 'r', 'i', 'o', or 'l'
# Postconditions:
#    Prints the contents of rbt in the given order.
#-------------------------------------------------------
def rbt_print( rbt, order ):
    if order == 'r':
        rbt_preorder( rbt['root'], rbt['print_func'] )
    elif order == 'i':
        rbt_inorder( rbt['root'], rbt['print_func'] )
    elif order == 'o':
        rbt_postorder( rbt['root'], rbt['print_func'] )
    elif order == 'l':
        rbt_levelorder( rbt['root'], rbt['print_func'] )
    else:
        print "Invalid order value: %s" % ( str( order ) )
    return
    
# Traverse the rbt in preorder, applying the function 'print_func'
# to each node.
def rbt_preorder( node, print_func ) :
    if( node != None ) :
        print_func( node['data'] )
        rbt_preorder( node['left'], print_func )
        rbt_preorder( node['right'], print_func )
    return

# Traverse the rbt in inorder, applying the function 'print_func'
# to each node.
def rbt_inorder( node, print_func ) :
    if( node != None ) :
        rbt_inorder( node['left'], print_func )
        print_func( node['data'] )
        rbt_inorder( node['right'], print_func )
    return

# Traverse the rbt in postorder, applying the function 'print_func'
# to each node.
def rbt_postorder( node, print_func ) :
    if( node != None ) :
        rbt_postorder( node['left'], print_func )
        rbt_postorder( node['right'], print_func )
        print_func( node['data'] )
    return

# Traverse the rbt in levelorder, applying the function 'print_func'
# to each node.
def rbt_levelorder( node, print_func ):
    if node != None:
        q = []
        q.append( node )
        count = 0
        mod = 1

        while len( q ) > 0:
            node = q.pop( 0 )
            print_func( node['data'] )
            count += 1
        
            if count % mod == 0:
                print
                mod = mod * 2 + 1

            if node['left'] != None:
                q.append( node['left'] )
            if node['right'] != None:
                q.append( node['right'] )
            
        print
    return

#---------------------------------------------------------
# Returns the contents of rbt in an array in sorted order.
#-------------------------------------------------------
# Parameters:
#    rbt - an RBT
# Preconditions:
#    rbt - valid linked RBT dictionary
# Postconditions:
#    Returns the contents of rbt in an array in sorted order.
#-------------------------------------------------------
def rbt_traverse( rbt ):
    a = []
    rbt_traverse_aux( rbt['root'], a )
    return a
    
# Traverse the RBT in inorder, adding the contents of each node
# to an array.
def rbt_traverse_aux( node, a ) :
    if( node != None ) :
        rbt_traverse_aux( node['left'], a )
        a.append( node['data'] )
        rbt_traverse_aux( node['right'], a )
    return

#---------------------------------------------------------
# Inserts a value into an RBT.
#-------------------------------------------------------
# Parameters:
#    rbt - an RBT.
#    value - a data element
# Preconditions:
#    rbt - valid linked RBT.
#    value - none
# Postconditions:
#    value is added to rbt - only one copy of value is allowed in rbt.
#-------------------------------------------------------
def rbt_add( rbt, value ):
    cmp_func = rbt['cmp_func']
    rbt['root'] = rbt_add_aux( rbt['root'], (value,1), rbt['cmp_func'] )
    rbt['root']['color'] = BLACK
    return
    
# Adds 'data' to a RBT. Allows only one copy of 'value'
# in the tree. Returns the tree root.
def rbt_add_aux( node, value, cmp_func ):
    
    if node == None:
        node = rbt_create_node( value )
    else:
        var1, count1 = node['data']
        var2, count2 = value
        result = cmp_func( var2, var1 )
        # Search the left subtree.        
        if result == LESSER:
            node['left'] = rbt_add_aux( node['left'], value, cmp_func )
            
            if rbt_is_red( node['left'] ):
                if rbt_is_red( node['right'] ):
                    # Case 1
                    node['color'] = RED
                    node['left']['color'] = BLACK
                    node['right']['color'] = BLACK
                else:
                    # Cases 2 and 3
                    if rbt_is_red( node['left']['left'] ):
                        # Rebalance the tree.
                        node = rbt_rotate_right( node )
                    elif rbt_is_red( node['left']['right'] ):
                        # Rebalance the tree.
                        node = rbt_double_rotate_right( node )
        # Search the right subtree.
        elif result == GREATER:
            node['right'] = rbt_add_aux( node['right'], value, cmp_func )
            
            if rbt_is_red( node['right'] ):
                if rbt_is_red( node['left'] ):
                    # Case 1
                    node['color'] = RED
                    node['right']['color'] = BLACK
                    node['left']['color'] = BLACK
                else:
                    # Cases 2 and 3
                    if rbt_is_red( node['right']['right'] ):
                        # Rebalance the tree.
                        node = rbt_rotate_left( node )
                    elif rbt_is_red( node['right']['left'] ):
                        # Rebalance the tree.
                        node = rbt_double_rotate_left( node )
        else:
            var, count = node['data']
            node['data'] = (var,count+1)
    return node


def rbt_add_traverse( bst ):
    a = []
    rbt_add_traverse_aux( bst['root'], a )
    return a
    
# Traverse the BST in inorder, adding the contents of each node
# to an array.
def rbt_add_traverse_aux( node, a ) :
    
    if( node != None ) :
        var, count = node['data']
        rbt_add_traverse_aux( node['left'], a )
        for i in range(count):
            a.append( var )
        rbt_add_traverse_aux( node['right'], a )
    return