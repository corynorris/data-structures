#-------------------------------------------------------
# bst_linked.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      

# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Linked version of the Binary Search Tree ADT.
#-------------------------------------------------------
# Constant Definitions
#    Comparison value names.
#-------------------------------------------------------
LESSER = - 1
EQUALS = 0
GREATER = 1

import copy

#---------------------------------------------------------
# Returns an empty BST.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
#    cmp_func   - data comparison function
# Preconditions:
#    print_func - valid function reference
#    cmp_func   - valid function reference
# Postconditions:
#    Returns an empty BST.
#-------------------------------------------------------
def bst_init( print_func, cmp_func ):
    bst = { 'root':None, 'print_func':print_func, 'cmp_func':cmp_func }
    return bst

#---------------------------------------------------------
# Determines if bst is empty.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
# Preconditions:
#    bst - valid linked BST.
# Postconditions:
#    Returns True if bst is empty, False otherwise.
#-------------------------------------------------------
def bst_empty( bst ):
    return bst['root'] == None

#---------------------------------------------------------
# Returns the maximum height of a BST.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
# Preconditions:
#    bst - valid linked BST.
# Postconditions:
#    Returns maximum height of bst.
#-------------------------------------------------------
def bst_height( bst ):
    return bst_height_aux( bst['root'] )

# Recurses using a bst node.
def bst_height_aux( node ):

    if node == None:
        # Base case: node is empty, so height is 0.
        height = 0
    else:
        # General case: determine whether the left or right subtree is deeper.
        height = 1 + max( bst_height_aux( node['left'] ), bst_height_aux( node['right'] ) )
    return height

#---------------------------------------------------------
# Inserts a value into a BST.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    value - a data element
# Preconditions:
#    bst - valid linked BST.
#    value - none
# Postconditions:
#    value is added to bst - only one copy of value is allowed in bst.
#-------------------------------------------------------
def bst_insert( bst, value ):
    bst['root'] = bst_insert_aux( bst['root'], value, bst['cmp_func'] )
    return

# Adds 'value' to bst. Allows only one copy of 'value'
# in the tree. Returns the tree root.
def bst_insert_aux( node, value, cmp_func ):
    
    if node is None:
        # Base case: node is empty, so construct a new node with 'data'.
        node = { 'left':None, 'data':copy.deepcopy( value ), 'right':None }
    else:
        # General case: move to the left or right subtree depending on the value of 'data'.
        result = cmp_func( value, node['data'] )

        if result == LESSER:
            # Search the left subtree.
            node['left'] = bst_insert_aux( node['left'], value, cmp_func )
        elif result == GREATER:
            # Search the right subtree.
            node['right'] = bst_insert_aux( node['right'], value, cmp_func )
        # Multiple copies of 'data' are not allowed in the tree - do nothing.
    return node
    
#---------------------------------------------------------
# Finds a value in a BST.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    key - a comparable part of a data element.
# Preconditions:
#    bst - valid linked BST.
#    key - none.
# Postconditions:
#    If exists, returns value of node containing key,
#    otherwise returns None.
#    Uses a recursive algorithm.
#-------------------------------------------------------
def bst_retrieve( bst, key ):
    # Find the node containing the key value.
    return bst_retrieve_aux( bst['root'], key, bst['cmp_func'] )

def bst_retrieve_aux( node, key, cmp_func ):

    if node is  None:
        # Base case: at bottom of tree and key not found.
        value = None
    else:
        # General case: search the left or right subtrees for the key value.
        result = cmp_func( key, node['data'] )

        if result == LESSER:
            # Search the left subtree.
            value = bst_retrieve_aux( node['left'], key, cmp_func )
        elif result == GREATER:
            # Search the right subtree.
            value = bst_retrieve_aux( node['right'], key, cmp_func )
        else:
            # Value has been found.
            value = copy.deepcopy( node['data'] )
    return value

#---------------------------------------------------------
# Finds a value in a BST.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    key - a comparable part of a data element.
# Preconditions:
#    bst - valid linked BST.
#    key - none.
# Postconditions:
#    If exists, returns value of node containing key,
#    otherwise returns None.
#    Uses an iterative algorithm.
#-------------------------------------------------------
def bst_retrieve( bst, key ):
    # Find the node containing the key value.
    node = bst['root']
    cmp_func = bst['cmp_func']
    value = None
    
    while node != None and value == None:
        result = cmp_func( key, node['data'] )
        
        if result == LESSER:
            node = node['left']
        elif result == GREATER:
            node = node['right']
        else:
            value = copy.deepcopy( node['data'] )
        
    return value

#---------------------------------------------------------
# Removes value from bst.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    key - a comparable part of a data element.
# Preconditions:
#    bst - valid linked BST.
#    key - none.
# Postconditions:
#    If exists, returns value of node containing key and deletes node,
#    otherwise returns None.
#-------------------------------------------------------
def bst_remove( bst, key ):
    bst['root'], value = bst_remove_aux( bst['root'], key, bst['cmp_func'] )
    return value

# Attempts to find a key value in a BST. Deletes the node
# if found and returns the tree root.
def bst_remove_aux( node, key, cmp_func ):

    if( node is None ):
        # Base Case: the key value is not in the tree.
        value = None
    else:
        # General case: search the left or right subtree for the key value.
        result = cmp_func( key, node['data'] )

        if result == LESSER:
            # Search the left subtree.
            node['left'], value = bst_remove_aux( node['left'], key, cmp_func )
        elif result == GREATER:
            # Search the right subtree.
            node['right'], value = bst_remove_aux( node['right'], key, cmp_func )
        else:
            # Value has been found.
            value = node['data']
            node = bst_delete_node( node )           
    return node, value

# Removes node from the tree and replaces it with the
# maximum node of its subtree.
def bst_delete_node( node ):
    
    if node['left'] is None:
        # node has no left children.
        node = node['right']
    elif node['right'] is None:
        # node has no right children.
        node = node['left']
    else:
        # node has two children. Get max (rightmost node) of left subtree.
        temp = node['left']

        while temp['right'] is not None:
            temp = temp['right']

        node['data'] = temp['data']
        bst_delete_node( temp )
    return node

#---------------------------------------------------------
# Counts the number of types of nodes in a bst.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
# Preconditions:
#    bst - valid linked BST.
# Postconditions:
#    Returns the number of nodes in a BST.
#    'zero', 'one', and 'two' contain the number of nodes
#    with that number of children in the tree.
#----------------------------------------------------------
def bst_node_counts( bst ):
    zero, one, two = bst_node_counts_aux( bst['root'], 0, 0, 0 )
    return zero, one, two

# Returns the number of nodes in a BST.
# 'zero', 'one', and 'two' contain the number of nodes
# with that number of children in the tree.
def bst_node_counts_aux( node, zero, one, two ):

    if node != None:
        
        if node['left'] is None and node['right'] is None:
            # Base case: node has no children.
            zero += 1
        elif node['left'] is not None and node['right'] is None:
            # General case: node has a left child.
            one += 1
            zero, one, two = bst_node_counts_aux( node['left'], zero, one, two )
        elif node['left'] is None and node['right'] is not None:
            # General case: node has a right child.
            one += 1
            zero, one, two = bst_node_counts_aux( node['right'], zero, one, two )
        else:
            # General case: node has two children.
            two += 1
            zero, one, two = bst_node_counts_aux( node['left'], zero, one, two )
            zero, one, two = bst_node_counts_aux( node['right'], zero, one, two )
            
    return zero, one, two

#---------------------------------------------------------
# Prints the contents of a BST using the custom
# printing function.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    order - the order to print bst contents in.
# Preconditions:
#    bst - valid linked BST.
#    bst - string 'r', 'i', 'o', or 'l'
# Postconditions:
#    Prints the contents of bst in the given order.
#-------------------------------------------------------
def bst_print( bst, order ):
    if order == 'r':
        bst_preorder_aux( bst['root'], bst['print_func'] )
    elif order == 'i':
        bst_inorder_aux( bst['root'], bst['print_func'] )
    elif order == 'o':
        bst_postorder_aux( bst['root'], bst['print_func'] )
    elif order == 'l':
        bst_levelorder_aux( bst['root'], bst['print_func'] )
    else:
        print "Invalid order value: %s" % ( str( order ) )
    return
    
# Traverses bst in inorder.
def bst_inorder_aux( node, print_func ) :
    
    if( node != None ) :
        bst_inorder_aux( node['left'], print_func )
        print_func( node['data'] )
        bst_inorder_aux( node['right'], print_func )
    return

# Traverses bst in postorder.
def bst_postorder_aux( node, print_func ) :
    
    if( node != None ) :
        bst_postorder_aux( node['left'], print_func )
        bst_postorder_aux( node['right'], print_func )
        print_func( node['data'] )
    return

# Traverses bst in preorder.
def bst_preorder_aux( node, print_func ) :
    
    if( node != None ) :
        print_func( node['data'] )
        bst_preorder_aux( node['left'], print_func )
        bst_preorder_aux( node['right'], print_func )
    return

# Traverses bst in preorder.
# This uses a Python list like a queue - adds to the end, removes
# from the front. It could also use a queue ADT instead.
def bst_preorder_aux( node, print_func ):
    s = []
    s.append( node )

    while len( s ) > 0:
        node = s.pop()
        
        if node != None:
            print_func( node['data'] )
            s.append( node['right'] )
            s.append( node['left'] )
    return

# Traverses bst in levelorder.
# This uses a Python list like a queue - adds to the end, removes
# from the front. It can also use a queue data structure such as
# 'queueList' or 'queueDictionary'.
def bst_levelorder_aux( node, print_func ):
    q = []
    q.append( node )

    while len( q ) > 0:
        node = q.pop( 0 )
        
        if node != None:
            print_func( node['data'] )
            q.append( node['left'] )
            q.append( node['right'] )
    return

# Traverse the BST in levelorder, applying the function 'print_func'
# to each node.
# This uses a Python list like a queue - adds to the end, removes
# from the front. It can also use a queue data structure such as
# 'queueList' or 'queueDictionary'.
def bstLevelMin( node, print_func ):
    q = []
    q.append( node )
    count = 0
    mod = 1

    while len( q ) > 0:
        node = q.pop( 0 )
        
        if node != None:
            print_func( node['data'] )
            count += 1
            
            if count % mod == 0:
                print
                mod = mod * 2 + 1
            q.append( node['left'] )
            q.append( node['right'] )
    return

#---------------------------------------------------------
# Utility function for retrieving the data comparison function.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
# Preconditions:
#    bst - valid linked BST dictionary
# Postconditions:
#    Returns bst's data comparison function.
#-------------------------------------------------------
def bst_get_cmp_func( bst ):
    return bst['cmp_func']

#---------------------------------------------------------
# Utility function for changing the data comparison function.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
#    cmp_func       - data comparison function
# Preconditions:
#    bst - valid linked BST dictionary
#    cmp_func       - valid function reference
# Postconditions:
#    Changes bst's comparison function to cmp_func.
#-------------------------------------------------------
def bst_set_cmp_func( bst, cmp_func ):
    bst['cmp_func'] = cmp_func
    return

#---------------------------------------------------------
# Utility function for retrieving the data print function.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
# Preconditions:
#    bst - valid linked BST dictionary
# Postconditions:
#    Returns bst's print function.
#-------------------------------------------------------
def bst_get_print_func( bst ):
    return bst['print_func']

#---------------------------------------------------------
# Utility function for changing the data print function.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
#    print_func     - data print function
# Preconditions:
#    bst - valid linked BST dictionary
#    print_func     - valid function reference
# Postconditions:
#    Changes bst's print function to print_func.
#-------------------------------------------------------
def bst_set_print_func( bst, print_func ):
    bst['print_func'] = print_func
    return


#---------------------------------------------------------
# Returns the contents of bst in an array in sorted order.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
# Preconditions:
#    bst - valid linked BST dictionary
# Postconditions:
#    Returns the contents of bst in an array in sorted order.
#-------------------------------------------------------
def bst_traverse( bst ):
    a = []
    bst_traverse_aux( bst['root'], a )
    return a
    
# Traverse the BST in inorder, adding the contents of each node
# to an array.
def bst_traverse_aux( node, a ) :
    
    if( node != None ) :
        bst_traverse_aux( node['left'], a )
        a.append( node['data'] )
        bst_traverse_aux( node['right'], a )
    return




#---------------------------------------------------------
# Inserts a value into a BST.
#-------------------------------------------------------
# Parameters:
#    bst - a BST.
#    value - a data element
# Preconditions:
#    bst - valid linked BST.
#    value - none
# Postconditions:
#    value is added to bst - only one copy of value is allowed in bst.
#-------------------------------------------------------
def bst_add( bst, value ):
    bst['root'] = bst_add_aux( bst['root'], (value,1), bst['cmp_func'] )
    return

# Adds 'value' to bst. Allows only one copy of 'value'
# in the tree. Returns the tree root.
def bst_add_aux( node, value, cmp_func ):
    
    if node is None:
        # Base case: node is empty, so construct a new node with 'data'.
        node = { 'left':None, 'data':copy.deepcopy( value ), 'right':None }
    else:
        # General case: move to the left or right subtree depending on the value of 'data'.
        var1, count1 = node['data']
        var2, count2 = value
        result = cmp_func( var2, var1 )

        if result == LESSER:
            # Search the left subtree.
            node['left'] = bst_add_aux( node['left'], value, cmp_func )
        elif result == GREATER:
            # Search the right subtree.
            node['right'] = bst_add_aux( node['right'], value, cmp_func )
        # Multiple copies of 'data' are not allowed in the tree - do nothing.
        else: #result is EQUALS           
            var, count = node['data']
            node['data'] = (var,count+1)
          
            
    return node

#---------------------------------------------------------
# Returns the contents of bst in an array in sorted order.
#-------------------------------------------------------
# Parameters:
#    bst - a BST
# Preconditions:
#    bst - valid linked BST dictionary
# Postconditions:
#    Returns the contents of bst in an array in sorted order.
#-------------------------------------------------------
def bst_add_traverse( bst ):
    a = []
    bst_add_traverse_aux( bst['root'], a )
    return a
    
# Traverse the BST in inorder, adding the contents of each node
# to an array.
def bst_add_traverse_aux( node, a ) :
    
    if( node != None ) :
        var, count = node['data']
        bst_add_traverse_aux( node['left'], a )
        for count in range(count):
            a.append( var )
        bst_add_traverse_aux( node['right'], a )
    return