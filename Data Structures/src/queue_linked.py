#-------------------------------------------------------
# queue_linked.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Linked version of the Queue ADT.
#-------------------------------------------------------
import copy

#---------------------------------------------------------
# Returns an empty queue. The queue header consists of
# the current queue size and the first dictionary in the
# queue. Each dictionary in the queue itself consists of
# the data for that node and the next dictionary in the
# queue.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
# Preconditions:
#    print_func - valid function reference
# Postconditions:
#    Returns an empty queue.
#-------------------------------------------------------
def queue_init( print_func ):
    queue = { 'size':0, 'front':None, 'rear':None, 'print_func':print_func }
    return queue

#---------------------------------------------------------
# Returns the size of queue.
#-------------------------------------------------------
# Parameters:
#    queue - a queue
# Preconditions:
#    queue - valid linked queue dictionary
# Postconditions:
#    Returns the size of queue.
#-------------------------------------------------------
def queue_size( queue ):
    return queue['size']

#---------------------------------------------------------
# Determines if queue is empty.
#-------------------------------------------------------
# Parameters:
#    queue - a queue
# Preconditions:
#    queue - valid linked queue dictionary
# Postconditions:
#    Returns True if queue is empty, False otherwise.
#-------------------------------------------------------
def queue_empty( queue ):
    return queue['size'] == 0

#---------------------------------------------------------
# Inserts value into queue.
#-------------------------------------------------------
# Parameters:
#    queue - a queue
#    value - a data element
# Preconditions:
#    queue - valid linked queue dictionary
#    value - none
# Postconditions:
#    value is added to the front of queue.
#-------------------------------------------------------
def queue_insert( queue, value ):
#queue = { 'size':0, 'front':None, 'rear':None, 'print_func':print_func }
    # Your code here.

    #increase the size
    queue['size'] += 1
    add_this = {'data':copy.deepcopy(value), 'next':None }
    #take the value that is at the front, and make it point to our new value
    if queue['front'] == None:
        queue['front'] = add_this
        queue['rear'] = add_this
    else:
        queue['rear']['next'] = add_this
        queue['rear'] = add_this

    

   
    
    return

#---------------------------------------------------------
# Removes value from queue.
#-------------------------------------------------------
# Parameters:
#    queue - a queue
# Preconditions:
#    queue - valid linked queue dictionary
# Postconditions:
#    Returns the value at the front of queue - the value is
#    removed from queue. Returns None if queue is empty.
#-------------------------------------------------------
def queue_remove( queue ):
    value = None
    if queue['size'] > 0:
        value = queue['front']['data']
        queue['front'] = queue['front']['next']
        queue['size'] -= 1
        if queue['front'] == None:
            queue['rear'] = None
    # Your code here.

    return value

#---------------------------------------------------------
# Peeks at the front of queue.
#-------------------------------------------------------
# Parameters:
#    queue - a queue
# Preconditions:
#    queue - valid linked queue dictionary
# Postconditions:
#    Returns a copy of the value at the front of queue - 
#    the value is not removed from queue. Returns None 
#    if queue is empty.
#-------------------------------------------------------
def queue_peek( queue ):
#queue = { 'size':0, 'front':None, 'rear':None, 'print_func':print_func }
    # Your code here.
    value = None
    if queue['size'] > 0:
        value = copy.deepcopy(queue['front']['data'])        

    return value

#---------------------------------------------------------
# Prints the contents of queue using queue's custom
# printing function in the order direction' ('f' or 'r').
#-------------------------------------------------------
# Parameters:
#    queue - a queue
#    direction - direction to print queue contents in
# Preconditions:
#    queue - valid linked queue dictionary
#    direction - string 'f' or 'r'
# Postconditions:
#    Prints each value in queue using print_func.
#    Prints in forward order if direction is 'f', in
#    reverse order if direction is 'r'.
#-------------------------------------------------------
def queue_print( queue, direction ):
    if direction == 'f':
        queue_print_forward_aux( queue['front'], queue['print_func'] )
    elif direction == 'r':
        queue_print_reverse_aux( queue['front'], queue['print_func'] )
    return
    
def queue_print_forward_aux( node, print_func ):
    if node is not None:
        print_func( node['data'] )
        queue_print_forward_aux( node['next'], print_func )
    return
    
def queue_print_reverse_aux( node, print_func ):
    if node is not None:
        queue_print_reverse_aux( node['next'], print_func )
        print_func( node['data'] )
    return
