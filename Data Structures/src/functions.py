#-------------------------------------------------------
# functions.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Functions for Stacks (cont'd) Lab.
#-------------------------------------------------------
from stack_array import *
#from stack_linked import *

#---------------------------------------------------------
# Utility function for moving list data to a stack.
# Uses only stack ADT functions.
#-------------------------------------------------------
# Parameters:
#    stack - a stack
#    data_list - data
# Preconditions:
#    stack - valid stack dictionary
#    data_list - a valid list
# Postconditions:
#    stack contains contents of data_list, data_list is empty.
#-------------------------------------------------------
def list_to_stack( stack, data_list ):
    
    while data_list != []:
        stack_push( stack, data_list.pop( 0 ) )    
    return

#---------------------------------------------------------
# Utility function for moving stack data to a list.
# Uses only stack ADT functions.
#-------------------------------------------------------
# Parameters:
#    stack - a stack
#    data_list - data
# Preconditions:
#    stack - valid stack dictionary
#    data_list - a valid list
# Postconditions:
#    data_list contains contents of stack, stack is empty.
#-------------------------------------------------------
def stack_to_list( stack, data_list ):
    
    while not stack_empty( stack ):
        data_list.append( stack_pop( stack ) )
    return
    
#---------------------------------------------------------
# Utility function for reversing the contents of a stack.
# Uses only stack ADT functions.
# Takes advantage of the fact that the previous two functions
# reverse the stack contents when moving from a stack to a list
# and visa versa. (Note: functions were written that way!)
#-------------------------------------------------------
# Parameters:
#    stack - a stack
# Preconditions:
#    stack - valid stack dictionary
# Postconditions:
#    Contents of stack are reversed.
#-------------------------------------------------------
def r_stack( stack ):
    data_list = []
    stack_to_list( stack, data_list )
    list_to_stack( stack, data_list )
    return
