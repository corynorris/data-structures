from priority_queue_linked import *

def print_(x):
    print x
    return
def compare(a,b):
    value = 0

    if a > b:
        value = 1
    if a < b:
        value = -1
    
    return value


q = priority_queue_init(print_,compare)

print 'adding values to queue:\n7\n6\n9\n5\n5'
priority_queue_insert(q,7)
priority_queue_insert(q,6)
priority_queue_insert(q,9)
priority_queue_insert(q,5)
priority_queue_insert(q,5)

print 'peeking at: %d'%( priority_queue_peek(q) )

print 'removing values from q'
while priority_queue_empty(q) == False:
    print 'value removed: %d' %(priority_queue_remove(q))


