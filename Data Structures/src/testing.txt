list_to_stack - Copy list to stack
Stack:
5
4
3
2
1
0
List:
[]

stack_to_list - Copy stack to list:
Stack:
List:
[5, 4, 3, 2, 1, 0]

r_stack - uses ADT
stack before:
5
4
3
2
1
0
stack after:
0
1
2
3
4
5

reverse_stack - extends ADT
stack before:
0
1
2
3
4
5
stack after:
5
4
3
2
1
0

