#-------------------------------------------------------
# list_linked.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Linked version of the List ADT.
#-------------------------------------------------------
import copy

#---------------------------------------------------------
# Returns an empty list.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
#    cmp_func - data comparison function
# Preconditions:
#    print_func - valid function reference
#    cmp_func - valid function reference
# Postconditions:
#    Returns an empty list.
#-------------------------------------------------------
def list_init( print_func, cmp_func ):
    list = { 'size':0, 'front':None, 'print_func':print_func, 'cmp_func':cmp_func }
    return list

#---------------------------------------------------------
# Returns the size of list.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid linked list dictionary
# Postconditions:
#    Returns the size of list.
#-------------------------------------------------------
def list_size( list ):
    return list['size']

#---------------------------------------------------------
# Determines if list is empty.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid linked list dictionary
# Postconditions:
#    Returns True if list is empty, False otherwise.
#-------------------------------------------------------
def list_empty( list ):
    return list['size'] == 0

#---------------------------------------------------------
# Inserts value into list.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    value - a data element
# Preconditions:
#    list - valid linked list dictionary
#    value - none
# Postconditions:
#    value is added to list.
#-------------------------------------------------------
def list_insert( list, value ):
    node = { 'data':copy.deepcopy( value ), 'next':list['front'] }
    list['front'] = node
    list['size'] += 1
    return

#---------------------------------------------------------
# Removes value from list.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid linked list dictionary
# Postconditions:
#    Returns the value at the front of list - the value is
#    removed from list. Returns None if list is empty.
#-------------------------------------------------------
def list_remove( list, key ):
    value = None
    item = list['front']
    last_item = item
    
    if list['size'] == 0:
        value = None
    else:
        for count in range(list['size']):
            
            if (list['cmp_func'](item['data'],key) == 0):
                value =item['data']
                item = item['next']
                last_item['next'] = item 

                list['size'] = list['size'] -1
                break;
            
            last_item = item
            item = item['next']         
        
        if value == None:
            value = str(key) +" cannot be found"
            

    return value

#---------------------------------------------------------
# Finds a value in list.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    key - a data element
# Preconditions:
#    list - valid linked list dictionary
#    key - a data element usable by cmp_func
# Postconditions:
#    Returns a copy of the value matching key, otherwise None.
#    (Uses an iterative algorithm)
#-------------------------------------------------------
'''
def list_find( list, key ):
    value = None
    item = list['front']
    if list['size'] == 0:
        value = None
    else:
        for count in range(list['size']):
            if (list['cmp_func'](item['data'],key) == 0):
                value = copy.deepcopy(item['data']) 
            item = item['next']         
        
        if value == None:
            value = str(key) +" cannot be found"
            

    return value
'''
#---------------------------------------------------------
# Finds a value in list.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    key - a data element
# Preconditions:
#    list - valid linked list dictionary
#    key - a data element usable by cmp_func
# Postconditions:
#    Returns a copy of the value matching key, otherwise None.
#    (Uses a recursive algorithm)
#-------------------------------------------------------

def list_find( list, key ):
    
    value = None
    node = list['front']
    if list['size'] != 0:
        value = list_find_aux(list['cmp_func'],key, node)

    return value

def list_find_aux( cmp_func, key, node):
    
    #if we don't find anything
    if node == None:
        value = str(key)+" cannot be found" 
    #check if there is a match
    elif cmp_func(node['data'],key) == 0:   
        value = copy.deepcopy(node['data'])
    #otherwise, check the next
    else:
        value = list_find_aux(cmp_func,key, node['next'])
        
    return value
        
        
#---------------------------------------------------------
# Determines whether two lists are identical.
#-------------------------------------------------------
# Parameters:
#    list1 - a list
#    list2 - a list
# Preconditions:
#    list1 - valid linked list dictionary
#    list2 - valid linked list dictionary
# Postconditions:
#    Returns True if list1 contains the same values as list2
#    in the same order, otherwise returns False.
#    (Uses an iterative algorithm)
#-------------------------------------------------------
def list_identical( list1, list2 ):
    
    # your code here

    return is_identical

#---------------------------------------------------------
# Determines whether two lists are identical.
#-------------------------------------------------------
# Parameters:
#    list1 - a list
#    list2 - a list
# Preconditions:
#    list1 - valid linked list dictionary
#    list2 - valid linked list dictionary
# Postconditions:
#    Returns True if list1 contains the same values as list2
#    in the same order, otherwise returns False.
#    (Uses a recursive algorithm)
#-------------------------------------------------------
def list_identical( list1, list2 ):
    
    # your code here

    return is_identical

#---------------------------------------------------------
# Prints the contents of list using list's custom
# printing function in the order direction' ('f' or 'r').
#-------------------------------------------------------
# Parameters:
#    list - a list
#    direction - direction to print list contents in
# Preconditions:
#    list - valid linked list dictionary
#    direction - string 'f' or 'r'
# Postconditions:
#    Prints each value in list using print_func.
#    Prints in forward order if direction is 'f', in
#    reverse order if direction is 'r'.
#-------------------------------------------------------
def list_print( list, direction ):
    if direction == 'f':
        list_print_forward_aux( list['front'], list['print_func'] )
    elif direction == 'r':
        list_print_reverse_aux( list['front'], list['print_func'] )
    return
    
def list_print_forward_aux( node, print_func ):
    if node is not None:
        print_func( node['data'] )
        list_print_forward_aux( node['next'], print_func )
    return
    
def list_print_reverse_aux( node, print_func ):
    if node is not None:
        list_print_reverse_aux( node['next'], print_func )
        print_func( node['data'] )
    return

#---------------------------------------------------------
# Utility function for retrieving the data print function.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid array list dictionary
# Postconditions:
#    Returns list's print function.
#-------------------------------------------------------
def list_get_print_func( list ):
    return list['print_func']

#---------------------------------------------------------
# Utility function for changing the data print function.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    print_func - data print function
# Preconditions:
#    list - valid array list dictionary
#    print_func - valid function reference
# Postconditions:
#    Changes list's print function to print_func.
#-------------------------------------------------------
def list_set_print_func( list, print_func ):
    list['print_func'] = print_func
    return

#---------------------------------------------------------
# Utility function for retrieving the data comparison function.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid array list dictionary
# Postconditions:
#    Returns list's comparison function.
#-------------------------------------------------------
def list_get_cmp_func( list ):
    return list['print_func']

#---------------------------------------------------------
# Utility function for changing the data comparison function.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    cmp_func - data print function
# Preconditions:
#    list - valid array list dictionary
#    cmp_func - valid function reference
# Postconditions:
#    Changes list's comparison function to cmp_func.
#-------------------------------------------------------
def list_set_cmp_func( list, cmp_func ):
    list['cmp_func'] = cmp_func
    return
