#-------------------------------------------------------
# stack_linked.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Linked version of the Stack ADT.
#-------------------------------------------------------
import copy

#---------------------------------------------------------
# Returns an empty stack data structure. Stores the
# custom print function print_func for the data to be 
# stored stack.
#
# Returns an empty stack. The stack header consists of
# the current stack size and the first dictionary in the
# stack. Each dictionary in the stack itself consists of
# the data for that node and the next dictionary in the
# stack.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
# Preconditions:
#    print_func - valid function reference
# Postconditions:
#    Returns an empty stack.
#-------------------------------------------------------
def stack_init( print_func ):
    stack = { 'size':0, 'top':None, 'print_func':print_func }
    return stack

#---------------------------------------------------------
# Returns the size of stack.
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
# Preconditions:
#    stack - valid linked stack dictionary
# Postconditions:
#    Returns the size of stack.
#-------------------------------------------------------
def stack_size( stack ):
    return stack['size']

#---------------------------------------------------------
# Determines if stack is empty.
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
# Preconditions:
#    stack - valid linked stack dictionary
# Postconditions:
#    Returns True if stack is empty, False otherwise.
#-------------------------------------------------------
def stack_empty( stack ):
    return stack['size'] == 0

#---------------------------------------------------------
# Pushes onto stack.
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
#    value - a data element
# Preconditions:
#    stack - valid linked stack dictionary
#    value - none
# Postconditions:
#    value is added to the top of stack.
#-------------------------------------------------------
def stack_push( stack, value ):
    node = { 'data':copy.deepcopy( value ), 'next':stack['top'] }
    stack['top'] = node
    stack['size'] += 1
    return

#---------------------------------------------------------
# Pops the top of stack.
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
# Preconditions:
#    stack - valid linked stack dictionary
# Postconditions:
#    Returns the value at the top of stack - the value is
#    removed from stack. Returns None if stack is empty.
#-------------------------------------------------------
def stack_pop( stack ):
    if stack['top'] is None:
        value = None
    else:
        value = stack['top']['data']
        stack['top'] = stack['top']['next']
        stack['size'] -= 1
    return value

#---------------------------------------------------------
# Peeks at the top of stack.
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
# Preconditions:
#    stack - valid linked stack dictionary
# Postconditions:
#    Returns a copy of the value at the top of stack - 
#    the value is not removed from stack. Returns None 
#    if stack is empty.
#-------------------------------------------------------
def stack_peek( stack ):
    if stack['top'] == None:
        value = None
    else:
        value = copy.deepcopy( stack['top']['data'] )
    return value

#---------------------------------------------------------
# Prints the contents of stack using stack's custom
# printing function in the order direction' ('f' or 'r').
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
#    direction - direction to print stack contents in
# Preconditions:
#    stack - valid linked stack dictionary
#    direction - string 'f' or 'r'
# Postconditions:
#    Prints each value in stack using print_func.
#    Prints in forward order if direction is 'f', in
#    reverse order if direction is 'r'.
#-------------------------------------------------------
def stack_print( stack, direction ):
    if direction == 'f':
        stack_print_forward_aux( stack['top'], stack['print_func'] )
    elif direction == 'r':
        stack_print_reverse_aux( stack['top'], stack['print_func'] )
    return
    
def stack_print_forward_aux( node, print_func ):
    if node != None:
        print_func( node['data'] )
        stack_print_forward_aux( node['next'], print_func )
    return
    
def stack_print_reverse_aux( node, print_func ):
    if node != None:
        stack_print_reverse_aux( node['next'], print_func )
        print_func( node['data'] )
    return

#---------------------------------------------------------
# Reverses the contents of stack.
#-------------------------------------------------------
# Parameters:
#    stack - a stack
# Preconditions:
#    stack - valid linked stack dictionary
# Postconditions:
#    Contents of stack are reversed in order.
#-------------------------------------------------------
def stack_reverse( stack ):
    new_top = None          # Top of the 'new' stack.
    current = stack['top']  # Node to process.
    
    our_func(stack, new_top, current)
    
    return
'''    while current != None:
        # Preserve the next node to process.
        next = current['next']
        # Push the current node onto the 'new' stack.
        current['next'] = new_top
        new_top = current
        #  Move to next node to process.
        current = next
        
    stack['top'] = new_top
    return
''' 
 

def our_func(stack, new_top, current):
    #something
    
    if current == None:
        stack['top'] = new_top
        return
    else:
        # Preserve the next node to process.
        next = current['next']
        # Push the current node onto the 'new' stack.
        current['next'] = new_top
        new_top = current
        #  Move to next node to process.
        current = next
        #call our func again
        our_func(stack,new_top,current)
    
    return





'''
def stack_reverse( stack ):
    
    new_top = None          # Top of the 'new' stack.
    current = stack['top']  # Node to process.
    
    while current != None:
        # Preserve the next node to process.
        next = current['next']
        # Push the current node onto the 'new' stack.
        current['next'] = new_top
        new_top = current
        #  Move to next node to process.
        current = next
        
    stack['top'] = new_top
    return
'''

