#-------------------------------------------------------
# priority_queue_array.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Array version of the Priority Queue ADT.
#-------------------------------------------------------
# Constant Definitions
#    Comparison value names.
#-------------------------------------------------------
LESSER = -1
EQUALS = 0
GREATER = 1

import copy

#---------------------------------------------------------
# Returns an empty priority queue.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
#    cmp_func   - data comparison function
# Preconditions:
#    print_func - valid function reference
#    cmp_func   - valid function reference
# Postconditions:
#    Returns an empty priority queue.
#-------------------------------------------------------
def priority_queue_init( print_func, cmp_func ):
    priority_queue = { 'data':[], 'print_func':print_func, 'cmp_func':cmp_func }
    return priority_queue

#---------------------------------------------------------
# Returns the size of priority_queue.
#-------------------------------------------------------
# Parameters:
#    priority_queue - a priority queue
# Preconditions:
#    priority_queue - valid array priority queue dictionary
# Postconditions:
#    Returns the size of priority_queue.
#-------------------------------------------------------
def priority_queue_size( priority_queue ):
    return len( priority_queue['data'] )

#---------------------------------------------------------
# Determines if priority_queue is empty.
#-------------------------------------------------------
# Parameters:
#    priority_queue - a priority queue
# Preconditions:
#    priority_queue - valid array priority queue dictionary
# Postconditions:
#    Returns True if priority_queue is empty, False otherwise.
#-------------------------------------------------------
def priority_queue_empty( priority_queue ):
    return ( len( priority_queue['data'] ) == 0 )

#---------------------------------------------------------
# Inserts value into priority_queue.
#-------------------------------------------------------
# Parameters:
#    priority_queue - a priority queue
#    value - a data element
# Preconditions:
#    priority_queue - valid array priority queue dictionary
#    value - none
# Postconditions:
#    value is added to priority queue.
#-------------------------------------------------------
def priority_queue_insert( priority_queue, value ):
    priority_queue['data'].append( copy.deepcopy( value ) )
    return

#---------------------------------------------------------
# Removes value from priority_queue.
#-------------------------------------------------------
# Parameters:
#    priority_queue - a data queue
# Preconditions:
#    priority_queue - valid array queue dictionary
# Postconditions:
#    Returns the highest priority value in priority_queue - the value is
#    removed from priority_queue. Returns None if priority_queue is empty.
#-------------------------------------------------------
def priority_queue_remove( priority_queue ):
    highest_priority = priority_queue['data'][0]
    position = 0
    
    '''
    for x in priority_queue['data']:  
        #if its bigger it is higher priority record where it is
        if priority_queue['cmp_func'](x,highest_priority) == GREATER :
            highest_priority = x


    
    priority_queue['data'].remove(highest_priority)
    '''
    for count in range(len(priority_queue['data'])):  
        #if its bigger it is higher priority record where it is
        if priority_queue['cmp_func'](priority_queue['data'][count],highest_priority) == GREATER :
            highest_priority = priority_queue['data'][0]
            position = count

    
    highest_priority = priority_queue['data'].pop(position)
    
    
    
    
    return highest_priority

#---------------------------------------------------------
# Peeks at the highest priority value in priority_queue.
#-------------------------------------------------------
# Parameters:
#    priority_queue - a priority queue
# Preconditions:
#    priority_queue - valid array priority queue dictionary
# Postconditions:
#    Returns a copy of the highest priority value in priority_queue - 
#    the value is not removed from priority_queue. Returns None 
#    if priority_queue is empty.
#-------------------------------------------------------
def priority_queue_peek( priority_queue ):
    value = None
    if len(priority_queue) > 0:
        highest_priority = priority_queue['data'][0]
       
        for x in priority_queue['data']:  
            #if its bigger it is higher priority record where it is
            if priority_queue['cmp_func'](x,highest_priority) == GREATER :
                highest_priority = x   

        value = copy.deepcopy(highest_priority)
    return value

#---------------------------------------------------------
# Prints the contents of priority_queue using priority_queue's custom
# printing function in the order direction' ('f' or 'r').
#-------------------------------------------------------
# Parameters:
#    priority_queue - a priority queue
#    direction - direction to print queue contents in
# Preconditions:
#    priority_queue - valid array priority queue dictionary
#    direction - string 'f' or 'r'
# Postconditions:
#    Prints each value in priority_queue using print_func.
#    Prints in forward order if direction is 'f', in
#    reverse order if direction is 'r'.
#-------------------------------------------------------
def priority_queue_print( priority_queue, direction ):
    print_func = priority_queue['print_func']
    n = len( priority_queue['data'] )
    
    if direction == 'f':
        for i in range( n ):
            print_func( priority_queue['data'][i] )
    elif direction == 'r':
        for i in range( n-1, -1, -1 ):
            print_func( priority_queue['data'][i] )
    return
