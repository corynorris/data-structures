#-------------------------------------------------------
# list_array.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Array version of the List ADT.
#-------------------------------------------------------
import copy

#---------------------------------------------------------
# Returns an empty list.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
# Preconditions:
#    print_func - valid function reference
# Postconditions:
#    Returns an empty list.
#-------------------------------------------------------
def list_init( print_func, cmp_func ):
    list = { 'data':[], 'print_func':print_func, 'cmp_func':cmp_func }
    return list

#---------------------------------------------------------
# Returns the size of list.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid array list dictionary
# Postconditions:
#    Returns the size of list.
#-------------------------------------------------------
def list_size( list ):
    return len( list['data'] )

#---------------------------------------------------------
# Determines if list is empty.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid array list dictionary
# Postconditions:
#    Returns True if list is empty, False otherwise.
#-------------------------------------------------------
def list_empty( list ):
    return ( len( list['data'] ) == 0 )

#---------------------------------------------------------
# Inserts value into list.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    value - a data element
# Preconditions:
#    list - valid array list dictionary
#    value - none
# Postconditions:
#    value is added to list.
#-------------------------------------------------------
def list_insert( list, value ):
    list['data'].append( copy.deepcopy( value ) )
    return

#---------------------------------------------------------
# Removes value from list.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    key - a data element
# Preconditions:
#    list - valid array list dictionary
#    key - a data element usable by cmp_func
# Postconditions:
#    Returns the value matching key, otherwise None.
#-------------------------------------------------------
def list_remove( list, key ):
    value = None
    
    if len(list) == 0:
        value = None
    else:
        for count in range(len(list['data'])):
            item = list['data'][count]
            if (list['cmp_func'](item, key) == 0):
                value = count
                
               
        if value == None:
            value = str(key) + " cannot be found"
        else:
            value = list['data'].pop(value)
    
    # your code here

    return value
    

#---------------------------------------------------------
# Finds a value in list.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    key - a data element
# Preconditions:
#    list - valid array list dictionary
#    key - a data element usable by cmp_func
# Postconditions:
#    Returns a copy of the value matching key, otherwise None.
#-------------------------------------------------------
def list_find( list, key ):
   
    value = None

    if len(list) == 0:
        value = None
    else:
        for item in list['data']:
            if (list['cmp_func'](item,key) == 0):
                value = copy.deepcopy(item)     
        if value == None:
            value = str(key) + " cannot be found"
    
    # your code here

    return value

#---------------------------------------------------------
# Determines whether two lists are identical.
#-------------------------------------------------------
# Parameters:
#    list1 - a list
#    list2 - a list
# Preconditions:
#    list1 - valid array list dictionary
#    list2 - valid array list dictionary
# Postconditions:
#    Returns True if list1 contains the same values as list2
#    in the same order, otherwise returns False.
#-------------------------------------------------------
def list_identical( list1, list2 ):

    is_identical = True
    if len(list1['data']) == len(list2['data']):
        for count in range (len(list1['data'])):
            if list1['data'][count] != list2['data'][count]:
                is_identical = False 
    else:
        is_identical = False   
    return is_identical
    
#---------------------------------------------------------
# Prints the contents of list using list's custom
# printing function in the order direction' ('f' or 'r').
#-------------------------------------------------------
# Parameters:
#    list - a list
#    direction - direction to print list contents in
# Preconditions:
#    list - valid array list dictionary
#    direction - string 'f' or 'r'
# Postconditions:
#    Prints each value in list using print_func.
#    Prints in forward order if direction is 'f', in
#    reverse order if direction is 'r'.
#-------------------------------------------------------
def list_print( list, direction ):
    print_func = list['print_func']
    n = len( list['data'] )
    
    if direction == 'f':
        for i in range( n ):
            print_func( list['data'][i] )
    elif direction == 'r':
        for i in range( n - 1, - 1, - 1 ):
            print_func( list['data'][i] )
    return

#---------------------------------------------------------
# Utility function for retrieving the data print function.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid array list dictionary
# Postconditions:
#    Returns list's print function.
#-------------------------------------------------------
def list_get_print_func( list ):
    return list['print_func']

#---------------------------------------------------------
# Utility function for changing the data print function.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    print_func - data print function
# Preconditions:
#    list - valid array list dictionary
#    print_func - valid function reference
# Postconditions:
#    Changes list's print function to print_func.
#-------------------------------------------------------
def list_set_print_func( list, print_func ):
    list['print_func'] = print_func
    return

#---------------------------------------------------------
# Utility function for retrieving the data comparison function.
#-------------------------------------------------------
# Parameters:
#    list - a list
# Preconditions:
#    list - valid array list dictionary
# Postconditions:
#    Returns list's comparison function.
#-------------------------------------------------------
def list_get_cmp_func( list ):
    return list['cmp_func']#fixed this

#---------------------------------------------------------
# Utility function for changing the data comparison function.
#-------------------------------------------------------
# Parameters:
#    list - a list
#    cmp_func - data print function
# Preconditions:
#    list - valid array list dictionary
#    cmp_func - valid function reference
# Postconditions:
#    Changes list's comparison function to cmp_func.
#-------------------------------------------------------
def list_set_cmp_func( list, cmp_func ):
    list['cmp_func'] = cmp_func
    return
