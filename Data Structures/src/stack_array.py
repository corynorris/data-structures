#-------------------------------------------------------
# stack_array.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Array version of the Stack ADT.
#-------------------------------------------------------
import copy

#---------------------------------------------------------
# Returns an empty stack data structure. Stores the
# custom print function 'print_func' for the data to be 
# stored 'stack'.
#
# The stack header consists of a 'data' element
# and a 'print_func' element. Data is stored in a list.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
# Preconditions:
#    print_func - valid function reference
# Postconditions:
#    Returns an empty stack.
#-------------------------------------------------------
def stack_init( print_func ):
    stack = { 'data':[], 'print_func':print_func }
    return stack

#---------------------------------------------------------
# Returns the size of stack.
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
# Preconditions:
#    stack - valid array stack dictionary
# Postconditions:
#    Returns the size of stack.
#-------------------------------------------------------
def stack_size( stack ):
    return len( stack['data'] )

#---------------------------------------------------------
# Determines if stack is empty.
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
# Preconditions:
#    stack - valid array stack dictionary
# Postconditions:
#    Returns True if stack is empty, False otherwise.
#-------------------------------------------------------
def stack_empty( stack ):
    return ( len( stack['data'] ) == 0 )

#---------------------------------------------------------
# Pushes onto stack.
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
#    value - a data element
# Preconditions:
#    stack - valid array stack dictionary
#    value - none
# Postconditions:
#    value is added to the top of stack.
#-------------------------------------------------------
def stack_push( stack, value ):
    stack['data'].append( copy.deepcopy( value ) )
    return

#---------------------------------------------------------
# Pops the top of stack.
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
# Preconditions:
#    stack - valid array stack dictionary
# Postconditions:
#    Returns the value at the top of stack - the value is
#    removed from stack. Returns None if stack is empty.
#-------------------------------------------------------
def stack_pop( stack ):
    if( len( stack['data'] ) > 0 ):
        value = stack['data'].pop()
    else:
        value = None
    return value

#---------------------------------------------------------
# Peeks at the top of stack.
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
# Preconditions:
#    stack - valid array stack dictionary
# Postconditions:
#    Returns a copy of the value at the top of stack - 
#    the value is not removed from stack. Returns None 
#    if stack is empty.
#-------------------------------------------------------
def stack_peek( stack ):
    n = len( stack['data'])
    
    if( n > 0 ):
        value = copy.deepcopy( stack['data'][n-1] )
    else:
        value = None
    return value

#---------------------------------------------------------
# Prints the contents of stack using stack's custom
# printing function in the order direction' ('f' or 'r').
#-------------------------------------------------------
# Parameters:
#    stack - a data stack
#    direction - direction to print stack contents in
# Preconditions:
#    stack - valid array stack dictionary
#    direction - string 'f' or 'r'
# Postconditions:
#    Prints each value in stack using print_func.
#    Prints in forward order if direction is 'f', in
#    reverse order if direction is 'r'.
#-------------------------------------------------------
def stack_print( stack, direction ):
    print_func = stack['print_func']
    n = len( stack['data'] )
    
    if direction == 'f':
        for i in range( n-1, -1, -1 ):
            print_func( stack['data'][i] )
    elif direction == 'r':
        for i in range( n ):
            print_func( stack['data'][i] )
    return

#---------------------------------------------------------
# Reverses the contents of stack.
#-------------------------------------------------------
# Parameters:
#    stack - a stack
# Preconditions:
#    stack - valid array stack dictionary
# Postconditions:
#    Contents of stack are reversed in order.
#-------------------------------------------------------
def stack_reverse( stack ):
    first = 0
    last = len( stack['data'] ) - 1
    rev_list_ip(stack, first, last)
    
    return 
def rev_list_ip( data, m, n ):
    
    if n - m > 1:
       swap( data, m, n )
       rev_list_ip( data, m + 1, n - 1 )
    return
'''
def stack_reverse( stack ):
    first = 0
    last = len( stack['data'] ) - 1
    
    reverse(stack, first, last)    
    
    return

def reverse(stack, first, last):
    if first - last >= 0:
        return
    else:
        swap(stack, first, last)
        something(stack, first+1, last-1)
                
    return
'''
def swap( data, i, j ):
    temp = data['data'][i]
    data['data'][i] = data['data'][j]
    data['data'][j] = temp
    return
