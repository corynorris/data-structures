#-------------------------------------------------------
# priority_queue_array.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Array version of the Priority Queue ADT.
#-------------------------------------------------------
# Constant Definitions
#    Comparison value names.
#-------------------------------------------------------
LESSER = - 1
EQUALS = 0
GREATER = 1

import copy

#---------------------------------------------------------
# Returns an empty priority queue.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
#    cmp_func   - data comparison function
# Preconditions:
#    print_func - valid function reference
#    cmp_func   - valid function reference
# Postconditions:
#    Returns an empty priority queue.
#-------------------------------------------------------
def priority_queue_init( print_func, cmp_func ):
    priority_queue = { 'size':0, 'front':None, 'print_func':print_func, 'cmp_func':cmp_func }
    return priority_queue

#---------------------------------------------------------
# Returns the size of priority_queue.
#-------------------------------------------------------
# Parameters:
#    priority_queue - a priority queue
# Preconditions:
#    priority_queue - valid linked priority queue dictionary
# Postconditions:
#    Returns the size of priority_queue.
#-------------------------------------------------------
def priority_queue_size( priority_queue ):
    return priority_queue['size']

#---------------------------------------------------------
# Determines if priority_queue is empty.
#-------------------------------------------------------
# Parameters:
#    priority_queue - a priority queue
# Preconditions:
#    priority_queue - valid linked priority queue dictionary
# Postconditions:
#    Returns True if priority_queue is empty, False otherwise.
#-------------------------------------------------------
def priority_queue_empty( priority_queue ):
    return priority_queue['size'] == 0

#---------------------------------------------------------
# Inserts value into priority_queue.
#-------------------------------------------------------
# Parameters:
#    priority_queue - a priority queue
#    value - a data element
# Preconditions:
#    priority_queue - valid linked priority queue dictionary
#    value - none
# Postconditions:
#    value is added to priority queue.
#-------------------------------------------------------
def priority_queue_insert( priority_queue, value ):
    #queue = { 'size':0, 'front':None, 'rear':None, 'print_func':print_func }
    # Your code here.

    #increase the size
    priority_queue['size'] += 1
    add_this = {'data':copy.deepcopy(value), 'next':None }
    added = False
    #take the value that is at the front, and make it point to our new value
    if priority_queue['front'] == None:
        priority_queue['front'] = add_this
        
    else:
        
        #Find where it should be placed by pointing at a priority right beside it.
       
        #check if its the greatest value, add it at the end if it is.       
        #loop through and compare
        
        
        # if it is bigger than the front value, add it.
        if priority_queue['cmp_func'](value, priority_queue['front']['data']) == GREATER:
            add_this['next'] = priority_queue['front']
            priority_queue['front'] = add_this
          
        else:
            #otherwise if it is smaller loop through until it is greater than the next value
            next = priority_queue['front']['next']
            current = priority_queue['front']
                
            while next!= None and added == False:
                #if value is greater or equal add it, and flag not to add it again
                if priority_queue['cmp_func'](value, next['data']) == GREATER:
                    #put it before
                    added = True
                    add_this['next'] = next              
                    current['next'] = add_this
                elif priority_queue['cmp_func'](value, next['data']) == EQUALS:
                    #same as previous could use an or but i don't like or's
                    added = True
                    add_this['next'] = next              
                    current['next'] = add_this
              
                #after checking, check the next value 
                current = next   
                next = next['next']

            #if we are here with added == False, then current = the last position and next = none
            #ie add it to the next of current
            if added == False:
                current['next'] = add_this
            
                #while its smaller point at the 

    
    return
    # Your code here


#---------------------------------------------------------
# Removes value from priority_queue.
#-------------------------------------------------------
# Parameters:
#    priority_queue - a data queue
# Preconditions:
#    priority_queue - valid linked queue dictionary
# Postconditions:
#    Returns the highest priority value in priority_queue - the value is
#    removed from priority_queue. Returns None if priority_queue is empty.
#-------------------------------------------------------
def priority_queue_remove( priority_queue ):
    value = None
    if priority_queue['size'] > 0:
        value = priority_queue['front']['data']
        priority_queue['front'] = priority_queue['front']['next']
        priority_queue['size'] -= 1

    # Your code here.

    return value

#---------------------------------------------------------
# Peeks at the highest priority value in priority_queue.
#-------------------------------------------------------
# Parameters:
#    priority_queue - a priority queue
# Preconditions:
#    priority_queue - valid linked priority queue dictionary
# Postconditions:
#    Returns a copy of the highest priority value in priority_queue - 
#    the value is not removed from priority_queue. Returns None 
#    if priority_queue is empty.
#-------------------------------------------------------
def priority_queue_peek( priority_queue ):

    value = None
    if priority_queue['size'] > 0:
        value = copy.deepcopy(priority_queue['front']['data'])        

    return value

  

#---------------------------------------------------------
# Prints the contents of priority_queue using priority_queue's custom
# printing function in the order direction' ('f' or 'r').
#-------------------------------------------------------
# Parameters:
#    priority_queue - a priority queue
#    direction - direction to print queue contents in
# Preconditions:
#    priority_queue - valid linked priority queue dictionary
#    direction - string 'f' or 'r'
# Postconditions:
#    Prints each value in priority_queue using print_func.
#    Prints in forward order if direction is 'f', in
#    reverse order if direction is 'r'.
#-------------------------------------------------------
def priority_queue_print( priority_queue, direction ):
    if direction == 'f':
        priority_queue_print_forward_aux( priority_queue['front'], priority_queue['print_func'] )
    elif direction == 'r':
        priority_queue_print_reverse_aux( priority_queue['front'], priority_queue['print_func'] )
    return
    
def priority_queue_print_forward_aux( node, print_func ):
    if node is not None:
        print_func( node['data'] )
        priority_queue_print_forward_aux( node['next'], print_func )
    return
    
def priority_queue_print_reverse_aux( node, print_func ):
    if node is not None:
        priority_queue_print_reverse_aux( node['next'], print_func )
        print_func( node['data'] )
    return
