#-------------------------------------------------------
# album_test
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2009-01-19
#-------------------------------------------------------
# test the queue adt using the album function
#-------------------------------------------------------

from queue_array import *

import album

#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------



#-------------------------------------------------------
# Input variables
#-------------------------------------------------------



#-------------------------------------------------------
# Output variables
#-------------------------------------------------------


#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------



#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------
q = queue_init(album.print_)
album_list = album.read_file('albums.txt')

for track in album_list:   
    queue_insert(q, track)
    
print 'our list: '
queue_print(q,'f')
    
print 'the first track is %s' % (queue_peek(q))
print 'the size is: %d' % (queue_size(q)) 

track = queue_remove(q)
while track != None:
    print 'taking away %s' % (track)
    track = queue_remove(q)

    

