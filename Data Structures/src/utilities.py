#-------------------------------------------------------
# list_to_stack
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2009-01-19
#-------------------------------------------------------
# copies data from a list into a stack
#-------------------------------------------------------

from stack_linked import *
#from stack_linked import *


#-------------------------------------------------------
# Parameters:
#    stack, list
# Preconditions:
#    stack: a stack
#    list: a list
# Postconditions:
#    stack: contains the items from the listlist
#-------------------------------------------------------

def list_to_stack(list,stack):

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

    

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------


#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

    value = {}
    counter = 0
#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------
   
    #take the items from the list and push them onto the stack
    while (len(list) != 0):        
        value = list.pop()
        stack_push(stack, value)
    #empty the list
    
    #return the stack
    return 

#-------------------------------------------------------
# stack_to_list
#-------------------------------------------------------
#-------------------------------------------------------
# Parameters:
#    stack, list
# Preconditions:
#    stack: a stack
#    list: a list
# Postconditions:
#    stack: contains the items from the listlist
#-------------------------------------------------------

def stack_to_list(stack,list):

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------

    

#-------------------------------------------------------
# Output variables
#-------------------------------------------------------


#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------

    value = {}
    counter = 0
#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------
   
    #take the items from the list and push them onto the stack
    while (stack_empty(stack) == False):        
        value = stack_pop(stack)
        list.append(value)
    
    #empty the list
    
    #return the stack
    return list
    