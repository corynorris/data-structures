#-------------------------------------------------------
# stack test
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-12
#-------------------------------------------------------
# test the stack adt's
#-------------------------------------------------------

def print_(stack):
    print stack['data']




from stack_linked import *

#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------

def stack_test():
    

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------
    stack = {}


#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    value = 0

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    #init
    stack = stack_init(print_)
    
    #push some values
    for count in range(1,10,3):
        print 'pushed %d'%(count)
        stack_push(stack, count)

    
#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------

    stack_peek(stack)
    while stack_peek(stack)!= None:
         value = stack_pop(stack)
         print "popped %d" %(value)
    



stack_test()




