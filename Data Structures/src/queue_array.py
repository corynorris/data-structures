#-------------------------------------------------------
# queue_array.py
#-------------------------------------------------------
# Author:  David Brown
# ID:      
# Email:   dbrown@wlu.ca
# Version: 2009-01-05
#-------------------------------------------------------
# Array version of the Queue ADT.
#-------------------------------------------------------
import copy

#---------------------------------------------------------
# Returns an empty queue.
#-------------------------------------------------------
# Parameters:
#    print_func - data print function
# Preconditions:
#    print_func - valid function reference
# Postconditions:
#    Returns an empty queue.
#-------------------------------------------------------
def queue_init( print_func ):
    queue = { 'data':[], 'print_func': print_func }
    return queue

#---------------------------------------------------------
# Returns the size of queue.
#-------------------------------------------------------
# Parameters:
#    queue - a queue
# Preconditions:
#    queue - valid array queue dictionary
# Postconditions:
#    Returns the size of queue.
#-------------------------------------------------------
def queue_size( queue ):
    return len( queue['data'] )

#---------------------------------------------------------
# Determines if queue is empty.
#-------------------------------------------------------
# Parameters:
#    queue - a queue
# Preconditions:
#    queue - valid array queue dictionary
# Postconditions:
#    Returns True if queue is empty, False otherwise.
#-------------------------------------------------------
def queue_empty( queue ):
    return ( len( queue['data'] ) == 0 )

#---------------------------------------------------------
# Inserts value into queue.
#-------------------------------------------------------
# Parameters:
#    queue - a queue
#    value - a data element
# Preconditions:
#    queue - valid array queue dictionary
#    value - none
# Postconditions:
#    value is added to the front of queue.
#-------------------------------------------------------
def queue_insert( queue, value ):
    # queue = { 'data':[], 'print_func': print_func }
    queue['data'] += [copy.deepcopy(value)]
    # Your code here.

    return

#---------------------------------------------------------
# Removes value from queue.
#-------------------------------------------------------
# Parameters:
#    queue - a queue
# Preconditions:
#    queue - valid array queue dictionary
# Postconditions:
#    Returns the value at the front of queue - the value is
#    removed from queue. Returns None if queue is empty.
#-------------------------------------------------------
def queue_remove( queue ):
    value = None
    if (queue_size(queue) > 0):
        value = queue['data'][0]
        queue['data'].remove(value)
    
    # Your code here.

    return value

#---------------------------------------------------------
# Peeks at the front of queue.
#-------------------------------------------------------
# Parameters:
#    queue - a queue
# Preconditions:
#    queue - valid array queue dictionary
# Postconditions:
#    Returns a copy of the value at the front of queue - 
#    the value is not removed from queue. Returns None 
#    if queue is empty.
#-------------------------------------------------------
def queue_peek( queue ):
    value = None
    if (len(queue) > 0):
        value = copy.deepcopy(queue['data'][0])

    # Your code here.

    return value

#---------------------------------------------------------
# Prints the contents of queue using queue's custom
# printing function in the order direction' ('f' or 'r').
#-------------------------------------------------------
# Parameters:
#    queue - a queue
#    direction - direction to print queue contents in
# Preconditions:
#    queue - valid array queue dictionary
#    direction - string 'f' or 'r'
# Postconditions:
#    Prints each value in queue using print_func.
#    Prints in forward order if direction is 'f', in
#    reverse order if direction is 'r'.
#-------------------------------------------------------
def queue_print( queue, direction ):
    print_func = queue['print_func']
    n = len( queue['data'] )
    
    if direction == 'f':
        for i in range( n ):
            print_func( queue['data'][i] )
    elif direction == 'r':
        for i in range( n-1, -1, -1 ):
            print_func( queue['data'][i] )
    return
