#-------------------------------------------------------
# stack test
#-------------------------------------------------------
# Author:  Cory Norris
# ID:      080704410    
# Email:   norr4410@wlu.ca
# Version: 2008-11-12
#-------------------------------------------------------
# test the stack adt's
#-------------------------------------------------------

def print_(stack):
    print stack['data']



import stack_array
import album

#-------------------------------------------------------
# Parameters:
#    [parameter descriptions - if any]
# Preconditions:
#    [precondition descriptions - if any]
# Postconditions:
#    [postcondition descriptions] 
#-------------------------------------------------------

def stack_test():
    

#-------------------------------------------------------
# Input variables
#-------------------------------------------------------
    stack = {}


#-------------------------------------------------------
# Output variables
#-------------------------------------------------------
    value = 0

#-------------------------------------------------------
# Other variables or constants
#-------------------------------------------------------
    #init
    stack = stack_array.stack_init(print_)
    
    list = album.read_file('albums.txt')
    
    #push some values
    for count in list:
        print 'pushed %s'%(count)
        stack_array.stack_push(stack, count)

    
#-------------------------------------------------------
# [other comments as necessary]
#-------------------------------------------------------

    stack_array.stack_peek(stack)
    while stack_array.stack_peek(stack)!= None:
         value = stack_array.stack_pop(stack)
         print "popped %s" %(value)
    
         


stack_test()
